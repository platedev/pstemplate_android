package com.sophos.network.wireless


import android.content.Intent
import android.os.SystemClock
import android.view.View
import android.widget.ScrollView
import android.widget.TextView
import androidx.biometric.BiometricManager
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.*
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.acelink.library.biometric.BiometricPromptUtils.BiometricDisable
import com.acelink.library.biometric.BiometricPromptUtils.BiometricTooMany
import com.acelink.library.gson.JsonHelper
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.psnet.HttpResult
import com.acelink.library.sharepreference.PSGCMEncryptSPSecurity
import com.acelink.library.sharepreference.SPUtils
import com.acelink.library.utils.EasyUtils
import com.acelink.library.utils.EasyUtils.tryCatchBlock
import com.acelink.library.utils.JudgeIpAddress
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.R
import com.sophos.network.library.ping.Ping
import com.sophos.network.library.viewactions.RecyclerViewMatcher
import com.sophos.network.library.viewactions.ViewActs.Companion.getCountFromRecyclerView
import com.sophos.network.library.viewactions.ViewActs.Companion.getText
import com.sophos.network.library.viewactions.ViewActs.Companion.selectTabAtPosition
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.LanPortPageMode
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.adapter.ClientsAdapter
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.adapter.WirelessBandAdapter
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.fragments.adapter.DeviceAllListAdapter

import com.sophos.network.wireless.gui.system.AboutFragment
import com.sophos.network.wireless.gui.system.SettingsFragment
import com.sophos.network.wireless.gui.widget.dialog.SophosChooseDialog
import com.sophos.network.wireless.gui.widget.dragger.ViewDragLayout
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import com.sophos.network.wireless.psclass.enum.Authentication_Type
import com.sophos.network.wireless.psclass.enum.Radio
import com.sophos.network.wireless.psclass.gson.data.GetSecurity
import com.sophos.network.wireless.psclass.gson.data.SimpleResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.hamcrest.Matchers.hasToString
import java.lang.IndexOutOfBoundsException
import java.lang.StringBuilder


class ALLListTest {

    companion object{
        val testTypeNormal=0
        val testTypeCentral=1
        val testTypeUnknown=2
        private val newListPos=0
    }

    suspend fun pingTest()
    {
        var ping= Ping.getHost("8.8.8.8")
        ConstantClass.printForDebug("pingTest  ${ping}")
        //Assert.assertEquals(true,ping)
    }

    fun checkRecyclePosition(fragment: Fragment,targer:Int,rec:RecyclerView){
        if(targer>=5){
            fragment.lanuchMain {
                rec.smoothScrollToPosition(targer)
            }
            SystemClock.sleep(2000)
        }
    }


    fun menuTest(){
        //activityRule.onActivity {
        ConstantClass.printForDebug(null)
        ConstantClass.printForDebug("")

        ConstantClass.printForDebug(StringBuilder().apply { ((1..2001).map {append("1")  }) }.toString())
        EasyUtils.tryCatchBlock { throw IndexOutOfBoundsException() }
        EasyUtils.tryCatchBlock<Int> ({
            throw IndexOutOfBoundsException()
            return@tryCatchBlock 1 },0)
        tryCatchBlock {
            JsonHelper.parseJson(null,SimpleResult::class.java)
            JsonHelper.parseJson("",SimpleResult::class.java)
            frags_page.allList.pageFragment?.lanuchIO {
                SophosNetwork.retryAsyn { return@retryAsyn null }
                SophosNetwork.retryAsynBoolean { false }
            }
            BaseSophosDevice().apply {
                (0..7).forEach {
                    WiFiRadio=it
                    getWiFiRadioFrequency()
                }
                var models= arrayOf(null,"840","420")

                models.forEach {
                    model=it
                    model="840"
                    bandList()
                    arrayOf(0,60,100).forEach { cpu->
                        CPUUtilization=cpu
                        arrayOf(0,60,100).forEach { mem->
                            MemoryUtilization=mem
                            arrayOf(0,300,1000).forEach {user->
                                ConnectedUsers=user
                                calculateHealth()
                            }
                        }
                    }
                    isSupport6G()
                }

                isComplete()
                mac="1"
                isComplete()
                location="11"
                isComplete()
                model=null
                location=null
                isUnknown()
                model="1"
                isUnknown()
                SNR
                CPUUtilization
                MemoryUtilization
                location
                firmwareVersion
                model
                heath=1
            }
            SophosNetwork.getResult(HttpResult.Success("{}",0))
            SophosNetwork.getReloadTime(HttpResult.Success("{}",0))
            GetSecurity.SecurityData().apply {
                auth_detail= GetSecurity.AuthDetail()
                    auth_method=-1
                    Authentication_Type.getAuth(this,Radio.G6)
                    Authentication_Type.getAuth(this,Radio.G5)
                    auth_method=0
                    var auth=Authentication_Type.getAuth(this,Radio.G5)
                     auth.creatEncrypyJson(this)
                    auth_method=5
                    var owe=Authentication_Type.getAuth(this,Radio.G5)
                     owe.creatEncrypyJson(this)
                    auth_method=1
                    Authentication_Type.getAuth(this,Radio.G5)
                    auth_method=2
                    auth_detail!!.wpa_type = 5
                    auth_detail!!.encrypt_type=3
                    Authentication_Type.getAuth(this,Radio.G5)
                    auth_detail!!.wpa_type =11
                    Authentication_Type.getAuth(this,Radio.G5)
                     auth.creatEncrypyJson(this)
                    auth_detail!!.wpa_type =10
                    Authentication_Type.getAuth(this,Radio.G5)
                  auth.creatEncrypyJson(this)
                    auth_detail!!.wpa_type =9
                    Authentication_Type.getAuth(this,Radio.G5)
                    auth.creatEncrypyJson(this)

                    auth_method=3
                    auth_detail!!.wpa_type = 5
                    auth_detail!!.encrypt_type=3
                    Authentication_Type.getAuth(this,Radio.G5)
                    auth_method=3
                    auth_detail!!.wpa_type = 10
                    Authentication_Type.getAuth(this,Radio.G5)
                    auth_method=3
                    auth_detail!!.wpa_type = 9
                    Authentication_Type.getAuth(this,Radio.G5)

            }
            ConstantClass.printForDebug("Authentication_Type.setAuth")
            GetSecurity.SecurityData().apply {
                auth_detail= GetSecurity.AuthDetail()
                Authentication_Type.values().forEach {
                    Authentication_Type.setAuth(this,it)
                    it.getStringRes()
                    it.needPassword()
                }

            }
            frags_page.allList.pageFragment?.apply {
                lanuchMain {
                    SophosChooseDialog(activity!!,null, arrayOf("1","2").toMutableList(),result={dialog, choose ->  }).apply {
                        show()
                        SystemClock.sleep(1000)
                        dismiss()
                    }
                }

            }
            SystemClock.sleep(1100)

            ConstantClass.printForDebug("TrycatchBlock End======================================>")
        }

        Espresso.onView(withId(R.id.app_main_all_layout)).check(matches(isDisplayed()))
        ConstantClass.printForDebug("menuTest 11")
      //  Espresso.onView(withId(R.id.app_main_all_layout)).perform(click())
      //  ConstantClass.printForDebug("menuTest  app_main_all_layout click")
        Espresso.onView(withId(R.id.app_title_left)).let { menu->
            ConstantClass.printForDebug("menu_ap get ${matches(isDisplayed())}")
            menu.perform(ViewActions.click())
            ConstantClass.printForDebug("menu_ap end")
            SystemClock.sleep(1000)
            menu.perform(ViewActions.click())
            SystemClock.sleep(1000)
            menu.perform(ViewActions.click())
        }

        SystemClock.sleep(1000)
        Espresso.onView(withId(R.id.nav_view_left)).check(matches(isDisplayed()))
        ConstantClass.printForDebug("nav_view_left isDisplayed")
        Espresso.onView(withId(R.id.main_settings)).perform(click())
        SystemClock.sleep(1000)

      /*  Espresso.onView(withId(R.id.ap_setting_face_switch)).perform(click()) //may lead crash
        SystemClock.sleep(1000)
        Espresso.onView(withId(R.id.ap_setting_password_switch)).perform(click())
        SystemClock.sleep(1000)

        Espresso.onView(withId(R.id.ap_setting_face_switch)).perform(click())
        SystemClock.sleep(1000)
        Espresso.onView(withId(R.id.ap_setting_password_switch)).perform(click())
        SystemClock.sleep(1000)*/

        (frags_page.settings.pageFragment as SettingsFragment).apply {
            arrayOf(BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE,BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE,
                BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED, BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED).forEach {
                lanuchMain {biometricFail(it)}
                SystemClock.sleep(1000)
                Espresso.onView(withId(R.id.ok)).perform(click())
                SystemClock.sleep(1000)
            }
            arrayOf(BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE,BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE,
                BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED, BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED).forEach {
                lanuchMain {pincodeFail(it)}
                SystemClock.sleep(1000)
                if(msgDialog?.isShowing?:false)msgDialog?.dismiss()
                SystemClock.sleep(1000)
            }
            SPUtils.setPincode(requireContext(),0)
            (frags_page.settings.pageFragment as SettingsFragment).getMainActivity()?.apply {
                lanuchMain {showLockTooManyAlready(BiometricDisable)}
                SystemClock.sleep(2000)
                lanuchMain {if(mainMsgDialog?.isShowing?:false)mainMsgDialog?.dismiss()}

                SystemClock.sleep(1000)
                lanuchMain {getMainActivity()?.showLockTooManyAlready(BiometricTooMany)}
                SystemClock.sleep(2000)
                lanuchMain {if(mainMsgDialog?.isShowing?:false)mainMsgDialog?.dismiss()}
                SystemClock.sleep(1000)
                lanuchMain { getMainActivity()?.showNoLockFoundAlready()}
                SystemClock.sleep(2000)
                lanuchMain {if(mainMsgDialog?.isShowing?:false)mainMsgDialog?.dismiss()}
                SystemClock.sleep(1000)
            }

        }

        SystemClock.sleep(1000)
        (frags_page.settings.pageFragment as SettingsFragment).apply {
            lanuchMain {onBackPress()}
        }
        //Espresso.onView(withId(R.id.app_title_left)).perform(click())

        SystemClock.sleep(1000)
        Espresso.onView(withId(R.id.main_about)).check(matches(isDisplayed()))

        Espresso.onView(withId(R.id.main_about)).perform(click());
        SystemClock.sleep(1000)
        ConstantClass.printForDebug("about_terms_lay")
        Espresso.onView(withId(R.id.about_terms_txt)).check(matches(isDisplayed()))
       // Espresso.onView(withId(R.id.about_terms_txt)).perform(click());
        (frags_page.about.pageFragment as AboutFragment).apply {
            lanuchMain {
                binding.aboutTermsTxt.performClick()
            }

            ConstantClass.printForDebug("about_terms_lay end")
            SystemClock.sleep(7000)


            val i = Intent(requireActivity(), MainActivity::class.java)
            i.action = Intent.ACTION_MAIN
            i.addCategory(Intent.CATEGORY_LAUNCHER)
            ContextCompat.startActivity(requireContext(),i,null)
        }


        SystemClock.sleep(5000)
        Espresso.pressBack()
        SystemClock.sleep(1000)
        ConstantClass.printForDebug("menu_ap test")
        //onView(withId(R.id.menu_ap)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withId(R.id.menu_ap)).perform(click());
        SystemClock.sleep(2000)

        frags_page.allList.pageFragment?.apply {
            lanuchMain { onBackPress() }
        }
        ConstantClass.printForDebug("menuTest End======================================>")
    }

    fun TabSelect(pos:Int){
        //activityRule.onActivity {
        ConstantClass.printForDebug("TabSelect ${pos}")
        onView(withId(R.id.device_all_list_tabs))?.let {
            it.perform(selectTabAtPosition(pos))

        }
    }

    fun EditTabSelect(pos:Int){
        //activityRule.onActivity {
        ConstantClass.printForDebug("EditTabSelect ${pos}")
        onView(withId(R.id.ap_edit_tabs))?.let {
            it.perform(selectTabAtPosition(pos))

        }
    }

    fun AllListTestFun(activityScenarioRule:ActivityScenarioRule<MainActivity>) {

        ConstantClass.printForDebug("AllListTestFun Start======================================>")

        TabSelect(1)
        SystemClock.sleep(1000)
        onView(withId(R.id.device_all_list_new_edit_filter)).check(matches(isDisplayed()))
        ConstantClass.printForDebug("AllListTestFun new filter isDisplayed")
        var countNew=getCountFromRecyclerView(R.id.device_new_list_recycleview)
        ConstantClass.printForDebug("AllListTestFun countNew ${countNew}")
        if(countNew>0){
            var newRecycle= RecyclerViewMatcher(R.id.device_new_list_recycleview)
            onView(newRecycle.atPositionOnView(newListPos,R.id.device_list_next))?.apply {
               // this.check { view, noViewFoundException ->
                   // noViewFoundException?.printStackTrace()
                   // if(view!=null){
                        onView(withId(R.id.device_all_list_new_edit_filter))?.let {
                            it.withFailureHandler(object :FailureHandler{
                                override fun handle(error: Throwable?, viewMatcher: org.hamcrest.Matcher<View>?) {
                                    ConstantClass.printForDebug("device_all_list_new_edit_filter handle")
                                    error?.printStackTrace()
                                }
                            })
                            SystemClock.sleep(1000)
                            ConstantClass.printForDebug("AllListTestFun new filter get ${matches(isDisplayed())}")
                            it.perform(typeText("s1"), closeSoftKeyboard())
                            SystemClock.sleep(1000)
                            it.perform(clearText())
                            it.perform(typeText("pp"), closeSoftKeyboard())
                            SystemClock.sleep(1000)
                            it.perform(clearText())
                            it.perform(typeText("a0"), closeSoftKeyboard())
                            SystemClock.sleep(1000)
                            it.perform(clearText())
                            ConstantClass.printForDebug("AllListTestFun new filter key end")
                        }
                        ConstantClass.printForDebug("nAllListTestFun ew filter key click")
                        perform(click())
                  //  }
                }

           // }
        }
            SystemClock.sleep(1000)
            TabSelect(0)
            SystemClock.sleep(1000)
            onView(withId(R.id.device_all_list_my_edit_filter)).check(matches(isDisplayed()))
            ConstantClass.printForDebug("device_all_list_new_edit_filter countNew isDisplayed")
            var countMy=getCountFromRecyclerView(R.id.devicelist_all_list_recycleview)

            ConstantClass.printForDebug("device_all_list_tabs countNew ${countNew}")
            if(countMy>0){
                SystemClock.sleep(1000)
                ConstantClass.printForDebug("my filter get ${matches(isDisplayed())}")
                onView(withId(R.id.device_all_list_my_edit_filter))?.let {
                    it.perform(typeText("s1"), closeSoftKeyboard())
                    SystemClock.sleep(1000)
                    it.perform(clearText())
                    it.perform(typeText("pp"), closeSoftKeyboard())
                    SystemClock.sleep(1000)
                    it.perform(clearText())
                    it.perform(typeText("a0"), closeSoftKeyboard())
                    SystemClock.sleep(1000)
                    it.perform(clearText())
                    ConstantClass.printForDebug("my filter key end")
                }

            }
        PSGCMEncryptSPSecurity.clears()
        activityScenarioRule.scenario.onActivity {
            it.getSophosDevice().forEach {
                it.new=true
            }
            it.getSophosDevice().clear()
        }
        TabSelect(1)
        SystemClock.sleep(1000)
        TabSelect(0)
        SystemClock.sleep(5000)
       /* TabSelect(1)
        SystemClock.sleep(20000)
        onView(withId(R.id.device_all_new_add)).perform(click())
         SystemClock.sleep(1000)
         onView(withId(R.id.ok)).perform(click())*/


        SystemClock.sleep(1000)
        PSGCMEncryptSPSecurity.clears()
        pressBack()
        ConstantClass.printForDebug("AllListTestFun End======================================>")
        SystemClock.sleep(500)
    }

    //testType -1:cancel 0:normal 1:central 2:unknown
    fun LoginFun(activityScenarioRule:ActivityScenarioRule<MainActivity>?,testType:Int,block:()->Unit) {

        ConstantClass.printForDebug("LoginFun Start testType=${testType}----->")
        TabSelect(0)
        SystemClock.sleep(1000)
        onView(withId(R.id.device_all_list_my_edit_filter)).check(matches(isDisplayed()))
        ConstantClass.printForDebug("LoginTestFun my filter isDisplayed")
        var countMy=getCountFromRecyclerView(R.id.devicelist_all_list_recycleview)
        ConstantClass.printForDebug("LoginTestFun countMy ${countMy}")
        if(countMy>0){
            ConstantClass.printForDebug("LoginFun click my first")
            if(testType==2){
                activityScenarioRule?.scenario?.onActivity {
                    (frags_page.allList.pageFragment as DeviceAllListFragment).getViewPager2Adapter().deviceMyAllListFragment.getDeviceMyAllListAdapter()?.filterResult?.forEach {
                        ConstantClass.printForDebug("My List For each mac=${it.mac}")
                            it?.timeNotFound=20
                    }
                }
                //click on first item
                onView(withId(R.id.devicelist_all_list_recycleview))
                    .perform(RecyclerViewActions.actionOnItemAtPosition<DeviceAllListAdapter.ViewHolder>(0, click()));
                SystemClock.sleep(10000)
                block.invoke()
            }else{
                //click on first item
               // onView(withId(R.id.devicelist_all_list_recycleview))
                  //  .perform(RecyclerViewActions.actionOnItemAtPosition<DeviceAllListAdapter.ViewHolder>(0, click()));
                (frags_page.allList.pageFragment as DeviceAllListFragment).getViewPager2Adapter()?.deviceMyAllListFragment?.apply {
                    lanuchMain {
                        binding?.devicelistAllListRecycleview?.findViewHolderForLayoutPosition(0)?.itemView?.performClick()
                    }

                }
                SystemClock.sleep(2000)
                loginDialogTest(activityScenarioRule,testType,false,false)
                SystemClock.sleep(2000)
                block.invoke()
            }


        }else{
            TabSelect(1)
            SystemClock.sleep(1000)
            var countNew=getCountFromRecyclerView(R.id.device_new_list_recycleview)
            ConstantClass.printForDebug("LoginTestFun  countNew ${countNew}")

            if(countNew>newListPos) {
                var newRecycle = RecyclerViewMatcher(R.id.device_new_list_recycleview)
                onView(newRecycle.atPositionOnView(newListPos, R.id.device_list_next))?.apply {
                    perform(click())
                    //onView(withId(R.id.device_new_list_recycleview)).perform(RecyclerViewActions.actionOnItem<DeviceAllListAdapter.ViewHolder>(hasDescendant(withText("00:AA:BB:DE:DA:3A")), click()));
                    SystemClock.sleep(1000)
                    TabSelect(0)
                    SystemClock.sleep(1000)
                    ConstantClass.printForDebug("LoginTestFun  again ")
                    LoginFun(activityScenarioRule,testType,block)
                }
            }
        }
    }

    fun LoginTestFun() {

        ConstantClass.printForDebug("LoginTestFun Start======================================>")
        TabSelect(0)
        SystemClock.sleep(1000)
        onView(withId(R.id.device_all_list_my_edit_filter)).check(matches(isDisplayed()))
        ConstantClass.printForDebug("LoginTestFun my filter isDisplayed")
        var countMy=getCountFromRecyclerView(R.id.devicelist_all_list_recycleview)
        ConstantClass.printForDebug("LoginTestFun countMy ${countMy}")
        if(countMy>0){
            ConstantClass.printForDebug("click my first")
            //click on first item
            onView(withId(R.id.devicelist_all_list_recycleview))
                .perform(RecyclerViewActions.actionOnItemAtPosition<DeviceAllListAdapter.ViewHolder>(0, click()));
            SystemClock.sleep(2000)
            loginDialogTest(null,-1,false,true)
            SystemClock.sleep(1000)
            onView(withId(R.id.devicelist_all_list_recycleview))
                .perform(RecyclerViewActions.actionOnItemAtPosition<DeviceAllListAdapter.ViewHolder>(0, click()));
            SystemClock.sleep(2000)
            loginDialogTest(null,0,false,true)
            SystemClock.sleep(5000)
            loginDialogTest(null,0,false,false)
            onView(withId(R.id.ap_edit_title_delete)).apply {
                check(matches(isDisplayed()))
                perform(click())
            }
            SystemClock.sleep(15000)


            TabSelect(1)
            SystemClock.sleep(1000)
            var countNew=getCountFromRecyclerView(R.id.device_new_list_recycleview)
            ConstantClass.printForDebug("LoginTestFun  countNew ${countNew}")
            if(countNew>0) {
                var newRecycle = RecyclerViewMatcher(R.id.device_new_list_recycleview)
                onView(newRecycle.atPositionOnView(newListPos, R.id.device_list_next))?.apply {
                    perform(click())
                    SystemClock.sleep(1000)
                    ConstantClass.printForDebug("LoginTestFun  check ")
                    TabSelect(0)
                    SystemClock.sleep(1000)
                    var countMy=getCountFromRecyclerView(R.id.devicelist_all_list_recycleview)
                    if(countMy>0){
                        onView(withId(R.id.devicelist_all_list_recycleview))
                            .perform(RecyclerViewActions.actionOnItemAtPosition<DeviceAllListAdapter.ViewHolder>(0, click()));
                        SystemClock.sleep(2000)
                        loginDialogTest(null,0,true,false)
                        onView(withId(R.id.ap_edit_title_delete)).apply {
                            check(matches(isDisplayed()))
                            perform(click())
                        }
                        SystemClock.sleep(8000)
                    }
                }
            }




        }else{
            TabSelect(1)
            SystemClock.sleep(1000)
            var countNew=getCountFromRecyclerView(R.id.device_new_list_recycleview)
            ConstantClass.printForDebug("LoginTestFun  countNew ${countNew}")
            if(countNew>0) {
                var newRecycle = RecyclerViewMatcher(R.id.device_new_list_recycleview)
                onView(newRecycle.atPositionOnView(newListPos, R.id.device_list_next))?.apply {
                    perform(click())
                    SystemClock.sleep(1000)
                    ConstantClass.printForDebug("LoginTestFun  again ")
                    LoginTestFun()
                }
            }
        }

        ConstantClass.printForDebug("LoginTestFun End======================================>")
    }

    fun APIGetTestFun() {

        ConstantClass.printForDebug("APITestFun Start======================================>")
        LoginFun(null,0,{
            ConstantClass.printForDebug("APITestFun 1")
            EditTabSelect(1)
            SystemClock.sleep(2500)
            ConstantClass.printForDebug("APITestFun 2")
            EditTabSelect(2)
            SystemClock.sleep(2500)
            ConstantClass.printForDebug("APITestFun 3")
            EditTabSelect(3)
            SystemClock.sleep(2500)
            ConstantClass.printForDebug("APITestFun ")
            EditTabSelect(4)
            SystemClock.sleep(1000)
            ConstantClass.printForDebug("APITestFun 5")
            EditTabSelect(5)
            SystemClock.sleep(1000)
            ConstantClass.printForDebug("APITestFun 6")
            EditTabSelect(6)
            SystemClock.sleep(1000)
        })
        ConstantClass.printForDebug("APITestFun End======================================>")
    }

    fun APISystemPingTestFun() {

        ConstantClass.printForDebug("APISystemPingTestFun Start======================================>")
        LoginFun(null,0,{
            ConstantClass.printForDebug("System 0")
            EditTabSelect(0)
            SystemClock.sleep(1000)
            onView(withId(R.id.ap_edit_editable_layout)).let {editable->
                editable.check(matches(isDisplayed()))
                ConstantClass.printForDebug("editable click")
                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                editable.perform(click())//save
                SystemClock.sleep(1000)
                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                editable.perform(click())//save
                SystemClock.sleep(1000)
                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                (frags_page.ap_edit.pageFragment as APEditFragment).apply {
                    lanuchMain {
                        onBackPress()
                    }
                }
                //pressBack()
                SystemClock.sleep(1000)
                onView(withId(R.id.ap_edit_product_name))?.apply {
                    editable.perform(click())//Edit
                    SystemClock.sleep(1000)
                    val name=getText(this)
                    perform(clearText(), closeSoftKeyboard())
                    SystemClock.sleep(1000)
                    perform(typeText("${name}_test"), closeSoftKeyboard())
                    SystemClock.sleep(1000)
                    editable.perform(click())//Change->Normal
                    SystemClock.sleep(100000)

                    editable.perform(click())//Edit
                    SystemClock.sleep(1000)

                    perform(clearText(), closeSoftKeyboard())
                    SystemClock.sleep(1000)
                    perform(typeText(name), closeSoftKeyboard())
                    SystemClock.sleep(1000)
                    editable.perform(click())//Change->Normal
                    SystemClock.sleep(50000)

                }
            }

            ConstantClass.printForDebug("Ping test ")
            EditTabSelect(5)
            SystemClock.sleep(2500)
            onView(withId(R.id.ap_edit_ping_test_dest_address)).apply {
                perform(typeText("8.8.8.8"), closeSoftKeyboard())
                SystemClock.sleep(1000)
                onView(withId(R.id.ap_edit_ping_test_exe)).perform(click())
                SystemClock.sleep(15000)
            }

            ConstantClass.printForDebug("traceroute test 6")
            EditTabSelect(6)
            SystemClock.sleep(2500)
            onView(withId(R.id.ap_edit_ping_test_dest_address)).apply {
                perform(typeText("8.8.8.8"), closeSoftKeyboard())
                SystemClock.sleep(1000)
                onView(withId(R.id.ap_edit_ping_test_exe)).perform(click())
                SystemClock.sleep(10000)
            }
            Espresso.onView(withId(R.id.app_title_left)).let { menu->
                ConstantClass.printForDebug("menu_ap get ${matches(isDisplayed())}")
                menu.perform(ViewActions.click())
                ConstantClass.printForDebug("menu_ap end")
                SystemClock.sleep(1000)

            }
        })
        ConstantClass.printForDebug("APISystemPingTestFun End======================================>")
    }

    fun APILanPortTestFun() {
       var lanPortSetTime=115000L//need greater 100sec
        ConstantClass.printForDebug("APILanPortTestFun Start======================================>")
        LoginFun(null,0,{
            ConstantClass.printForDebug("APILanPortTestFun 0")
            EditTabSelect(1)
            SystemClock.sleep(4000)
            EditTabSelect(1)
            SystemClock.sleep(3000)

            onView(withId(R.id.ap_edit_editable_layout)).let {editable->
                editable.check(matches(isDisplayed()))
                ConstantClass.printForDebug("editable click")
                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                editable.perform(click())//save
                SystemClock.sleep(2000)

                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                editable.perform(click())//save
                SystemClock.sleep(2000)

                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                (frags_page.ap_edit.pageFragment as APEditFragment).apply {
                    lanuchMain {
                        onBackPress()
                    }
                }
               // pressBack()
                SystemClock.sleep(2000)


                (frags_page.ap_edit.pageFragment as APEditFragment).getViewPager2EditAdapter().deviceLanPortFragment.let {lanport->
                    lanport.binding.apply {
                        JudgeIpAddress.isIPv4Address(null)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"abc.255.0.0","192.168.1.25",true,"192.168.1.25","192.168.1.25")//mask
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.0.0","192.168.1.25",true,"192.168.1.25","192.168.1.25")//mask
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.0","192.168.1.25.25",true,"192.168.1.25","192.168.1.25")//ip
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"25.255.255.0","192.168.1.25",true,"192.168.1.25","192.168.1.25")//mask
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.25.255.0","192.168.1.25",true,"192.168.1.25","192.168.1.25")//mask
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.25.0","192.168.1.25",true,"192.168.1.25","192.168.1.25")//mask
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.25","192.168.1.25",true,"192.168.1.25","192.168.1.25")//mask
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","192.168.1.25",true,"192.168.1.25","192.168.1.25")//A ip
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","172.16.0.9",true,"127.15.0.0","172.16.125.0")//ip
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","252.16.0.9",true,"127.15.0.0","172.16.125.0")//B ip
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)


                        //start end parse check
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","192.168.0.9",true,"192.168.0.0.0","192.168.0.100")//start
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","192.168.0.9",true,"192.168.0.0","192.168.0.100.0")//end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","192.168.0.1",true,"168.168.0.127","168.168.0.100")//end <start
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","172.168.0.9",true,"168.168.0.0","172.168.0.200")//end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","172.168.0.9",true,"168.168.0.10","172.168.0.252")//end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","172.168.0.9",true,"168.168.0.1","172.168.0.100")//<startIP<end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","225.168.0.9",true,"168.168.0.1","172.168.0.100")//<startIP<end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","192.168.0.9",true,"192.168.0.1","192.168.1.8")//end 3nd num g
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","225.168.0.9",true,"168.168.0.0","172.168.0.100")//startIP is subnet
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.255.128","225.168.0.9",true,"168.168.0.1","172.168.0.127")//startIP is broadcast
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)


                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"172.16.0.0.0","172.16.0.100")//start
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"172.16.0.0","172.16.0.100.0")//end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"17.16.0.0","172.16.0.100")//start
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"172.16.0.0","17.16.0.100")//end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"200.16.0.0","172.16.0.100")//start
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"172.16.0.0","200.16.0.100")//start<ip<end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"172.16.0.9","172.16.0.100")//same start
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.11",true,"172.16.0.2","172.16.0.11")//same end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"172.16.0.12","172.16.0.13")//B True
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"168.16.0.0","168.16.10.0")//startIP is subnet
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.255.128.0","172.16.0.9",true,"168.168.0.1","172.16.255.255")//startIP is broadcast
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)

                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.16.0.0.0","10.16.0.100")//start
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.16.0.0","10.16.0.100.0")//end
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"127.16.0.0","172.15.125.0")
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.16.0.0","172.15.125.0")
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.0.0.0","10.15.125.0")
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.16.0.0","10.0.125.0")
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.16.0.9","10.15.125.0")
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.0.0.0","10.16.10.0")//startIP is subnet
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.1.0.0","10.127.255.255")//startIP is broadcast
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)
                        JudgeIpAddress.CheckMaskandIPAddress(lanport,"255.128.0.0","10.16.0.9",true,"10.16.0.10","10.125.125.0")//A retrun true
                        SystemClock.sleep(1000)
                        lanport.messageDialog?.dismiss()
                        SystemClock.sleep(500)

                        var lanTypeSelect:(static:Boolean,dhcpServerEnable:Boolean)->Unit={static,dhcpServerEnable->
                            ConstantClass.printForDebug("lanTypeSelect static=${static}  dhcpServerEnable=${lanport.dhcpServerEnable}")
                            SystemClock.sleep(1000)
                            lanport.lanuchMain { apEditLanTypeSelect.performClick() }
                            SystemClock.sleep(1000)

                            lanport.lanuchMain {
                                lanport.lanSelectDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(if(static)0 else 1)?.itemView?.performClick()
                            }
                            SystemClock.sleep(2000)
                            if(static){

                                lanport.lanuchMain { lanport.binding.apEditLanIpTxt.setText("192.168.1.9.9")}
                                SystemClock.sleep(1000)
                                editable.perform(click())
                                SystemClock.sleep(1000)
                                lanport.lanuchMain {lanport.messageDialog?.dismiss()}
                                SystemClock.sleep(1000)
                                lanport.lanuchMain {
                                    lanport.binding.apEditLanIpTxt.setText("192.168.1.9")
                                    lanport.binding.apEditSubnetMaskTxt.setText("255.255.255.0.0")
                                }
                                SystemClock.sleep(1000)
                                editable.perform(click())
                                SystemClock.sleep(1000)
                                lanport.messageDialog?.dismiss()
                                SystemClock.sleep(1000)
                                lanport.lanuchMain {
                                    lanport.binding.apEditSubnetMaskTxt.setText("255.255.255.0")
                                    lanport.binding.apEditGatewayTxt.setText("192.168.1.1.0")
                                }
                                SystemClock.sleep(1000)
                                editable.perform(click())
                                SystemClock.sleep(1000)
                                lanport.messageDialog?.dismiss()
                                SystemClock.sleep(1000)
                                lanport.lanuchMain {
                                lanport.binding.apEditGatewayTxt.setText("192.168.1.2")
                                lanport.binding.apEditDnsPrimaryTxt.setText("0.0.0.0.0")
                                }
                                SystemClock.sleep(1000)
                                editable.perform(click())
                                SystemClock.sleep(1000)
                                lanport.messageDialog?.dismiss()
                                SystemClock.sleep(1000)
                                lanport.lanuchMain {
                                    lanport.binding.apEditDnsPrimaryTxt.setText("0.0.0.0")
                                    lanport.binding.apEditDnsSecondaryTxt.setText("0.0.0.0.0")
                                }
                                SystemClock.sleep(1000)
                                editable.perform(click())
                                SystemClock.sleep(1000)
                                lanport.messageDialog?.dismiss()
                                SystemClock.sleep(1000)
                                lanport.lanuchMain {lanport.binding.apEditDnsSecondaryTxt.setText("0.0.0.0")}

                                if(dhcpServerEnable){
                                    ConstantClass.printForDebug("apEditLanPortScroll FOCUS_DOWN for dhcpServerEnable=${lanport.dhcpServerEnable}")
                                    lanport.lanuchMain {
                                        apEditLanPortScroll.fullScroll(ScrollView.FOCUS_DOWN);
                                    }
                                    SystemClock.sleep(2000)
                                    ConstantClass.printForDebug("apEditLanPortScroll  dhcpServerEnable Click")
                                    if(!lanport.dhcpServerEnable){
                                        lanport.lanuchMain {
                                            lanport.binding.apEditDhcpServerCheck.performClick()
                                        }
                                        SystemClock.sleep(1000)
                                    }
                                    ConstantClass.printForDebug("apEditLanPortScroll  dhcpServerEnable Click")
                                    lanport.lanuchMain {
                                        lanport.binding.apEditDhcpStartingIpTxt.setText("192.168.1.11.11")
                                    }
                                    SystemClock.sleep(1000)
                                    editable.perform(click())
                                    SystemClock.sleep(1000)
                                    lanport.messageDialog?.dismiss()
                                    SystemClock.sleep(1000)
                                    lanport.lanuchMain {
                                        lanport.binding.apEditDhcpStartingIpTxt.setText("192.168.1.11")
                                        lanport.binding.apEditDhcpEndingIpTxt.setText("192.168.18.1.12")
                                    }

                                    SystemClock.sleep(1000)
                                    editable.perform(click())
                                    SystemClock.sleep(1000)
                                    lanport.messageDialog?.dismiss()
                                    SystemClock.sleep(1000)
                                    lanport.lanuchMain {
                                        lanport.binding.apEditDhcpStartingIpTxt.setText("192.168.1.11")
                                        lanport.binding.apEditDhcpEndingIpTxt.setText("192.168.1.10")
                                    }
                                    SystemClock.sleep(1000)
                                    editable.perform(click())
                                    SystemClock.sleep(1000)
                                    lanport.messageDialog?.dismiss()
                                    SystemClock.sleep(1000)
                                    lanport.lanuchMain {
                                        lanport.binding.apEditDhcpEndingIpTxt.setText("192.168.1.12")
                                        SystemClock.sleep(1000)
                                    }
                                    lanport.lanuchMain {
                                        lanport.binding.apEditDhcpLeaseTimeTxt.performClick()
                                    }

                                    SystemClock.sleep(1500)
                                    lanport.lanuchMain {
                                        lanport.leaseTimeDialog?.bindingChoose?.chooseDialogRecycle!!.findViewHolderForLayoutPosition((0..3).random())?.itemView?.performClick()
                                    }
                                    SystemClock.sleep(1500)
                                }
                            }
                            ConstantClass.printForDebug("apEditLanIpTxt =${lanport.binding.apEditLanIpTxt.text.toString()}")
                            SystemClock.sleep(1000)
                        }



                            var dhcpSetting:(pos:Int)->Unit={pos ->
                                ConstantClass.printForDebug("DHCP DHCP Setting ${pos} time")
                                editable.perform(click())//Normal
                                SystemClock.sleep(1000)


                                var type=apEditLanTypeSelect.text.toString()
                                val posLan=if("IP" in type) 0 else 1
                                ConstantClass.printForDebug("dhcpSetting lan_type_select  posLan=${posLan}")
                                //DHCP first
                                if(posLan==0){
                                  //  SystemClock.sleep(1000)
                                    lanTypeSelect.invoke(false,false)
                                    SystemClock.sleep(1000)
                                }

                                lanport.lanuchMain { apEditGatewaySelectTxt.performClick() }
                                SystemClock.sleep(2000)
                                lanport.lanuchMain {
                                    lanport.dhcpChooseDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(pos)?.itemView?.performClick()
                                }
                                SystemClock.sleep(1000)

                                lanport.lanuchMain { apEditDnsPrimarySelectTxt.performClick() }
                                SystemClock.sleep(2000)
                                lanport.lanuchMain {
                                    lanport.dhcpChooseDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(pos)?.itemView?.performClick()
                                }
                                SystemClock.sleep(1000)

                                lanport.lanuchMain {
                                    apEditLanPortScroll.fullScroll(ScrollView.FOCUS_DOWN);
                                }
                                SystemClock.sleep(2000)

                                lanport.lanuchMain { apEditDnsSecondarySelectTxt.performClick() }
                                SystemClock.sleep(2000)
                                lanport.lanuchMain {
                                    lanport.dhcpChooseDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(pos)?.itemView?.performClick()
                                }
                                SystemClock.sleep(1000)

                                editable.perform(click())//Edit
                                ConstantClass.printForDebug("editable DHCP Setting ${pos} Start")
                                SystemClock.sleep(lanPortSetTime)
                                ConstantClass.printForDebug("editable DHCP Setting ${pos} END")
                            }

                            dhcpSetting.invoke(0)//user
                            dhcpSetting.invoke(1)//dhcp


                        var LanTypeSetting:(static:Boolean,dhcpServerEnable:Boolean)->Unit={static,dhcpServerEnable ->
                            var type=apEditLanTypeSelect.text.toString()
                            val posLan=if("IP" in type) 0 else 1
                            ConstantClass.printForDebug("LanTypeSetting 2 posLan=${posLan} static=${static} dhcpServerEnable=${dhcpServerEnable}")
                            if(static){
                                if(lanport.lanPortPageMode!= LanPortPageMode.Edit_Static)// dhcpServerEnable true only runb UI
                                  editable.perform(click())//Edit
                                lanTypeSelect.invoke(static,dhcpServerEnable)
                                // SystemClock.sleep(1000)
                                if(!dhcpServerEnable)// dhcpServerEnable true only runb UI
                                {
                                    editable.perform(click())
                                    ConstantClass.printForDebug("LanTypeSetting set start static=${static} dhcpServerEnable=${dhcpServerEnable}")
                                    SystemClock.sleep(lanPortSetTime)
                                }//Edit

                            }else{
                                if(posLan==0){
                                    if(lanport.lanPortPageMode!= LanPortPageMode.Edit_Static)// dhcpServerEnable true only runb UI
                                        editable.perform(click())//Edit
                                    lanTypeSelect.invoke(false,false)//DHCP  dhcpServerEnable only false
                                    // SystemClock.sleep(1000)
                                    editable.perform(click())//Edit
                                    ConstantClass.printForDebug("LanTypeSetting set start static=${static} dhcpServerEnable=${dhcpServerEnable}")
                                    SystemClock.sleep(lanPortSetTime)
                                }
                            }

                        }
                        LanTypeSetting.invoke(true,false)
                        LanTypeSetting.invoke(true,true)
                        LanTypeSetting.invoke(false,false)


                    }
                }

            }

        })
        ConstantClass.printForDebug("APILanPortTestFun End======================================>")
    }

    fun WirelessBandTestFun() {
        var wirelessSetTime=100000L//need greater 100sec
        ConstantClass.printForDebug("WirelessBandTestFun Start======================================>")
        LoginFun(null,0,{
            ConstantClass.printForDebug("WirelessBandTestFun 0")
            EditTabSelect(AdapterDeviceEditFragments.edit_wireless_band_page)
            SystemClock.sleep(5000)
            EditTabSelect(AdapterDeviceEditFragments.edit_wireless_band_page)
            SystemClock.sleep(5000)
            onView(withId(R.id.ap_edit_editable_layout)).let {editable->
                editable.check(matches(isDisplayed()))
                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                editable.perform(click())//save
                SystemClock.sleep(5000)

                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                editable.perform(click())//save
                SystemClock.sleep(5000)

                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                (frags_page.ap_edit.pageFragment as APEditFragment).apply {
                    lanuchMain {
                        onBackPress()
                    }
                }
               // pressBack()
                SystemClock.sleep(2000)
                ConstantClass.printForDebug("editable Checking End")
                (frags_page.ap_edit.pageFragment as APEditFragment).getViewPager2EditAdapter().deviceWirelessBandsFragment.let {bands->
                    bands.binding.apply {

                        var edit2GNum:(num:Int,testNPRule:Boolean)->Unit={num,testNPRule->
                            ConstantClass.printForDebug("editable edit2GNum num=${num}")
                            SystemClock.sleep(2000)
                            bands.lanuchMain {
                                apEditWirelessScroll.fullScroll(ScrollView.FOCUS_UP);
                            }
                            SystemClock.sleep(2000)
                            bands.lanuchMain {
                                apEdit24gNumTxt.performClick()
                            }

                            SystemClock.sleep(1000)
                            checkRecyclePosition(bands,num,bands.num24GDialog!!.bindingChoose?.chooseDialogRecycle!!)
                            bands.lanuchMain {
                                bands.num24GDialog!!.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(num)!!.itemView?.performClick()
                            }
                            SystemClock.sleep(1000)


                            if(testNPRule) {
                                (bands.binding.itemWireless24gBands.findViewHolderForLayoutPosition(
                                    0
                                ) as? WirelessBandAdapter.ViewHolder)?.apply {
                                    bands.lanuchMain {
                                        this@apply.vb.apEditAuthenticationType.performClick()
                                    }
                                    SystemClock.sleep(2000)

                                    var newRecycle = RecyclerViewMatcher(R.id.choose_dialog_recycle)
                                    onView(newRecycle.atPositionOnView(1, -1)).perform(click())
                                    SystemClock.sleep(1000)

                                    var ssid = this@apply.vb.apEditSsid.text.toString()
                                    bands.lanuchMain {
                                        this@apply.vb.apEditSsid.setText("${ssid}中文")
                                    }
                                    SystemClock.sleep(1000)
                                    editable.perform(click())
                                    SystemClock.sleep(1000)
                                    onView(withId(R.id.ok)).perform(click())
                                    SystemClock.sleep(1000)

                                    bands.lanuchMain {
                                        this@apply.vb.apEditSsid.setText("")
                                    }
                                    SystemClock.sleep(1000)
                                    editable.perform(click())
                                    SystemClock.sleep(1000)
                                    onView(withId(R.id.ok)).perform(click())
                                    SystemClock.sleep(1000)

                                    bands.lanuchMain {
                                        this@apply.vb.apEditSsid.setText(ssid)
                                    }
                                    SystemClock.sleep(1000)

                                    var key = this@apply.vb.apEditPresharedKey.text.toString()
                                    var testInvalidPassword: (pass: String) -> Unit = { pass ->

                                        bands.lanuchMain {
                                            this@apply.vb.apEditPresharedKey.setText("${key}中文")
                                        }
                                        SystemClock.sleep(1000)
                                        editable.perform(click())
                                        SystemClock.sleep(1000)

                                        onView(withId(R.id.ok)).perform(click())
                                        SystemClock.sleep(1000)
                                        bands.lanuchMain {
                                            if (key.isNullOrEmpty() || key.length < 8) {
                                                this@apply.vb.apEditPresharedKey.setText("123456789012345")
                                            } else {
                                                this@apply.vb.apEditPresharedKey.setText(key)
                                            }

                                        }
                                        SystemClock.sleep(1000)
                                    }
                                    testInvalidPassword.invoke("1234")
                                    testInvalidPassword.invoke("key_")

                                }
                            }

                        }
                        var edit5GNum:(num:Int)->Unit= { num ->
                            ConstantClass.printForDebug("editable edit5GNum num=${num}")
                            SystemClock.sleep(2000)
                            bands.lanuchMain {
                                apEditWirelessScroll.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                            SystemClock.sleep(2000)
                            bands.lanuchMain {
                                apEdit5gNumTxt.performClick()
                            }
                            SystemClock.sleep(1000)
                            checkRecyclePosition(bands,num,bands.num5GDialog!!.bindingChoose?.chooseDialogRecycle!!)
                            bands.lanuchMain {
                                bands.num5GDialog!!.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(num)!!.itemView?.performClick()
                            }
                            SystemClock.sleep(1000)
                        }

                        var edit6GNum:(num:Int)->Unit= { num ->
                            (frags_page.ap_edit.pageFragment as APEditFragment)?.getViewPager2EditAdapter()?.deviceWirelessBandsFragment?.apply {
                                if(support6G){
                                    ConstantClass.printForDebug("editable edit56Num num=${num}")
                                    SystemClock.sleep(1000)
                                    bands.lanuchMain {
                                        apEditWirelessScroll.fullScroll(ScrollView.FOCUS_DOWN);
                                    }
                                    SystemClock.sleep(1000)
                                    bands.lanuchMain {
                                        apEdit6gNumTxt.performClick()
                                    }
                                    SystemClock.sleep(1000)
                                    checkRecyclePosition(bands,num,bands.num6GDialog!!.bindingChoose?.chooseDialogRecycle!!)
                                    bands.lanuchMain {
                                        bands.num6GDialog!!.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(num)!!.itemView?.performClick()
                                    }
                                    SystemClock.sleep(1000)
                                    (bands.binding.itemWireless5gBands.findViewHolderForLayoutPosition(0) as?
                                            WirelessBandAdapter.ViewHolder)?.apply {
                                        bands.lanuchMain {
                                            this@apply.vb.apEditAuthenticationType.performClick()
                                        }
                                        SystemClock.sleep(2000)
                                        bands.lanuchMain {authDialog?.dismiss()}
                                        SystemClock.sleep(1000)
                                    }
                                }
                            }

                        }

                        editable.perform(click())//Normal
                        edit2GNum.invoke(7,true)
                        edit5GNum.invoke(7)
                        edit6GNum.invoke(7)
                        editable.perform(click())//Save
                        SystemClock.sleep(wirelessSetTime)

                        editable.perform(click())//Normal
                        edit2GNum.invoke(2,false)
                        edit5GNum.invoke(2)
                        edit6GNum.invoke(2)
                        editable.perform(click())//Save
                        SystemClock.sleep(wirelessSetTime)
                    }
                }

            }

        })
        ConstantClass.printForDebug("APILanPortTestFun End======================================>")
    }

    fun ClientsTestFun() {
        ViewDragLayout.DEBUG=true
        ConstantClass.printForDebug("ClientsTestFun Start======================================>")
        LoginFun(null,0,{
            ConstantClass.printForDebug("ClientsTestFun 0")
            EditTabSelect(AdapterDeviceEditFragments.edit_clients_page)
            SystemClock.sleep(4000)
            EditTabSelect(AdapterDeviceEditFragments.edit_clients_page)
            SystemClock.sleep(4000)

            onView(withId(R.id.ap_edit_clients_reflesh)).apply {
                check(matches(isDisplayed()))
                perform(click())
                SystemClock.sleep(1500)
            }

                ConstantClass.printForDebug("clients_reflesh ")
                (frags_page.ap_edit.pageFragment as APEditFragment).getViewPager2EditAdapter().deviceClientsFragment.let {clients->
                    clients.binding.apply {
                        (apEditClientsRecycleview.adapter as? ClientsAdapter)?.let{adapter->
                            var count=adapter.getItemCount()
                            if(count>0)
                            {
                                (apEditClientsRecycleview?.findViewHolderForLayoutPosition(0) as? ClientsAdapter.ViewHolder)?.vb?.apply {
                                    clients.lanuchMain {
                                        clientHoverVdl?.dragSpecificView(R.id.client_parent_view,-300,0)
                                    }
                                    SystemClock.sleep(2000)
                                    clients.lanuchMain {
                                        clientRemoveView.performClick()
                                    }
                                    SystemClock.sleep(10000)
                                }
                            }

                        }
                    }
                }



        })
        ConstantClass.printForDebug("APILanPortTestFun End======================================>")
    }

    fun APIDayTimeTestFun() {

        ConstantClass.printForDebug("APIDayTimeTestFun Start======================================>")
        LoginFun(null,0,{
            ConstantClass.printForDebug("APIDayTimeTestFun 0")
            EditTabSelect(AdapterDeviceEditFragments.edit_day_time_page)
            SystemClock.sleep(2000)
            EditTabSelect(AdapterDeviceEditFragments.edit_day_time_page)
            SystemClock.sleep(2000)
            onView(withId(R.id.ap_edit_editable_layout)).let {editable->
                editable.check(matches(isDisplayed()))
                ConstantClass.printForDebug("editable click")
                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                editable.perform(click())//save
                SystemClock.sleep(1000)
                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                editable.perform(click())//save
                SystemClock.sleep(1000)
                editable.perform(click())//Edit
                SystemClock.sleep(1000)
                (frags_page.ap_edit.pageFragment as APEditFragment).apply {
                    lanuchMain {
                        onBackPress()
                    }
                }
               // pressBack()//useless
                SystemClock.sleep(1000)
                (frags_page.ap_edit.pageFragment as APEditFragment).getViewPager2EditAdapter().dayTimeFragment.let {daytime->
                    daytime.binding.apply {
                            var changeNTPServerEnable:(enable:Boolean)->Unit={enable->
                                ConstantClass.printForDebug("changeNTPServerEnable enable=${enable} ntpServerEnable=${daytime.ntpServerEnable}")
                                if(daytime.ntpServerEnable!=enable){
                                    daytime.lanuchMain {
                                        apEditNtpServerNtpCheck.performClick()
                                    }
                                    SystemClock.sleep(1000)

                                 }
                            }


                        var TimeZoneServerEnableSetting:(daylight:Boolean,serverPos:Int,zonePos:Int)->Unit={daylight,serverPos,zonePos ->
                            ConstantClass.printForDebug("TimeZoneServerEnableSetting daylight=${daylight} serverPos=${serverPos} zonePos=${zonePos}")
                            editable.perform(click())//Edit
                            SystemClock.sleep(1000)
                            daytime.lanuchMain {
                                apEditDaytimeScrollParent.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                            SystemClock.sleep(1000)
                            changeNTPServerEnable.invoke(true)
                            daytime.lanuchMain {
                                //Enable Disable
                                var Enable=apEditDaylightSavingText.text.toString()=="Enable"
                                if(daylight!=Enable){
                                    apEditDaylightSavingText.performClick()
                                }
                            }
                            SystemClock.sleep(1000)
                            daytime.lanuchMain {
                                apEditServerRegion.performClick()
                            }
                            SystemClock.sleep(1000)
                            daytime.lanuchMain {
                                daytime.serverRegionDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(serverPos)?.itemView?.performClick()
                            }

                            SystemClock.sleep(1000)
                            daytime.lanuchMain {
                                apEditTimeZoneLay.performClick()
                            }
                            SystemClock.sleep(1000)
                            daytime.lanuchMain {
                                daytime.ZoneDialog?.bindingChoose?.chooseDialogRecycle?.apply {
                                    smoothScrollToPosition(zonePos)
                                }
                            }
                            SystemClock.sleep(2000)
                            daytime.lanuchMain {
                                daytime.ZoneDialog?.bindingChoose?.chooseDialogRecycle!!.findViewHolderForLayoutPosition(zonePos)!!.itemView!!.performClick()
                            }
                            SystemClock.sleep(1000)
                            editable.perform(click())
                            SystemClock.sleep(30000)
                        }

                        var TimeZoneManualSetting:(year:Int,mon:Int,day:Int,hour:Int,min:Int,sec:Int)->Unit={year,mon,day,hour,min,sec ->
                            editable.perform(click())//start edit
                            SystemClock.sleep(1000)

                            daytime.lanuchMain {
                                apEditDaytimeScrollParent.fullScroll(ScrollView.FOCUS_UP);
                            }
                            SystemClock.sleep(1000)
                            changeNTPServerEnable.invoke(false)
                            arrayOf(year,mon,day,hour,min,sec).forEachIndexed { index, value ->

                                when(index){
                                    0-> {
                                        daytime.lanuchMain {
                                            apEditYearTxt.performClick()
                                        }
                                        SystemClock.sleep(1000)
                                        checkRecyclePosition(daytime,value,daytime.yearDialog?.bindingChoose?.chooseDialogRecycle!!)
                                        daytime.lanuchMain {

                                            daytime.yearDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(year)?.itemView?.performClick()
                                        }
                                    }
                                    1->{
                                        daytime.lanuchMain {
                                            apEditMonthTxt.performClick()
                                        }
                                        SystemClock.sleep(1000)
                                        checkRecyclePosition(daytime,value,daytime.monthDialog?.bindingChoose?.chooseDialogRecycle!!)
                                        daytime.lanuchMain {
                                            daytime.monthDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(mon)?.itemView?.performClick()
                                        }
                                    }
                                    2->{
                                        daytime.lanuchMain {
                                            apEditDayTxt.performClick()
                                        }
                                        SystemClock.sleep(1000)
                                        checkRecyclePosition(daytime,value,daytime.dayDialog?.bindingChoose?.chooseDialogRecycle!!)
                                        daytime.lanuchMain {
                                            daytime.dayDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(day)?.itemView?.performClick()
                                        }
                                    }
                                    3->{
                                        daytime.lanuchMain {
                                            apEditHoursTxt.performClick()
                                        }
                                        SystemClock.sleep(1000)
                                        checkRecyclePosition(daytime,value,daytime.hourDialog?.bindingChoose?.chooseDialogRecycle!!)
                                        daytime.lanuchMain {
                                            daytime.hourDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(hour)?.itemView?.performClick()
                                        }
                                    }
                                    4->{
                                        daytime.lanuchMain {
                                            apEditMinuteTxt.performClick()
                                        }
                                        SystemClock.sleep(1000)
                                        checkRecyclePosition(daytime,value,daytime.minDialog?.bindingChoose?.chooseDialogRecycle!!)
                                        daytime.lanuchMain {
                                            daytime.minDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(hour)?.itemView?.performClick()
                                        }
                                    }
                                    5->{
                                        daytime.lanuchMain {
                                            apEditSecondTxt.performClick()
                                        }
                                        SystemClock.sleep(1000)
                                        checkRecyclePosition(daytime,value,daytime.secDialog?.bindingChoose?.chooseDialogRecycle!!)
                                        daytime.lanuchMain {
                                            daytime.secDialog?.bindingChoose?.chooseDialogRecycle?.findViewHolderForLayoutPosition(hour)?.itemView?.performClick()
                                        }
                                    }
                                }
                                SystemClock.sleep(1000)
                            }

                            SystemClock.sleep(1000)
                            editable.perform(click())//set

                            SystemClock.sleep(30000)
                        }

                        var TimeZoneSyncPhone:()->Unit={
                            editable.perform(click())//start edit
                            SystemClock.sleep(1000)

                            daytime.lanuchMain {
                                apEditDaytimeScrollParent.fullScroll(ScrollView.FOCUS_UP);
                            }
                            SystemClock.sleep(1000)
                            changeNTPServerEnable.invoke(false)

                            daytime.lanuchMain {
                                apEditDayTimeAcquireTxt.performClick()
                            }

                            SystemClock.sleep(1000)
                            editable.perform(click())//set

                            SystemClock.sleep(30000)
                        }
                        TimeZoneServerEnableSetting.invoke(true,3,59)//Asia  ,8:00 Perth
                        TimeZoneServerEnableSetting.invoke(false,5,8)//america,Central America
                        TimeZoneManualSetting.invoke(3,3,3,3,3,3)
                        TimeZoneManualSetting.invoke(10,10,10,10,10,10)
                        TimeZoneSyncPhone.invoke()

                    }
                }

            }

        })
        ConstantClass.printForDebug("APILanPortTestFun End======================================>")
    }

    fun APIRebootTestFun() {
        LoginFun(null,0,{
            onView(withId(R.id.ap_edit_reboot_layout)).apply {
                check(matches(isDisplayed()))
                perform(click())
                SystemClock.sleep(1000)
                onView(withId(R.id.ok)).perform(click())

            }
            SystemClock.sleep(130000)
            (frags_page.ap_edit.pageFragment as APEditFragment).apply {
                SystemClock.sleep(5000)
                lanuchMain {
                    showRebootResult(false)
                }
                SystemClock.sleep(4000)
                lanuchMain {
                    showChangeResult(false)
                }
                SystemClock.sleep(4000)
            }
            onView(withId(R.id.ap_edit_editable_layout)).check(matches(isDisplayed()))
        })
    }

    fun CentralTestFun(activityScenarioRule:ActivityScenarioRule<MainActivity>):Boolean {
        ConstantClass.printForDebug("CentralTestFun Start======================================>")
        var central=false
        LoginFun(activityScenarioRule,1,{
            central=frags_page.ap_central.pageFragment!=null
            if(!central){
                Espresso.pressBack()
                SystemClock.sleep(5000)
                return@LoginFun
            }
            ConstantClass.printForDebug("CentralTestFun 1")
            EditTabSelect(AdapterDeviceEditFragments.edit_system_page+1)
            SystemClock.sleep(2500)
            ConstantClass.printForDebug("CentralTestFun 2")
            EditTabSelect(AdapterDeviceEditFragments.edit_lan_port_page+1)
            SystemClock.sleep(3000)
            ConstantClass.printForDebug("CentralTestFun 3")
            EditTabSelect(AdapterDeviceEditFragments.edit_wireless_band_page+1)
            SystemClock.sleep(5000)
            ConstantClass.printForDebug("CentralTestFun 4")
            EditTabSelect(AdapterDeviceEditFragments.edit_clients_page+1)
            SystemClock.sleep(2000)
            ConstantClass.printForDebug("CentralTestFun 5")
            EditTabSelect(AdapterDeviceEditFragments.edit_day_time_page+1)
            SystemClock.sleep(1000)
            ConstantClass.printForDebug("CentralTestFun 6")
            EditTabSelect(6)
            SystemClock.sleep(1000)
            ConstantClass.printForDebug("CentralTestFun 0")
            EditTabSelect(0)
            SystemClock.sleep(2500)
            Espresso.pressBack()
            SystemClock.sleep(5000)
        })

        LoginFun(activityScenarioRule,1,{

            onView(withId(R.id.ap_edit_title_delete)).apply {
                check(matches(isDisplayed()))
                perform(click())
            }
            SystemClock.sleep(10000)
        })
        ConstantClass.printForDebug("CentralTestFun END======================================>")
        return central
    }

    fun UnknownTestFun(activityScenarioRule:ActivityScenarioRule<MainActivity>) {
        ConstantClass.printForDebug("UnknownTestFun Start======================================>")

        LoginFun(activityScenarioRule,2,{

        })
        SystemClock.sleep(2000)
        ConstantClass.printForDebug("UnknownTestFun  pressBack")
        //pressBack()
        onView(withId(R.id.app_title_back)).perform(click())
        SystemClock.sleep(5000)
        LoginFun(activityScenarioRule,2,{

            onView(withId(R.id.ap_edit_title_delete)).apply {
                check(matches(isDisplayed()))
                perform(click())
            }
            SystemClock.sleep(10000)
        })
        ConstantClass.printForDebug("UnknownTestFun END======================================>")
    }


    fun loginDialogTest(activityScenarioRule:ActivityScenarioRule<MainActivity>?,testType:Int,check:Boolean,errorPassTest:Boolean){
        SystemClock.sleep(1000)
        if(testType==-1){
            SystemClock.sleep(1000)
            onView(withId(R.id.cancel)).perform(click())
            SystemClock.sleep(1000)
            return
        }
        onView(withId(R.id.login_admin_name)).perform(typeText("admin"), closeSoftKeyboard())
        SystemClock.sleep(1000)
        if(errorPassTest){
            var password= "123"
            onView(withId(R.id.login_admin_password)).perform(clearText(), closeSoftKeyboard())
            SystemClock.sleep(500)
            onView(withId(R.id.ok)).perform(click())
            SystemClock.sleep(500)
            onView(withId(R.id.login_admin_password)).perform(typeText(password), closeSoftKeyboard())
        }else{
            var password= "1234"
            onView(withId(R.id.login_admin_password)).perform(typeText(password), closeSoftKeyboard())
        }

        if(check){
            onView(withId(R.id.keep_login_check)).perform(click())
            SystemClock.sleep(500)
            onView(withId(R.id.keep_login_check_txt)).perform(click())
            SystemClock.sleep(500)
            onView(withId(R.id.keep_login_check_txt)).perform(click())
          //  SystemClock.sleep(500)
        }
        SystemClock.sleep(1000)
        ConstantClass.printForDebug("loginDialogTest testType=${testType} ")
        if(testType==1){

            activityScenarioRule?.scenario?.onActivity {
                (frags_page.allList.pageFragment as DeviceAllListFragment).getViewPager2Adapter().deviceMyAllListFragment.getDeviceMyAllListAdapter()?.filterResult?.forEach {
                    ConstantClass.printForDebug("My List loginDialogTest testType 1 each mac=${it.mac}")
                        it?.operatorMode=1
                    it.WiFiRadio=0//for test coverage
                    it.heath=3
                }
                it.lanuchMain {
                    (frags_page.allList.pageFragment as DeviceAllListFragment).getViewPager2Adapter().deviceMyAllListFragment.getDeviceMyAllListAdapter()?.notifyDataSetChanged()
                }
               // it.getSophosDevice().forEach {}
            }
        }
        SystemClock.sleep(500)
        onView(withId(R.id.ok)).perform(click())
        SystemClock.sleep(6000)
     //   ConstantClass.printForDebug("loginDialogTest End")
    }



    fun APIDisableTestFun() {

        LoginFun(null,0,{
            onView(withId(R.id.ap_edit_disable_layout)).apply {
                check(matches(isDisplayed()))
                perform(click())
                SystemClock.sleep(1000)
                onView(withId(R.id.cancel)).perform(click())
                SystemClock.sleep(1000)
                perform(click())
                onView(withId(R.id.ok)).perform(click())
                SystemClock.sleep(80000)

                perform(click())
               // SystemClock.sleep(1000)
                //onView(withId(R.id.ok)).perform(click())
                SystemClock.sleep(80000)
            }
        })
    }


   // @Test
    fun changeText_newFilter(main: MainActivity) {
       main.let { activity->
           ConstantClass.printForDebug("changeText_newActivity ")
           // Type text and then press the button.
           onView(withId(R.id.device_all_list_new_edit_filter))?.let { newFilter->
               newFilter.perform(typeText("s1"), closeSoftKeyboard())
               ConstantClass.printForDebug("perform s1")
               newFilter.check { view, noViewFoundException ->
                   noViewFoundException?.printStackTrace()
                   ConstantClass.printForDebug("perform check")
                   if(view!=null){

                       kotlin.runCatching {
                          // Thread.sleep(100)
                       }
                       ConstantClass.printForDebug("perform new")
                       // newFilter.perform(typeText("new"), closeSoftKeyboard())
                       (view as TextView)?.apply {
                           activity.lanuchMain {
                               setText("new")
                               withContext(Dispatchers.IO){
                                   kotlin.runCatching {
                                       //Thread.sleep(500)
                                   }
                               }
                               setText("")


                               ConstantClass.printForDebug("ap_edit_clients_recycleview perform")
                             /*  onView(withId(R.id.ap_edit_clients_recycleview)).perform(
                                   RecyclerViewActions.actionOnItemAtPosition<ClientsAdapter.ViewHolder>(
                                       0,
                                       object : ViewAction {
                                           override fun getConstraints(): org.hamcrest.Matcher<View> {
                                               return isAssignableFrom(View::class.java)
                                           }

                                           override fun getDescription() = "test"

                                           override fun perform(uiController: UiController?, view: View?) {
                                               val rvView = view?.findViewById<ImageView>(R.id.device_list_next)
                                               rvView?.performClick()
                                               ConstantClass.printForDebug("device_list_next performClick ${if(rvView==null)"null" else "not null"}")
                                           }

                                       }
                                   )
                               )*/
                           }

                       }


                   }else ConstantClass.printForDebug("device_all_list_new_edit_filter null")
               }
           }
           ConstantClass.printForDebug("changeText_newActivity end->")
       }

       // onView(withId(R.id.activityChangeTextBtn)).perform(click())

        // This view is in a different Activity, no need to tell Espresso.
      //  onView(withId(R.id.show_text_view)).check(matches(withText(STRING_TO_BE_TYPED)))

    }




}

