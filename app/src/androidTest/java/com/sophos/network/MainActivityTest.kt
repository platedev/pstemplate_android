package com.sophos.network

import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.UiThreadTestRule
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.ALLListTest
import com.sophos.network.wireless.MainActivity
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


@LargeTest
@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class MainActivityTest {

  /*  @get:Rule
    val runtimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )*/
      init {
         // AccessibilityChecks.enable().setRunChecksFromRootView(true);
      }

   // private lateinit var activityRule: ActivityScenario<MainActivity>
    @get:Rule var activityRule = activityScenarioRule<MainActivity>()


    @Before
    fun setUp() {
       // activityRule = ActivityScenario.launch(MainActivity::class.java)
        activityRule.scenario.moveToState(Lifecycle.State.RESUMED)
        var time=System.currentTimeMillis()
        var totalWait=40000
        Thread.sleep(12000)
        activityRule.scenario.onActivity {
            while(System.currentTimeMillis()<time+totalWait)
            {
                var isBreak=false
                var devs=it.getSophosDevice()
                for (dev in devs){
                    isBreak=dev.isComplete()||isBreak
                }
                if(isBreak)break
                else Thread.sleep(3000)
            }


        }
        //Thread.sleep(15000)
    }

    fun toastMassage(msg:String){
        activityRule.scenario.onActivity {
            Toast.makeText(it, msg, Toast.LENGTH_LONG).show()
        }
    }


       @Test
        @Throws(InterruptedException::class)
        fun A_menuTest() {
            ConstantClass.printForDebug("testCastButtonDisplay")
           toastMassage("menuTest")

                var all = ALLListTest()
            activityRule.scenario.onActivity {
                it.lanuchIO {
                    all.pingTest()

                }
            }

             all.menuTest()

        }

           @Test
           @Throws(InterruptedException::class)
           fun B_ListTest() {
               toastMassage("ListTest")
               var all = ALLListTest()
               all.AllListTestFun(activityRule)

           }

         @Test
          @Throws(InterruptedException::class)
          fun C_LoginTest() {
             toastMassage("LoginTest")
              ConstantClass.printForDebug("LoginTest")

              var all = ALLListTest()

              all.LoginTestFun()

          }



          @Test
          @Throws(InterruptedException::class)
          fun D_APIGetTest() {
              toastMassage("APIGetTest")
              ConstantClass.printForDebug("APITest")
              var all = ALLListTest()
              all.APIGetTestFun()
          }

    @Test
    @Throws(InterruptedException::class)
    fun E_ClientsTestFun() {
        toastMassage("ClientsTestFun")
        var all = ALLListTest()
        all.ClientsTestFun()
    }

        @Test
        @Throws(InterruptedException::class)
        fun F_APISystemPingTestFun() {
            toastMassage("APISystemPingTestFun")
            var all = ALLListTest()
            all.APISystemPingTestFun()
             }


    @Test
    @Throws(InterruptedException::class)
    fun G_APIDayTestFun() {
        toastMassage("APIDayTestFun")
        var all = ALLListTest()
        all.APIDayTimeTestFun()
    }

     @Test
     @Throws(InterruptedException::class)
     fun H_WirelessBandTestFun() {
         toastMassage("WirelessBandTestFun")
         var all = ALLListTest()
         all.WirelessBandTestFun()
     }



          @Test
           @Throws(InterruptedException::class)
           fun I_CentralTest() {
              toastMassage("CentralTest")
               var all = ALLListTest()
                 when(! all.CentralTestFun(activityRule)){
                     //may not into central
                 }
           }

         @Test
         @Throws(InterruptedException::class)
         fun J_UnKnownTest() {
             toastMassage("UnKnownTest===============================>")
             var all = ALLListTest()
             all.UnknownTestFun(activityRule)
         }


         @Test
         @Throws(InterruptedException::class)
         fun K_DisableTest() {
             toastMassage("DisableTest")
             ConstantClass.printForDebug("DisableTest")
             var all = ALLListTest()
             all.APIDisableTestFun()
         }


    @Test
    @Throws(InterruptedException::class)
    fun Y_RebootTest() {
        toastMassage("RebootTest")
        ConstantClass.printForDebug("RebootTest")
        var all = ALLListTest()
        all.APIRebootTestFun()
    }

    @Test
    @Throws(InterruptedException::class)
    fun Z_APILanPortTestFun() {
        toastMassage("APILanPortTestFun")
        var all = ALLListTest()
        all.APILanPortTestFun()
    }


}