package com.sophos.network.library.ping

import android.util.Log
import com.acelink.library.utils.data.ConstantClass
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executors
import java.util.concurrent.Future


class Ping {
    companion object {

        fun getHost(host: String): Boolean {
            return exeSingleForResult(Callable<Boolean> {

                try {
                    // var  inetAddress = InetAddress.getByName(host)

                    val timeoutMs = 1500
                    val socket = Socket()
                    val socketAddress = InetSocketAddress(host, 53)

                    socket.connect(socketAddress, timeoutMs)
                    socket.close()
                    ConstantClass.printForDebug("ping socket.connect success")
                    return@Callable  true
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    var inetAddress = InetAddress.getByName(host)
                    inetAddress.apply {
                        var success= (isReachable(5000) ?: false)
                        ConstantClass.printForDebug("ping isReachable =${success}")
                        if(success) return@Callable success

                    }

                } catch (e: Exception) {

                    e.printStackTrace()
                }
                try {

                    var success=ping()
                    ConstantClass.printForDebug("ping() =${success}")
                    if(success) return@Callable success

                } catch (e: Exception) {
                    e.printStackTrace()
                }
                ConstantClass.printForDebug("ping fail")
                return@Callable false
            })?:false

        }

        fun ping():Boolean{
            return exeSingleForResult(Callable<Boolean> {
                var isConnected=false
                val process = Runtime.getRuntime().exec("/system/bin/ping -c 1 8.8.8.8")
                if (process.waitFor() == 0) isConnected = true else isConnected = false
                return@Callable isConnected
            })?:false
        }

        private fun <T, R> exeSingleForResult(callable: Callable<T>?): R? {
            val pool = Executors.newSingleThreadExecutor()
            val f: Future<*> = pool.submit(callable)
            try {
                return f.get() as R
            } catch (e: ExecutionException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            return null
        }

        /*
Returns the latency to a given server in mili-seconds by issuing a ping command.
system will issue NUMBER_OF_PACKTETS ICMP Echo Request packet each having size of 56 bytes
every second, and returns the avg latency of them.
Returns 0 when there is no connection
*/
        private val NUMBER_OF_PACKTETS=4
        fun getLatency(ipAddress: String): Double {
            return exeSingleForResult(Callable<Double> {
                val pingCommand = "/system/bin/ping -c ${NUMBER_OF_PACKTETS.toString()} ${ipAddress}"
                var inputLine = ""
                var avgRtt = 0.0
                try {
                    // execute the command on the environment interface
                    val process = Runtime.getRuntime().exec(pingCommand)
                    /*val  process = ProcessBuilder()// NPL no use
                        .command("/system/bin/ping", "-c ${NUMBER_OF_PACKTETS.toString()} ${ipAddress}")
                        .redirectErrorStream(true)
                        .start()*/
                    // gets the input stream to get the output of the executed command
                    val bufferedReader = BufferedReader(InputStreamReader(process.inputStream))
                    inputLine = bufferedReader.readLine()
                    while (inputLine != null) {
                        if (inputLine.length > 0 && inputLine.contains("avg")) {  // when we get to the last line of executed ping command
                            break
                        }
                        ConstantClass.printForDebug("readline=$inputLine}")
                        inputLine = bufferedReader.readLine()
                    }
                    // Extracting the average round trip time from the inputLine string
                    val afterEqual =
                        inputLine.substring(inputLine.indexOf("="), inputLine.length).trim { it <= ' ' }
                    val afterFirstSlash =
                        afterEqual.substring(afterEqual.indexOf('/') + 1, afterEqual.length).trim { it <= ' ' }
                    val strAvgRtt = afterFirstSlash.substring(0, afterFirstSlash.indexOf('/'))
                    avgRtt = java.lang.Double.valueOf(strAvgRtt)
                    return@Callable avgRtt
                } catch (e: Exception) {
                    Log.v("Ping", "getLatency: EXCEPTION")
                    e.printStackTrace()
                }
                return@Callable -1.0
            })?:-1.0
        }
    }


}