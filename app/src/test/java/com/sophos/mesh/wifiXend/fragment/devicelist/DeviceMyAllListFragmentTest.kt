package com.sophos.network.wireless.fragment.devicelist

import androidx.fragment.app.testing.launchFragment
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions

import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4

import com.sophos.network.wireless.gui.general.ProgressFragment
import com.google.common.truth.Truth.assertThat
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.DeviceMyAllListFragment
import com.sophos.network.R
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.LooperMode

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class DeviceMyAllListFragmentTest {

  @Test
  fun launchDialogFragmentAndVerifyUI() {
    // Use launchFragment to launch the dialog fragment in a dialog.
    val scenario = launchFragment<DeviceMyAllListFragment>()

    scenario.onFragment { fragment ->
      assertThat(fragment.context).isNotNull()
      assertThat(fragment.activity).isNotNull()


    }

    // Now use espresso to look for the fragment's text view and verify it is displayed.
    Espresso.onView(ViewMatchers.withId(R.id.device_all_list_my_edit_filter))
      ?.perform(ViewActions.typeText("s1"), ViewActions.closeSoftKeyboard())
     // .check(ViewAssertions.matches(ViewMatchers.withText("I am a fragment")));



  }

  @Test
  fun launchDialogFragmentEmbeddedToHostActivityAndVerifyUI() {
    // Use launchFragmentInContainer to inflate a dialog fragment's view into Activity's content view.
    val scenario = launchFragmentInContainer<ProgressFragment>()

    scenario.onFragment { fragment ->
      // Dialog is not created because you use launchFragmentInContainer and the view is inflated
      // into the Activity's content view.
      assertThat(fragment.dialog).isNull()
    }

    // Now use espresso to look for the fragment's text view and verify it is displayed.
    Espresso.onView(ViewMatchers.withId(R.id.txtMsg))
   //   .check(ViewAssertions.matches(ViewMatchers.withText("I am a fragment")));
  }
}
