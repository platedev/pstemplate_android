package com.acelink.library.sharepreference

import android.content.Context
import android.content.SharedPreferences


object SPUtils {
    private const val ID_LANGUAGE = "LANGUAGE"
    private const val ID_Biometric = "Biometric"
    private const val ID_Pincode = "pincode"
    const val MODE_LIST = 0
    const val MODE_LOCATION = 1
    @Synchronized
    private fun getSP(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }

    @Synchronized
    fun getBiometric(context: Context): Int {
        return getSP(context).getInt(ID_Biometric, 0)
    }

    @Synchronized
    fun setBiometric(context: Context, i: Int) {
        val sp: SharedPreferences = getSP(context)
        val editor: SharedPreferences.Editor = sp.edit()
        if (editor != null) {
            editor.putInt(ID_Biometric, i)
            editor.commit()
        }
    }


    @Synchronized
    fun getPincode(context: Context): Int {
        return getSP(context).getInt(ID_Pincode, 0)
    }

    @Synchronized
    fun setPincode(context: Context, i: Int) {
        val sp: SharedPreferences = getSP(context)
        val editor: SharedPreferences.Editor = sp.edit()
        if (editor != null) {
            editor.putInt(ID_Pincode, i)
            editor.commit()
        }
    }

}