package com.acelink.library.common.annotations

object Definitions {
    const val errorCode = "error_code"

    object CGI {
        const val LOGIN_CGI = "Login"
        const val SYSTEM_INFO = "/ap/info/system/basic"
        const val SYSTEM_ADMINISTRATION = "/ap/config/system/administration"
        const val CLIENTS = "/ap/info/wireless/clients?radio="
        const val KICK_CLIENT = "/ap/systemctl/wireless/kick_client?radio="
        const val CLIENT_MAC = "&client_mac="
        const val TRACEROUTE = "/ap/info/utils?tool=traceroute"
        const val PING = "/ap/info/utils?tool=ping"

        const val REBOOT = "/ap/systemctl/sysReboot"
        const val RELOAD = "/ap/systemctl/sysReload"
        const val LAN_PORT = "/ap/config/network/basic"
        const val DAY_TIME = "/ap/config/system/dateTime"
        const val WIRELESS_BAND = "/ap/config/wireless/basic?radio="
        const val WIRELESS_SECURITY = "/ap/config/wireless/SSIDSecurity?radio="
        const val WIRELESS_SECURITY_INDEX = "&ssid_index="

    }

    object Result {
        const val UNDEFINED = -1
        const val SUCCESS = 0
        const val FORMAT_ERROR = 1
        const val INVALID_TOKEN = 2
        const val EXPIRED_TOKEN = 3
        const val DUPLICATE_IP = 4
        const val ELSE_ISSUE = 10
        const val tokenUpToMax = 98
        const val timeout = 999
    }
}