package com.acelink.library.gson

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.acelink.library.gson.JsonHelper
import kotlin.Throws
import com.google.gson.JsonSyntaxException
import com.google.gson.JsonSerializer
import com.google.gson.JsonDeserializer
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonParseException
import com.google.gson.JsonDeserializationContext
import java.lang.reflect.Modifier
import java.lang.reflect.Type

object JsonHelper {
    /*--------------------------------
   * Normal case
   *-------------------------------*/
    fun createJson(`object`: Any): String {
        val gson = GsonBuilder().registerTypeAdapter(String::class.java, StringConverter())
            .excludeFieldsWithoutExposeAnnotation().serializeNulls().excludeFieldsWithModifiers(
            Modifier.PROTECTED
        ).create()
        return gson.toJson(`object`)
    }

    @Throws(JsonSyntaxException::class)
    fun <T> parseJson(json: String?, aClass: Class<T>): T? {
        if (json == null || json.isEmpty()) {
            return null
        }
        val gson = GsonBuilder().registerTypeAdapter(String::class.java, StringConverter())
            .serializeNulls().create()
        return gson.fromJson(json, aClass)
    }

    /*@Nullable public static <T> T parseJson(@Nullable String json, @NonNull Type aClass)
          throws JsonSyntaxException {
    if (json == null || json.isEmpty()) {
      return null;
    }

    Gson gson = new GsonBuilder().registerTypeAdapter(String.class, new StringConverter()).serializeNulls().create();

    return gson.fromJson(json, aClass);
  }         */
    /*@Nullable public static <T> T parseJsonNormal(@Nullable String json, @NonNull Type aClass)
          throws JsonSyntaxException {
    if (json == null || json.isEmpty()) {
      return null;
    }
    JsonReader reader = new JsonReader(new StringReader(json));
    reader.setLenient(true);// allow malformed JSON data.

    Gson gson = new GsonBuilder().serializeNulls().registerTypeAdapter(String.class, new StringConverter()).create();

    return gson.fromJson(reader, aClass);
  }*/
    /*--------------------------------
   * Using MAP
   *-------------------------------*/
    /* @NonNull public static <T> String createJsonWithDynamicKey(@NonNull Map<String, T> object) {
    Gson gson = new GsonBuilder().serializeNulls().create();

    return gson.toJson(object);
  }


  @Nullable public static <T> Map<String, T> parseJsonWithDynamicKey(@Nullable String json) {
    if (json == null || json.isEmpty()) {
      return null;
    }

    Gson gson = new GsonBuilder().serializeNulls().create();

    return gson.fromJson(json, new TypeToken<Map<String, T>>(){}.getType());
  }*/
    /*--------------------------------
   * Using TypeAdapter
   *-------------------------------*/
    /*
  @Nullable public static String createJsonWithTypeAdapter(@NonNull Object object, Class<?> aClass, TypeAdapter typeAdapter) {
    Gson gson = new GsonBuilder().serializeNulls()
        .registerTypeAdapter(aClass, typeAdapter)
        .create();

    return gson.toJson(object);
  }

  @Nullable public static <T> T parseJsonWithTypeAdapter(@Nullable String json, @NonNull Class<T> aClass, TypeAdapter typeAdapter) {
    if (json == null || json.isEmpty()) {
      return null;
    }

    Gson gson = new GsonBuilder().serializeNulls()
        .registerTypeAdapter(aClass, typeAdapter)
        .create();

    return gson.fromJson(json, aClass);
  }
*/
    class StringConverter : JsonSerializer<String>, JsonDeserializer<String> {
        override fun serialize(
            src: String,
            typeOfSrc: Type,
            context: JsonSerializationContext
        ): JsonElement {
            //  if ( src == null ) src="";
            return JsonPrimitive(src)
        }

        @Throws(JsonParseException::class)
        override fun deserialize(
            json: JsonElement, typeOfT: Type,
            context: JsonDeserializationContext
        ): String {
            return json.asJsonPrimitive.asString
        }
    }
}