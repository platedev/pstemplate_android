package com.acelink.library.easy_input_filter

import android.text.InputFilter.LengthFilter
import android.text.SpannableStringBuilder
import android.text.Spanned
import androidx.constraintlayout.motion.widget.Key.CUSTOM

import com.acelink.library.easy_input_filter.CharacterUtils.isDigit


class EasyInputFilter private constructor(maxLength: Int, private val acceptorType: CharacterType,
                                         // private val customAcceptedCharacters: String?
                                          ) : LengthFilter(maxLength) {

    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int,
                        dend: Int): CharSequence {
        return super.filter(source, start, end, dest, dstart, dend)
            ?: kotlin.run {
                /*return if (source is SpannableStringBuilder) {
                    for (i in end - 1 downTo start) {
                        val c = source[i]
                        if (!accepted(c) ) {
                            source.delete(i, i + 1)
                        }
                    }
                    source
                } else {*/
                    val builder = StringBuilder()
                    for (i in start until end) {
                        val c = source[i]
                        if (accepted(c) ) {
                            builder.append(c)
                        }
                    }
                return builder.toString()
               // }
            }
    }

    private fun accepted(c: Char): Boolean {
        return when (acceptorType) {
          /*  CharacterType.CUSTOM -> {
                var i = 0
                while (i < customAcceptedCharacters!!.length) {
                    if (customAcceptedCharacters[i] == c) {
                        return true
                    }
                    i++
                }

                false
            }*/
            CharacterType.DIGIT -> isDigit(c)

            else -> true
        }
    }



    class Builder {
        private var maxLength: Int
        private var acceptorType: CharacterType
       // private var customAcceptedCharacters: String? = null


        fun setAcceptorType(acceptorType: CharacterType): Builder {
            this.acceptorType = acceptorType
            return this
        }

        fun setMaxLength(maxLength: Int): Builder {
            this.maxLength = maxLength
            return this
        }


        fun build(): EasyInputFilter {
         //   if (acceptorType === CharacterType.CUSTOM) {
           //     require(!(customAcceptedCharacters == null || customAcceptedCharacters!!.length == 0)) { "You assigned acceptor type as [CUSTOM], but you assigned null or empty custom accepted character array." }
           // } else {
           //     require(customAcceptedCharacters == null) { "You assigned custom accepted character array, but the acceptor type is not [CUSTOM]." }
         //   }

            return EasyInputFilter(maxLength, acceptorType//, customAcceptedCharacters
            )
        }

        companion object {
            private const val DEFAULT_LENGTH = 8
        }

        init {
            maxLength = DEFAULT_LENGTH
            acceptorType = CharacterType.DIGIT

        }
    }
}