package com.acelink.library.easy_input_filter

object CharacterUtils {



    @JvmStatic
    fun isDigit(c: Char): Boolean {
        val code = c.code
        return '0'.code <= code && code <= '9'.code
    }




}