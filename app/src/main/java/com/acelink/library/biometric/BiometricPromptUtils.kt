/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.acelink.library.biometric

import android.hardware.biometrics.BiometricManager.Authenticators.BIOMETRIC_STRONG
import android.hardware.biometrics.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import android.hardware.biometrics.BiometricPrompt.*
import android.hardware.fingerprint.FingerprintManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.acelink.library.sharepreference.SPUtils
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.R


// Since we are using the same methods in more than one Activity, better give them their own file.
object BiometricPromptUtils {
    private const val TAG = "BiometricPromptUtils"
    const val BiometricReject=-1
    const val BiometricTooMany=-2
    const val BiometricDisable=-3
    enum class bioAuthType{
        biometric,device,biometric_device
    }

    fun createBiometricPrompt(
        activity: AppCompatActivity,block: (errorCode:Int,result: BiometricPrompt.AuthenticationResult?) -> Unit
    ): BiometricPrompt {
        val executor = ContextCompat.getMainExecutor(activity)

        val callback = object : BiometricPrompt.AuthenticationCallback() {
            var errorTime=0
            override fun onAuthenticationError(errCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errCode, errString)
                ConstantClass.printForDebug(TAG, "errCode is $errCode and errString is: $errString")
                if("Cancel" in errString || "cancel" in errString){
                    block.invoke(BiometricReject,null)
                    return
                }
                else if (errCode == BIOMETRIC_ERROR_CANCELED||
                    errCode ==BIOMETRIC_ERROR_USER_CANCELED)// cancel by code
                {

                    block.invoke(BiometricReject,null)
                }else if (errCode==BIOMETRIC_ERROR_LOCKOUT)//too many times
                {
                    block.invoke(BiometricTooMany,null)
                }else if (errCode==BIOMETRIC_ERROR_LOCKOUT_PERMANENT)//. Biometric authentication is disabled until the user unlocks with strong authenticatio
                {
                    block.invoke(BiometricDisable,null)
                }
                errorTime++
                ConstantClass.printForDebug(TAG, "User biometric onAuthenticationError. errorTime=${errorTime}")
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                errorTime++
                ConstantClass.printForDebug(TAG, "User biometric onAuthenticationFailed. errorTime=${errorTime}")

            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                Log.e(TAG, "Biometric Authentication was successful")
                block.invoke(0,result)
            }
        }
        return BiometricPrompt(activity, executor, callback)
    }

    fun createPromptInfo(activity: AppCompatActivity,type:bioAuthType): BiometricPrompt.PromptInfo =
        BiometricPrompt.PromptInfo.Builder().apply {

            //  setTitle(activity.getString(R.string.prompt_info_title))
          //  setSubtitle(activity.getString(R.string.prompt_info_subtitle))
          //  setDescription(activity.getString(R.string.prompt_info_description))
            setTitle(activity.getString(R.string.app_name))
            setDescription(activity.getString(R.string.prompt_info_subtitle))
            setConfirmationRequired(false)

            if(type==bioAuthType.biometric)setNegativeButtonText(activity.getString(R.string.btn_cancel))//need use or IllegalArgumentException
            when(type){
                bioAuthType.device->setAllowedAuthenticators(BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                bioAuthType.biometric_device->setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                else->setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG)
            }



        }.build()
}