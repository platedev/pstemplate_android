package com.acelink.library.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.LinkAddress
import android.net.LinkProperties
import android.net.NetworkCapabilities
import android.net.wifi.WifiManager
import android.os.Build
import android.util.Log
import androidx.core.app.ActivityCompat
import com.acelink.library.utils.data.ConstantClass
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*


object WiFiUtils {
    fun getPhoneWifiMac(activity: Context):String?{
        var wifiMan = activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE) as WifiManager
        var wifiInfo = wifiMan.connectionInfo
       /*if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
           == PackageManager.PERMISSION_GRANTED)
       {
           var macAddr =  wifiInfo.macAddress
           //return macAddr.replace(":","").uppercase()
           if(macAddr!=null&&!macAddr.endsWith("00")){
               return macAddr?.uppercase()
           }
        }*/

        try {
            val all: List<NetworkInterface> =
                Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (!nif.name.equals("wlan0", ignoreCase = true)) continue
                val macBytes = nif.hardwareAddress ?: return ""
                val res1 = StringBuilder()
                for (b in macBytes) {
                    // res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:", b))
                }
                if (res1.length > 0) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (ex: Exception) {
            //handle exception
        }
        return null


    }

    fun getPhoneWifiIP(context: Context):String?{
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            val wifiInf = (context.getSystemService(Context.WIFI_SERVICE) as WifiManager).connectionInfo
            val ipAddress = wifiInf.ipAddress
            val ip = String.format(
                "%d.%d.%d.%d",
                ipAddress and 0xff,
                ipAddress shr 8 and 0xff,
                ipAddress shr 16 and 0xff,
                ipAddress shr 24 and 0xff
            )
            return ip
        }else{
            val connectivityManager = context.getSystemService(ConnectivityManager::class.java)
            var wm = context.getApplicationContext().getSystemService(Context.WIFI_SERVICE) as WifiManager
            if(connectivityManager==null)return null
            for (network in connectivityManager.allNetworks) {
                val nc = connectivityManager.getNetworkCapabilities(network) ?: continue
                if (nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {

                    val prop: LinkProperties = connectivityManager.getLinkProperties(network)!!
                    val las: List<LinkAddress> = prop.getLinkAddresses()
                    for (la in las) {
                        val inetAddress: InetAddress = la.address
                        if (inetAddress is Inet4Address) {
                            ConstantClass.printForDebug( "getPhoneWifiIP "+prop.getInterfaceName() + ": " + inetAddress.getHostAddress());
                            return inetAddress.getHostAddress()
                        }
                    }
                    return prop.linkAddresses[0].address.getHostAddress()

                }
            }

        }

     /*   try {
            val en = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val intf = en.nextElement()
                val enumIpAddr: Enumeration<InetAddress> = intf.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress: InetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress() && inetAddress is Inet4Address) {
                        return inetAddress.getHostAddress()
                    }
                }
            }
        } catch (ex: SocketException) {
            ex.printStackTrace()
        }*/
        return null
    }



}