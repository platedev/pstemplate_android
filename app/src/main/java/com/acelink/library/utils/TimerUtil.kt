package com.android.viewer.utils

import java.util.*

class TimerUtil {

    private var myTimer: Timer? = null
    var isTiming = false
        private set

    fun startTimer(callBack: ()->Unit, delay: Long, period: Long) {
        removeMyTimer()
        if (period > 0) {
            myTimer = Timer()
            myTimer!!.schedule(object : TimerTask() {
                override fun run() {
                    callBack.invoke()
                    isTiming = false
                }
            }, if (delay > 0) delay else 0, period)
        } else if (delay > 0) {
            myTimer = Timer()
            myTimer!!.schedule(object : TimerTask() {
                override fun run() {
                    callBack.invoke()
                }
            }, delay)
            isTiming = true
        }
    }

    @Synchronized
    fun removeMyTimer() {

        if (myTimer != null) {
            myTimer!!.cancel()
            myTimer!!.purge()
            myTimer = null
        }
        isTiming = false
    }
}