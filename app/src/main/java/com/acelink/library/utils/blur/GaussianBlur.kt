package com.acelink.library.utils.blur

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.renderscript.RenderScript
import android.renderscript.Allocation
import android.renderscript.ScriptIntrinsicBlur
import android.view.View

class GaussianBlur(private val context: Context) {
    //private val DEFAULT_RADIUS = 25f


    companion object{
        fun getScreenshot(view: View): Bitmap {

            // ConstantClass.printForLog(getClass(),"wifiInfo getBitmapFromView getWidth="+view.getWidth());
                val bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
                val c = Canvas(bitmap)
                view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())
                view.draw(c)
               return bitmap

        }
    }

    fun renderView(v: View,radius:Float): Bitmap?{
        return getScreenshot(v).let { render(it,radius) }
    }

    //  fun renderDefault(bitmap: Bitmap, scaleDown: Boolean)=render(bitmap,DEFAULT_RADIUS,scaleDown)


    fun render(bitmap: Bitmap,radius:Float): Bitmap {
        var bitmap = bitmap
        val rs = RenderScript.create(context)

        val output = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val inAlloc = Allocation.createFromBitmap(
            rs,
            bitmap,
            Allocation.MipmapControl.MIPMAP_NONE,
            Allocation.USAGE_GRAPHICS_TEXTURE
        )
        val outAlloc = Allocation.createFromBitmap(rs, output)
        val script = ScriptIntrinsicBlur.create(rs, inAlloc.element) // Element.U8_4(rs));
        script.setRadius(radius)
        script.setInput(inAlloc)
        script.forEach(outAlloc)
        outAlloc.copyTo(output)
        rs.destroy()
        return output
    }

    //0-25f
   /* fun renderView(v: View,radius:Float, scaleDown: Boolean): Bitmap?{
      return getScreenshot(v)?.let { render(it,radius,scaleDown) }
    }

  //  fun renderDefault(bitmap: Bitmap, scaleDown: Boolean)=render(bitmap,DEFAULT_RADIUS,scaleDown)

   private val DEFAULT_RADIUS = 15f
    private val DEFAULT_MAX_IMAGE_SIZE = 400f
    var maxImageSize = 0f

    fun render(bitmap: Bitmap,radius:Float, scaleDown: Boolean): Bitmap {
        var bitmap = bitmap
        val rs = RenderScript.create(context)
        if (scaleDown) {
            bitmap = scaleDown(bitmap)
        }
        val output = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val inAlloc = Allocation.createFromBitmap(
            rs,
            bitmap,
            Allocation.MipmapControl.MIPMAP_NONE,
            Allocation.USAGE_GRAPHICS_TEXTURE
        )
        val outAlloc = Allocation.createFromBitmap(rs, output)
        val script = ScriptIntrinsicBlur.create(rs, inAlloc.element) // Element.U8_4(rs));
        script.setRadius(radius)
        script.setInput(inAlloc)
        script.forEach(outAlloc)
        outAlloc.copyTo(output)
        rs.destroy()
        return output
    }



    fun scaleDown(input: Bitmap): Bitmap {
        val ratio = Math.min(maxImageSize / input.width, maxImageSize / input.height)
        val width = Math.round(ratio * input.width)
        val height = Math.round(ratio * input.height)
        return Bitmap.createScaledBitmap(input, width, height, true)
    }

    init {
        maxImageSize = DEFAULT_MAX_IMAGE_SIZE
    }*/



}