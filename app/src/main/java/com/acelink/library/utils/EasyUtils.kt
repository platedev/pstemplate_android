package com.acelink.library.utils

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Build
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageButton
import androidx.annotation.DrawableRes
import java.lang.Exception
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*
import java.util.regex.Pattern

object EasyUtils {
        /**
         * Set image button params.
         *
         * @param btn resource view
         * @param id image resource id
         * @param visible view visibility
         * @param enabled view enable ability
         */
       /* fun buttonSetting(
            btn: ImageButton, @DrawableRes id: Int, visible: Int,
            enabled: Boolean
        ) {
            btn.setImageResource(id)
            btn.visibility = visible
            btn.isEnabled = enabled
        }
        /**
         * Format the value that less than ten, add zero in front of it.
         */
        fun addZero(value: Int): String {
            return if (value < 10) {
                "0$value"
            } else value.toString()
        }

        /**
         * Convert dp to pixel by device.
         *
         * @return int
         */
        fun convertDpToPixel(context: Context, dp: Int): Int {
            val density = context.resources.displayMetrics.density
            return Math.round(dp.toFloat() * density)
        }

        /**
         * Sets transformation method to the target edit text
         * [PasswordTransformationMethod], [HideReturnsTransformationMethod]
         *
         * @param editText target edit text
         */
        fun setTransformationMethod(editText: EditText) {
            val method = editText.transformationMethod
            if (method == null || method !is PasswordTransformationMethod) {
                editText.transformationMethod = PasswordTransformationMethod.getInstance()
            } else {
                editText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }
        }







        fun getMacWithColon(mac: String?): String {
            val stringBuilder = StringBuilder()
            if (mac != null) {
                val length = mac.length
                for (i in 0 until length) {
                    stringBuilder.append(mac[i])
                    if (i % 2 != 0 && i < length - 1) {
                        stringBuilder.append(":")
                    }
                }
            }
            return stringBuilder.toString()
        }

        fun removeColonSign(mac: String?): String {
            val stringBuilder = StringBuilder()
            if (mac != null) {
                val length = mac.length
                for (i in 0 until length) {
                    if (mac[i] == ':') {
                        // Skip the colon sign
                        continue
                    }
                    stringBuilder.append(mac.toUpperCase()[i])
                }
            }
            return stringBuilder.toString()
        }

        fun containsIgnoreCase(haystack: String?, needle: String?): Boolean {
            if (needle == "") return true
            if (haystack == null || needle == null || haystack == "") return false
            val p = Pattern.compile(needle, Pattern.CASE_INSENSITIVE + Pattern.LITERAL)
            val m = p.matcher(haystack)
            return m.find()
        }*/


    /*
          fun getWiFiManager(context: Context): WifiManager {
              return context.getSystemService(Context.WIFI_SERVICE) as WifiManager
          }

       fun <T : View?> setElevation(view: T, Elevation: Int) {
              if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                  view!!.elevation = Elevation.toFloat()
              }
          }

          fun showKeyBoard(context: Context, editText: EditText) {
              editText.postDelayed({
                  editText.requestFocus()
                  editText.setSelection(editText.text.length)
                  val imm =
                      context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                  //imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                  imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED)
              }, 200)
          }

          fun showHiddenKeyBoard(context: Context, editText: EditText) {
              editText.postDelayed({
                  editText.requestFocus()
                  editText.setSelection(editText.text.length)
                  val imm =
                      context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                  imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
              }, 200)
          }

          fun closeKeybord(context: Context, editText: EditText) {
              val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
              imm.hideSoftInputFromWindow(editText.windowToken, 0)
          }*/

        /** 获得IP信息  */
       /* val oneIPV4: String
            get() {
                val list = ArrayList<String>()
                var result = "ip get failed"
                try {
                    val en: Enumeration<*> = NetworkInterface.getNetworkInterfaces()
                    while (en.hasMoreElements()) {
                        val intf = en.nextElement() as NetworkInterface
                        val enumIpAddr: Enumeration<*> = intf.inetAddresses
                        while (enumIpAddr.hasMoreElements()) {
                            val inetAddress = enumIpAddr.nextElement() as InetAddress
                            if (inetAddress is Inet4Address) {
                                if (!inetAddress.isLoopbackAddress()) {
                                    list.add(inetAddress.getHostAddress())
                                }
                            }
                        }
                    }
                } catch (ex: SocketException) {
                }
                if (list.size > 0) result = list[0]
                return result
            }*/

        fun tryCatchBlock (block: () -> Unit):Boolean{
            try {
                block.invoke()
               return true
            }catch (e: Exception){
                e.printStackTrace()
                return  false
            }
        }

        fun <T> tryCatchBlock(block:() -> T?,default: T?):T?{
            try {
              return  block.invoke()

            }catch (e: Exception){
                e.printStackTrace()

            }
            return default
        }
    }

