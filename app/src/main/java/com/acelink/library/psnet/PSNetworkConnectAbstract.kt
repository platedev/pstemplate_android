package com.acelink.library.psnet

import android.annotation.SuppressLint
import android.util.Log
import com.acelink.library.utils.data.ConstantClass
import com.burgstaller.okhttp.AuthenticationCacheInterceptor
import com.burgstaller.okhttp.CachingAuthenticatorDecorator
import com.burgstaller.okhttp.digest.CachingAuthenticator
import com.burgstaller.okhttp.digest.Credentials
import com.burgstaller.okhttp.digest.DigestAuthenticator
import com.sophos.network.BuildConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import java.net.HttpURLConnection
import java.net.URL
import java.security.KeyManagementException
import java.security.KeyStore
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

enum class PSNetworkMethod{
    GET,PUT,POST;

}

abstract class  PSNetworkConnectAbstract<T> {
    companion object{
        val connTimeoutTime=5000
        val recvTimeoutTime=10000
        val recvTimeoutTime20s=20000
    }
    /*suspend fun postConnect(url: String, file: String?,  connTimeoutTime: Int, recvTimeoutTime: Int): HttpResult<T> {
      return  asyn{requestJson(url,file,connTimeoutTime,recvTimeoutTime)}
    }

    suspend fun postConnect(url: String, file: String?): HttpResult<T> {
        ConstantClass.printForDebug("postConnect url=${url}\n file=${file}")
       return asyn{requestJson(url,file, connTimeoutTime, recvTimeoutTime)}
    }

    suspend fun connect(url: String,  connTimeoutTime: Int, recvTimeoutTime: Int): HttpResult<T> {
       return asyn{requestJson(url,null,connTimeoutTime,recvTimeoutTime)}
    }

    suspend  fun connect(url: String): HttpResult<T> {

        return asyn{ requestJson(url,null, connTimeoutTime, recvTimeoutTime) }
    }

    private fun requestJson(url: String, file: String?,  connTimeoutTime: Int, recvTimeoutTime: Int): HttpResult<T> {

        var huc: HttpURLConnection? = null
        try {

            huc=createConnection(url,file,connTimeoutTime,recvTimeoutTime)
            file?.run {
                huc.doInput = true
                huc.doOutput = true
                huc.requestMethod = "POST"
                huc.instanceFollowRedirects = true
                huc.setRequestProperty("Content-Type", "application/json")
                huc.setRequestProperty("charset", "utf-8")
                huc.setRequestProperty("Content-Length", file.length.toString())
                huc.outputStream.write(file.toByteArray(Charsets.UTF_8))
                huc.outputStream.flush()
                huc.outputStream.close()
            }
            val str =getConnResule(huc)
            huc.inputStream.close()
            huc?.disconnect()
            return HttpResult.Success(str,huc.responseCode)
        }
        catch (e: Exception) {
            e.printStackTrace()
            huc?.disconnect()
            return HttpResult.Error(e)

        }

    }*/



    suspend  fun connectDigest(url: String,username:String,password:String): HttpResult<String> {

        return asynString{ requestDigest(url,username,password,PSNetworkMethod.GET,null, connTimeoutTime, recvTimeoutTime) }
    }

    suspend  fun connectDigestPost(url: String,file:String,username:String,password:String): HttpResult<String> {

        return asynString{ requestDigest(url,username,password,PSNetworkMethod.POST,file, connTimeoutTime, recvTimeoutTime) }
    }

    suspend  fun connectDigestPost20s(url: String,file:String,username:String,password:String): HttpResult<String> {

        return asynString{ requestDigest(url,username,password,PSNetworkMethod.POST,file, connTimeoutTime, recvTimeoutTime20s) }
    }

    suspend  fun connectDigestPUT(url: String,username:String,password:String): HttpResult<String> {

        return asynString{ requestDigest(url,username,password,PSNetworkMethod.PUT,null, connTimeoutTime, recvTimeoutTime) }
    }

    suspend  fun testPUT(url: String): HttpResult<String> {
        ConstantClass.printForDebug("123","testPUT")
        return asynString{ requestDigest(url,"admin","1234",PSNetworkMethod.PUT,"{}", connTimeoutTime, recvTimeoutTime) }
    }

    /* private fun disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate?>? {
                    return null
                }

                override fun checkClientTrusted(
                    certs: Array<X509Certificate?>?,
                    authType: String?
                ) {
                }

                override fun checkServerTrusted(
                    certs: Array<X509Certificate?>?,
                    authType: String?
                ) {
                }
            }
            )

            // Install the all-trusting trust manager
            val sc = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCerts, SecureRandom())
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)

            // Create all-trusting host name verifier
            val allHostsValid = HostnameVerifier { hostname, session -> true }

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
            e.printStackTrace()
        }
    }*/

  /* fun asyn()= GlobalScope.launch(Dispatchers.IO) {

    }*/

    private suspend fun asynString(http:()-> HttpResult<String>): HttpResult<String> = coroutineScope {
        withContext(Dispatchers.IO) {
            val a = async { http.invoke() }
            a.await() //+ b.await() + c.await()
        }
    }



    /*private suspend fun asyn(vararg http:()->String):String = coroutineScope {
        withContext(Dispatchers.IO) {
           var result=""
            for (h in http)
            {
                val a = async {h.invoke() }
                result+a.await()
            }
            result
        }
    }                 */

    private fun requestDigest(url: String,username:String,password:String, method: PSNetworkMethod,file:String?,  connTimeoutTime: Int, recvTimeoutTime: Int): HttpResult<String> {

        try {
            val authenticator = DigestAuthenticator(Credentials(username, password))

            val authCache: Map<String, CachingAuthenticator> = ConcurrentHashMap()
            val client: OkHttpClient = OkHttpClient.Builder().apply {
                authenticator(CachingAuthenticatorDecorator(authenticator, authCache))
                addInterceptor(AuthenticationCacheInterceptor(authCache))
                connectTimeout(connTimeoutTime.toLong(), TimeUnit.MILLISECONDS)
                readTimeout(recvTimeoutTime.toLong(), TimeUnit.MILLISECONDS)
                //writeTimeout(WRITE_TIMEOUT, TimeUnit.MILLISECONDS)

                val fac=SSLContext.getInstance(listOf("TLSv1.2","TLSv1.1")[0]).apply {
                    val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                        override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                            return arrayOf()
                        }
                        @SuppressLint("TrustAllX509TrustManager")
                        override fun checkClientTrusted(
                            chain: Array<out java.security.cert.X509Certificate>?,
                            authType: String?
                        ) {

                        }
                        @SuppressLint("TrustAllX509TrustManager")
                        override fun checkServerTrusted(
                            chain: Array<out java.security.cert.X509Certificate>?,
                            authType: String?
                        ) {

                        }
                    })
                    init(null, trustAllCerts, SecureRandom())
                }.socketFactory
                val trustManagerFactory = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm()
                )
                //NULL is app cert。
                trustManagerFactory.init(null as KeyStore?)
                val trustManagers = trustManagerFactory.trustManagers
                check(!(trustManagers.size != 1 || trustManagers[0] !is X509TrustManager)) {
                    ("Unexpected default trust managers:${Arrays.toString(trustManagers)}")
                }

                sslSocketFactory(fac , trustManagers[0] as X509TrustManager)
                hostnameVerifier{ _, _ -> true }
                if(BuildConfig.DEBUG){
                    addInterceptor(HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                        override fun log(message: String) {
                            Log.d("OKHTTP", message)
                        }
                    }).apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                }
            }
                .build()
            when(method){
                PSNetworkMethod.GET->{
                    val request: Request = Request.Builder()
                        .url(url)
                        .get()
                        .build()
                    val response: Response = client.newCall(request).execute()
                    val responseBody = response.body!!.string()
                    ConstantClass.printForDebug("response.code=${response.code}")
                    return HttpResult.Success(responseBody,response.code)
                }
                PSNetworkMethod.POST->{
                    val request: Request = Request.Builder()
                        .url(url)
                        .post(file!!.toRequestBody("application/json".toMediaTypeOrNull()))
                        .build()
                    val response = client.newCall(request).execute()
                    val responseBody = response.body!!.string()

                    return HttpResult.Success(responseBody,response.code)
                }
                PSNetworkMethod.PUT->{
                    val request: Request = Request.Builder()
                        .url(url)
                        .put(file.run {
                            if (this!=null){
                              return@run  toRequestBody("application/json".toMediaTypeOrNull())
                            }  else{
                                return@run  "".toRequestBody()
                            }

                        })
                        .build()
                    val response: Response = client.newCall(request).execute()
                    val responseBody = response.body!!.string()
                    ConstantClass.printForDebug("response.code=${response.code}")
                    return HttpResult.Success(responseBody,response.code)
                }
            }


        }
        catch (e: Exception) {
            e.printStackTrace()
            //huc?.disconnect()
            return HttpResult.Error(e)

        }
    }


    /*
   open abstract fun getConnResule(huc: HttpURLConnection):T


   private fun createConnection(url:String, file: String?,  connTimeoutTime: Int, recvTimeoutTime: Int):HttpURLConnection{
       var huc: HttpURLConnection? = null
       if (url.indexOf("https") >= 0) {
           huc = URL(url).openConnection() as HttpsURLConnection
           huc.sslSocketFactory = createSocketFactory(listOf("TLSv1.2"))
           huc.hostnameVerifier = HostnameVerifier { _, _ -> true }
       }
       else {
           huc = URL(url).openConnection() as HttpURLConnection
       }
       huc.useCaches = false

       huc.connectTimeout = connTimeoutTime
       huc.readTimeout = recvTimeoutTime


       return huc
   }

   private fun createSocketFactory(protocols: List<String>) =
       SSLContext.getInstance(protocols[0]).apply {
           val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
               override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                   return arrayOf()
               }
               @SuppressLint("TrustAllX509TrustManager")
               override fun checkClientTrusted(
                   chain: Array<out java.security.cert.X509Certificate>?,
                   authType: String?
               ) {

               }
               @SuppressLint("TrustAllX509TrustManager")
               override fun checkServerTrusted(
                   chain: Array<out java.security.cert.X509Certificate>?,
                   authType: String?
               ) {

               }
           })
           init(null, trustAllCerts, SecureRandom())
       }.socketFactory      */



}