package com.acelink.library.psnet

sealed class HttpResult<out R> {
    data class Success<out T>(val data: T,val code: Int) : HttpResult<T>()
    data class Error(val exception: Exception) : HttpResult<Nothing>()
}