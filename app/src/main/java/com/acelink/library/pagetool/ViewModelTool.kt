package com.sophos.network.wireless.pagetool

import android.app.Dialog
import android.view.LayoutInflater
import androidx.activity.ComponentActivity
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

object ViewModelTool {

    /**
     * Returns a [Lazy] delegate to access the ComponentActivity's ViewModel, if [factoryProducer]
     * is specified then [ViewModelProvider.Factory] returned by it will be used
     * to create [ViewModel] first time.

     * This property can be accessed only after the Activity is attached to the Application,
     * and access prior to that will result in IllegalArgumentException.
     */
    @MainThread
     inline fun <reified VM : ViewModel> ComponentActivity.viewModels(
        noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null
    ): Lazy<VM> {
        val factoryPromise = factoryProducer ?: {
            defaultViewModelProviderFactory
        }

        return ViewModelLazy(VM::class, { viewModelStore }, factoryPromise)
    }

    @MainThread
     inline fun <reified VM : ViewModel> Fragment.viewModels(
        noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null
    ): Lazy<VM> {
        val factoryPromise = factoryProducer ?: {
            defaultViewModelProviderFactory
        }

        return ViewModelLazy(VM::class, { viewModelStore }, factoryPromise)
    }


}