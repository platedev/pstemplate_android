package com.sophos.network.wireless.pagetool

import com.sophos.network.wireless.gui.APUnknownFragment
import com.sophos.network.wireless.gui.BaseFragment
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.central.APCentralFragment
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment

import com.sophos.network.wireless.gui.general.WelcomeFragment
import com.sophos.network.wireless.gui.system.AboutFragment
import com.sophos.network.wireless.gui.system.SettingsFragment

enum class frags_page {
    wlecome,leaveWelcome
    ,allList,ap_edit,ap_central,about,settings,unknown;
    var pageFragment: BaseFragment?=null
    var mTag:String?=null

    fun getInstance():BaseFragment{
       return when(this){
            wlecome->WelcomeFragment.getInstance().apply {
                    mTag=WelcomeFragment.mTag
                    pageFragment=this
            }
           allList-> DeviceAllListFragment.getInstance().apply {
                     mTag= DeviceAllListFragment.mTag
                    pageFragment=this
           }
           ap_edit-> APEditFragment.getInstance().apply {
                    mTag=APEditFragment.mTag
                    pageFragment=this
           }
           ap_central-> APCentralFragment.getInstance().apply {
                   mTag=APCentralFragment.mTag
                   pageFragment=this
           }
           about-> AboutFragment.getInstance().apply {
                   mTag=AboutFragment.mTag
                   pageFragment=this
           }
           settings-> SettingsFragment.getInstance().apply {
               mTag=SettingsFragment.mTag
               pageFragment=this
           }
           unknown-> APUnknownFragment.getInstance().apply {
               mTag=APUnknownFragment.mTag
               pageFragment=this
           }
            else->throw  NoSuchMethodException()

        }
    }

    fun getTag():String{
        return mTag!!
    }
}