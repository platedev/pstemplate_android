package com.sophos.network.wireless.psclass.gson.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.acelink.library.gson.JsonHelper
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SophosDeviceItem{

    @Expose @SerializedName("name") var name:String?=null
    @Expose @SerializedName("enable24g") var enable24g:Int?=null
    @Expose @SerializedName("enable5g") var enable5g:Int?=null
    @Expose @SerializedName("enable6g") var enable6g:Int?=null
    @Expose @SerializedName("operatorMode") var operatoer:Int?=null

    fun json(): String {
        return JsonHelper.createJson(this)
    }

    override fun toString(): String {
        return "SophosDeviceItem(name=$name, enable24g=$enable24g, enable5g=$enable5g, enable6g=$enable6g, operatoer=$operatoer)"
    }


}