package com.sophos.network.wireless.gui

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.sophos.network.wireless.pagetool.frags_page
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.R
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments

import com.sophos.network.databinding.FragApUnknownBinding
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_clients_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_day_time_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_lan_port_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_ping_test_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_system_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_traceroute_test_page
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments.Companion.edit_wireless_band_page
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment

import com.sophos.network.wireless.psclass.sharepreference.device.SophosAccount

class APUnknownFragment : BaseFragment() {
    companion object{
        fun getInstance(): APUnknownFragment {
            return  APUnknownFragment()
        }
       val mTag = "APUnknownFragment"
    }

    val binding by viewBinding(FragApUnknownBinding::inflate)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug(mTag,"onCreateView ${javaClass.simpleName} ")


        getMainActivity()!!.updateTitleRightVisable(View.GONE)

        updateTitleUI()


        return binding.root
    }

    private fun updateTitleUI(){
        getCurrentEditDevice()!!.apply {
            binding.apEditTitleName.setText(deviceName)
            binding.apEditTitleName.isSelected=true
            binding.apEditTitleMac.setText(mac)
            binding.apEditTitleClintCount.setText("0 ${getString(R.string.connected_clients)}")
            binding.apEditTitleDelete.setOnClickListener {
                ConstantClass.printForDebug("apEditTitleDelete")
                SophosAccount.deleteDevice(mac!!)

                val device= getSophosDevice()
                val news=device!!.find { it.mac==mac }
                 news?.new=true

                (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
                    notificationAllSlaveUpdate()
                    getViewPager2Adapter().deviceMyAllListFragment.startScan(false)
                }
                onBackPress()

            }
        }

    }



    override fun onBackPress() {
        super.onBackPress()

         getMainActivity()!!.changeMainPage(frags_page.allList)


    }

    override fun onSelect() {
        ConstantClass.printForDebug("${mTag} onSelect APEditFragment")
        super.onSelect()
    }

    override fun onDeSelect() {

        super.onDeSelect()
    }




}