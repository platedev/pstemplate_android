package com.sophos.network.wireless.psclass.enum

import com.acelink.library.gson.JsonHelper
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.R
import com.sophos.network.wireless.psclass.enum.Radio.Companion.G6DefaultAuth
import com.sophos.network.wireless.psclass.gson.data.GetSecurity
import org.json.JSONObject

enum class Authentication_Type {
    none,
    //wpa_wpa2,
    wpa2_aes, wpa2_wpa3, wpa3_aes,
   // wpa_wpa2_eap,
    wpa2_eap_aes,
    wpa3_eap_aes,
    owe,
    wep;//WEP not support

    fun getStringRes(): Int = when (this) {
        none -> R.string.none
      //  wpa_wpa2 -> R.string.wpa_wpa2
        wpa2_aes -> R.string.wpa2_aes
        wpa2_wpa3 -> R.string.wpa2_wpa3
        wpa3_aes -> R.string.wpa3_aes//6G support
    //    wpa_wpa2_eap -> R.string.wpa_wpa2_eap
        wpa2_eap_aes -> R.string.wpa2_eap_aes
        wpa3_eap_aes -> R.string.wpa3_eap_aes//6G support
        owe -> R.string.owe//6G support
        wep -> R.string.wep
    }

    fun getSecurityData(index: Int, type: Authentication_Type): GetSecurity.SecurityData {
        var default = GetSecurity.SecurityData.getDefaultSecurityData(index).apply {
            GetSecurity.AuthDetail.setAuthDetailData(this, type)
        }
        return default

    }

    fun creatEncrypyJson(data: GetSecurity.SecurityData): String {
        return when (data.auth_method) {
            0,5 -> data.run {//none or own
                var json = JsonHelper.createJson(this)
                var jsonObject = JSONObject(json)
                jsonObject.remove("auth_detail")

                jsonObject.put("auth_detail", JSONObject("{}"))
                jsonObject.toString()
            }
            else -> JsonHelper.createJson(data)
        }
    }

    fun needPassword():Boolean{
        return this!=none&&this!=owe&&this!=wep
    }
        /*
      None = [auth_method=0]
       WPA/WPA2 Mixed Mode-AES = [auth_method=2][wpa_type=8][encrypt_type=3]
      WPA2-AES = [auth_method=2][wpa_type=5][encrypt_type=3]
      WPA2/WPA3 Mixed Mode-AES = [auth_method=2][wpa_type=11][encrypt_type=3]
      WPA3-AES = [auth_method=2][wpa_type=10][encrypt_type=3]

      WPA/WPA2 Mixed Mode-EAP-AES = [auth_method=3][wpa_type=8][encrypt_type=3]
      WPA2-EAP-AES = [auth_method=3][wpa_type=5][encrypt_type=3]
      WPA3-EAP-AES = [auth_method=3][wpa_type=10][encrypt_type=3]
       OWE = [auth_method=5]
      * */
        companion object {
            fun getAuth(securityDara: GetSecurity.SecurityData,radio: Radio): Authentication_Type {
                var defaultSelect:(r:Radio)->Authentication_Type={
                    when(it){
                        Radio.G6 ->G6DefaultAuth//wpa3_aes
                        else->none
                    }
                }
                return when (securityDara.auth_method) {
                    0 -> none
                    1->wep
                    5 -> owe
                    else->{
                        securityDara?.auth_detail?.run{
                            when (securityDara.auth_method) {
                                2 ->  {
                                    // if (wpa_type == 8 && encrypt_type == 3) wpa_wpa2
                                    if (wpa_type == 5 && encrypt_type == 3) wpa2_aes
                                    else if (wpa_type == 11 && encrypt_type == 3) wpa2_wpa3
                                    else if (wpa_type == 10 && encrypt_type == 3) wpa3_aes
                                    else defaultSelect.invoke(radio)
                                }
                                3 ->{
                                    // if (wpa_type == 8 && encrypt_type == 3) wpa_wpa2_eap
                                    if (wpa_type == 5 && encrypt_type == 3) wpa2_eap_aes
                                    else if (wpa_type == 10 && encrypt_type == 3) wpa3_eap_aes
                                    else defaultSelect.invoke(radio)
                                }
                                else -> defaultSelect.invoke(radio)
                            }
                        } ?: kotlin.run {
                            ConstantClass.printForDebug("auth_method=${securityDara.auth_method} auth_detail null")
                            defaultSelect.invoke(radio)
                        }
                    }

                }
            }

            fun setAuth(security: GetSecurity.SecurityData, type: Authentication_Type) {
                security.apply {

                    when (type) {
                        none -> this.auth_method = 0
                       /* wpa_wpa2 -> {
                            this.auth_method = 2
                            this.auth_detail!!.wpa_type = 8
                            this.auth_detail!!.encrypt_type = 3
                        }*/
                        wpa2_aes -> {
                            this.auth_method = 2
                            this.auth_detail!!.wpa_type = 5
                            this.auth_detail!!.encrypt_type = 3
                        }
                        wpa2_wpa3 -> {
                            this.auth_method = 2
                            this.auth_detail!!.wpa_type = 11
                            this.auth_detail!!.encrypt_type = 3
                        }
                        wpa3_aes -> {
                            this.auth_method = 2
                            this.auth_detail!!.wpa_type = 10
                            this.auth_detail!!.encrypt_type = 3
                        }
                      /*  wpa_wpa2_eap -> {
                            this.auth_method = 3
                            this.auth_detail!!.wpa_type = 8
                            this.auth_detail!!.encrypt_type = 3
                        }*/
                        wpa2_eap_aes -> {
                            this.auth_method = 3
                            this.auth_detail!!.wpa_type = 5
                            this.auth_detail!!.encrypt_type = 3
                        }
                        wpa3_eap_aes -> {
                            this.auth_method = 3
                            this.auth_detail!!.wpa_type = 10
                            this.auth_detail!!.encrypt_type = 3
                        }
                        owe -> this.auth_method = 5

                    }
                }
            }

        }

}
