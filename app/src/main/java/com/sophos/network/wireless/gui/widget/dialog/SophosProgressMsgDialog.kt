package com.sophos.network.wireless.gui.widget.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.sophos.network.R

import com.sophos.network.databinding.SophosDialogProgressMsgBinding

class SophosProgressMsgDialog
    (
    private val con: Context,
   private val mTitle: String,
   private val mMSG: String,
    private val result:(dialog:Dialog)->Unit
) : Dialog(con, R.style.MyDialog), OnShowListener {

    private val binding by viewBinding(SophosDialogProgressMsgBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // ConstantClass.printForDebug("OwnYesNoDialog " )
        window?.apply { attributes.windowAnimations = R.style.DialogAnimationScaletIn }

        setCancelable(false)
        setCanceledOnTouchOutside(true)
        setContentView(binding.root)
        setOnShowListener(this)
    }

    override fun onShow(arg0: DialogInterface) {
        mTitle.apply {
            binding.yesnoTitle.text = this
        }/*?:run{
            binding.yesnoTitle.visibility=View.GONE//no use
        }*/

        binding.yesnoMsg.text = mMSG
        setOnDismissListener { result.invoke(this) }
    }

    fun updateMsg(msg:String){
        binding.yesnoMsg.text = msg
    }


}