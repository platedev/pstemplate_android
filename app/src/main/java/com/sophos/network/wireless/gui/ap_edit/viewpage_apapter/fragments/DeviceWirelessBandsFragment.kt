package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.acelink.library.utils.data.ConstantClass

import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.acelink.library.pagetool.CoroutineIO.contextMain
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.utils.StringUtils
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.adapter.WirelessBandAdapter
import com.sophos.network.wireless.gui.widget.dialog.SophosSmallChooseDialog
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.R
import com.sophos.network.databinding.FragApEditWirelessBandsBinding
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.ap_edit.APEditFragment.Companion.updateValueResulPartialtFail
import com.sophos.network.wireless.gui.ap_edit.APEditFragment.Companion.updateValueResultFullFail
import com.sophos.network.wireless.gui.ap_edit.APEditFragment.Companion.updateValueResultNoChange
import com.sophos.network.wireless.gui.ap_edit.APEditFragment.Companion.updateValueResultReturn
import com.sophos.network.wireless.gui.ap_edit.APEditFragment.Companion.updateValueResultSuccess
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag.BaseSubEditFragment
import com.sophos.network.wireless.gui.widget.dialog.SophosMessageDialog
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.wireless.psclass.enum.Authentication_Type
import com.sophos.network.wireless.psclass.enum.Radio
import com.sophos.network.wireless.psclass.gson.data.*
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

enum class WirelessBandsPageMode{
    Normal,Edit
}

class DeviceWirelessBandsFragment : BaseSubEditFragment(),View.OnClickListener {
    companion object{
        fun getInstance(): DeviceWirelessBandsFragment {
            return  DeviceWirelessBandsFragment()
        }
       val mTag = "DeviceWirelessBandsFragment"

    }

    val binding by viewBinding(FragApEditWirelessBandsBinding::inflate)

    var wirelessBandsPageMode=WirelessBandsPageMode.Normal

    var is24GOn=true
    var is5GOn=true
    var is6GOn=true
    var num24G=0
    var num5G=0
    var num6G=0

    var support6G=false
    private set

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")

        binding.itemWireless24gBands.layoutManager=object: LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        };
        binding.itemWireless5gBands.layoutManager=object: LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        };
        binding.itemWireless6gBands.layoutManager=object: LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        };
        support6G=getCurrentEditDevice()?.isSupport6G()?:false
        binding.itemWireless24gBands.adapter=WirelessBandAdapter(this,arrayListOf(),Radio.G24)
        binding.itemWireless5gBands.adapter=WirelessBandAdapter(this,arrayListOf(),Radio.G5)
        binding.itemWireless6gBands.adapter=WirelessBandAdapter(this,arrayListOf(),Radio.G6)
        updateWirelessBandsPageMode()

        return binding.root
    }

    private fun get24GAdapter()= binding.itemWireless24gBands.adapter as WirelessBandAdapter

    private fun get5GAdapter()= binding.itemWireless5gBands.adapter as WirelessBandAdapter

    private fun get6GAdapter()= binding.itemWireless6gBands.adapter as WirelessBandAdapter

    fun updateWirelessBandsPageMode(){

            if(support6G){
                binding.apEdit6gParent.visibility=View.VISIBLE
            }else{
                binding.apEdit6gParent.visibility=View.GONE
            }

        when(wirelessBandsPageMode){
            WirelessBandsPageMode.Normal->{

                ContextCompat.getColor(requireContext(),R.color.gray_edit).apply {
                   // binding.apEditWirelless24gTxt.setTextColor(this)
                    binding.apEditWirelless24gRadio.setTextColor(this)

                   // binding.apEditWirelless5gTxt.setTextColor(this)
                    binding.apEditWirelless5gRadio.setTextColor(this)


                  //  binding.apEditWirelless6gTxt.setTextColor(this)
                    if(support6G)binding.apEditWirelless6gRadio.setTextColor(this)

                   /* binding.apEdit24gNumTitle.setTextColor(this)
                    binding.apEdit24gNumTxt.setTextColor(this)
                    binding.apEdit5gNumTitle.setTextColor(this)
                    binding.apEdit5gNumTxt.setTextColor(this)
                    if(support6G){
                        binding.apEdit6gNumTitle.setTextColor(this)
                        binding.apEdit6gNumTxt.setTextColor(this)
                    }*/

                }

                binding.apEdit24gNumTxt.text=num24G.toString()
                binding.apEdit5gNumTxt.text=num5G.toString()

                binding.apEdit24gNumDropdown.visibility=View.INVISIBLE
                binding.apEdit24gNumDropdown.setOnClickListener(null)
                binding.apEdit24gNumTxt.setOnClickListener(null)
                binding.apEdit5gNumDropdown.visibility=View.INVISIBLE
                binding.apEdit5gNumDropdown.setOnClickListener(null)
                binding.apEdit5gNumTxt.setOnClickListener(null)

                binding.apEditWirelless24gCheck.setImageResource(if(is24GOn)R.mipmap.on_disable else R.mipmap.off_disable)
                binding.apEditWirelless5gCheck.setImageResource(if(is5GOn)R.mipmap.on_disable else R.mipmap.off_disable)

                ConstantClass.printForDebug("updateWirelessBandsPageMode is6GOn=${is6GOn}")
                binding.apEditWirelless24gRadioIcon.setImageResource(if(is24GOn)R.mipmap.ap_enable_light else R.mipmap.radio_disable_off)
                binding.apEditWirelless5gRadioIcon.setImageResource(if(is5GOn)R.mipmap.ap_enable_light else R.mipmap.radio_disable_off)
                binding.apEditWirelless24gCheck.setOnClickListener(null)
                binding.apEditWirelless5gCheck.setOnClickListener(null)
                get24GAdapter().notifyDataSetChanged()
                get5GAdapter().notifyDataSetChanged()

                if(support6G){
                    binding.apEdit6gNumTxt.text=num6G.toString()
                    binding.apEditWirelless6gCheck.setImageResource(if(is6GOn)R.mipmap.on_disable else R.mipmap.off_disable)
                    binding.apEdit6gNumDropdown.visibility=View.INVISIBLE
                    binding.apEdit6gNumDropdown.setOnClickListener(null)
                    binding.apEdit6gNumTxt.setOnClickListener(null)
                    binding.apEditWirelless6gRadioIcon.setImageResource(if(is6GOn)R.mipmap.ap_enable_light else R.mipmap.radio_disable_off)
                    binding.apEditWirelless6gCheck.setOnClickListener(null)
                    get6GAdapter().notifyDataSetChanged()
                }

            }
            WirelessBandsPageMode.Edit->{


                ContextCompat.getColor(requireContext(),R.color.table_color).apply {

                    binding.apEditWirelless24gRadio.setTextColor(this)
                    binding.apEditWirelless5gRadio.setTextColor(this)
                    binding.apEditWirelless6gRadio.setTextColor(this)

                    binding.apEdit24gNumTitle.setTextColor(this)
                    binding.apEdit24gNumTxt.setTextColor(this)

                    binding.apEdit5gNumTitle.setTextColor(this)
                    binding.apEdit5gNumTxt.setTextColor(this)
                    if(support6G)
                    {
                        binding.apEdit6gNumTitle.setTextColor(this)
                        binding.apEdit6gNumTxt.setTextColor(this)
                    }

                }

                binding.apEditWirelless24gCheck.setImageResource(if(is24GOn)R.mipmap.on_enable else R.mipmap.off_enable)
                binding.apEditWirelless5gCheck.setImageResource(if(is5GOn)R.mipmap.on_enable else R.mipmap.off_enable)

                binding.apEditWirelless24gCheck.setOnClickListener {
                    is24GOn=!is24GOn
                    binding.apEditWirelless24gCheck.setImageResource(if(is24GOn)R.mipmap.on_enable else R.mipmap.off_enable)
                    binding.apEditWirelless24gRadioIcon.setImageResource(if(is24GOn)R.mipmap.ap_enable else R.mipmap.ap_disable)
                }
                binding.apEditWirelless5gCheck.setOnClickListener(null)
                binding.apEditWirelless5gCheck.setOnClickListener {
                    is5GOn=!is5GOn
                    binding.apEditWirelless5gCheck.setImageResource(if(is5GOn)R.mipmap.on_enable else R.mipmap.off_enable)
                    binding.apEditWirelless5gRadioIcon.setImageResource(if(is5GOn)R.mipmap.ap_enable else R.mipmap.ap_disable)
                }

                binding.apEdit24gNumTxt.text=num24G.toString()
                binding.apEdit5gNumTxt.text=num5G.toString()

                binding.apEditWirelless24gRadioIcon.setImageResource(R.mipmap.ap_enable)
                binding.apEditWirelless5gRadioIcon.setImageResource(R.mipmap.ap_enable)

                binding.apEdit24gNumDropdown.visibility=View.VISIBLE
                binding.apEdit24gNumDropdown.setOnClickListener(this)
                binding.apEdit24gNumTxt.setOnClickListener(this)
                binding.apEdit5gNumDropdown.visibility=View.VISIBLE
                binding.apEdit5gNumDropdown.setOnClickListener(this)
                binding.apEdit5gNumTxt.setOnClickListener(this)

                get24GAdapter()?.notifyDataSetChanged()
                get5GAdapter()?.notifyDataSetChanged()

                if(support6G)
                {
                    binding.apEditWirelless6gCheck.setImageResource(if(is6GOn)R.mipmap.on_enable else R.mipmap.off_enable)
                    binding.apEditWirelless6gCheck.setOnClickListener(null)
                    binding.apEditWirelless6gCheck.setOnClickListener {
                        is6GOn=!is6GOn
                        binding.apEditWirelless6gCheck.setImageResource(if(is6GOn)R.mipmap.on_enable else R.mipmap.off_enable)
                        binding.apEditWirelless6gRadioIcon.setImageResource(if(is6GOn)R.mipmap.ap_enable else R.mipmap.ap_disable)
                    }
                    binding.apEditWirelless6gRadioIcon.setImageResource(R.mipmap.ap_enable)
                    binding.apEdit6gNumTxt.text=num6G.toString()
                    binding.apEdit6gNumDropdown.visibility=View.VISIBLE
                    binding.apEdit6gNumDropdown.setOnClickListener(this)
                    binding.apEdit6gNumTxt.setOnClickListener(this)
                    get6GAdapter().notifyDataSetChanged()
                }

            }
        }
    }

    fun bandList()= getCurrentEditDevice()?.bandList()!!

    override fun onDestroyView() {
        super.onDestroyView()
    }

    var num24GDialog:SophosSmallChooseDialog<Int>?=null
    private set
    var num5GDialog:SophosSmallChooseDialog<Int>?=null
        private set
    var num6GDialog:SophosSmallChooseDialog<Int>?=null
        private set

    override fun onClick(v: View) {
       when(v.id){

           R.id.ap_edit_24g_num_txt,
           R.id.ap_edit_24g_num_dropdown->{
               ConstantClass.printForDebug("ap_edit_24g_num_txt 2.4G Click ")
               num24GDialog=SophosSmallChooseDialog<Int>(requireActivity(),"",bandList().toMutableList(),0,false,{dialog,pos->
                   dialog.dismiss()
                   (requireActivity() as MainActivity).blurMainFrame(false)

                   var band=bandList().get(pos)

                   num24G=band
                   binding.apEdit24gNumTxt.text=band.toString()

                   var currentSets=get24GAdapter().getDataSet()
                   ConstantClass.printForDebug("SophosChooseDialog 2.4G size=${currentSets?.size} num24G= ${num24G}")
                   updateBandNum(currentSets!!,num24G,Radio.G24)


               }).let {
                   it.isAdjustHeight=true
                   it.adjustHeightPercent=40
                   it.setOnDismissListener {
                       binding.apEdit24gNumDropdown.setImageResource(R.mipmap.drop_down)
                       (requireActivity() as MainActivity).blurMainFrame(false)
                   }
                   it.show()
                   it
               }
               binding.apEdit24gNumDropdown.setImageResource(R.mipmap.drop_up)
               (requireActivity() as MainActivity).blurMainFrame(true)
           }
           R.id.ap_edit_5g_num_txt,
           R.id.ap_edit_5g_num_dropdown->{

               num5GDialog=SophosSmallChooseDialog<Int>(requireActivity(),"",bandList().toMutableList(),0,false,{dialog,pos->
                   var band=bandList().get(pos)
                   num5G=band
                   ConstantClass.printForDebug("SophosChooseDialog 5G ${band}")
                   binding.apEdit5gNumTxt.text=band.toString()
                   dialog.dismiss()
                   (requireActivity() as MainActivity).blurMainFrame(false)
                   var currentSets=get5GAdapter()!!.getDataSet()
                   updateBandNum(currentSets!!,num5G,Radio.G5)
               }).apply {
                   isAdjustHeight=true
                   adjustHeightPercent=40
                   setOnDismissListener {
                       binding.apEdit5gNumDropdown.setImageResource(R.mipmap.drop_down)
                       (requireActivity() as MainActivity).blurMainFrame(false)
                   }
                   show()
               }
               binding.apEdit5gNumDropdown.setImageResource(R.mipmap.drop_up)
               (requireActivity() as MainActivity).blurMainFrame(true)
           }
           R.id.ap_edit_6g_num_txt,
           R.id.ap_edit_6g_num_dropdown->{

               num6GDialog=SophosSmallChooseDialog<Int>(requireActivity(),"",bandList().toMutableList(),0,false,{dialog,pos->
                   var band=bandList().get(pos)
                   num6G=band
                   ConstantClass.printForDebug("SophosChooseDialog 6G ${band}")
                   binding.apEdit6gNumTxt.text=band.toString()
                   dialog.dismiss()
                   (requireActivity() as MainActivity).blurMainFrame(false)
                   var currentSets=get6GAdapter().getDataSet()
                   updateBandNum(currentSets!!,num6G,Radio.G6)
               }).apply {
                   isAdjustHeight=true
                   adjustHeightPercent=40
                   setOnDismissListener {
                       binding.apEdit6gNumDropdown.setImageResource(R.mipmap.drop_down)
                       (requireActivity() as MainActivity).blurMainFrame(false)
                   }
                   show()
               }
               binding.apEdit6gNumDropdown.setImageResource(R.mipmap.drop_up)
               (requireActivity() as MainActivity).blurMainFrame(true)
           }
       }
    }

    private fun updateBandNum( currentSets:MutableList<GetWirelessBands.WirelessName>,num:Int,radio: Radio){
        if(currentSets?.size?.run { num>this&&this>=1 }?:false){
            currentSets!!.addAll(MutableList<GetWirelessBands.WirelessName>(num-currentSets!!.size,{j->
                //    ConstantClass.printForDebug("WirelessName add pos=${(currentSets?.size!!+j)}")
                GetWirelessBands.WirelessName().apply {
                    ssidname = currentSets.get(0).ssidname?.run {
                        var ssid= StringBuffer(this)


                       /* if(ssid.get(len-1)=='0'){
                            var str=(currentSets?.size!!+j).toString()
                            ssid.setCharAt(len-1,str.get(0).toChar())
                            if(str.length==2)
                                ssid.append(str.get(1))
                        }else{*/
                            ssid.append("_${(currentSets?.size!!+j)}")
                       // }
                        ssid.toString()
                    }?:"ssid${currentSets?.size!!+j}"
                    index= (currentSets?.size!!+j)
                    vlan_id=1

                    if(radio==Radio.G6){
                        auth=Authentication_Type.wpa3_aes
                        pass="123456789012345"
                    }else{
                        auth=Authentication_Type.wpa2_aes
                        pass="123456789012345"
                    }

                }
            }))
            ConstantClass.printForDebug("WirelessName updateDataSet =${(currentSets?.size!!)} ")
            /* var newSet= MutableList<GetWirelessBands.WirelessName>(0){it->GetWirelessBands.WirelessName()}
             newSet.addAll(currentSets)
           get24GAdapter()?.updateDataSet(newSet,num24G)*/
            updateRadioSize(radio,num)

        }else{
            updateRadioSize(radio,num)
        }
    }

    private fun updateRadioSize(radio: Radio,num:Int)=when(radio){
        Radio.G24-> get24GAdapter().updateSize(num)
        Radio.G5-> get5GAdapter().updateSize(num)
        Radio.G6-> get6GAdapter().updateSize(num)
    }

    fun onSelect(){
        lanuchIO {
            getBands()
        }
    }

    suspend fun getBands(){
        getMainActivity()?.onShowProgress(null)
        ConstantClass.printForDebug("getBands")
        viewModel()?.getBands {
            lanuchMain {
                Radio.values().forEach{ radio->
                    radio.valueBand?.data?.clientList?.apply {
                      //  ConstantClass.printForDebug("radio=${radio} radio_enable=${radio.valueBand?.data?.radio_enable}")

                        when(radio){
                            Radio.G24->{
                                is24GOn=radio.valueBand?.data?.radio_enable==1
                                num24G= radio.valueBand?.data?.ssid_num_enable!!
                                binding.itemWireless24gBands.adapter?.apply {
                                    get24GAdapter().updateDataSet(toMutableList(),size)

                                }?: kotlin.run {
                                    binding.itemWireless24gBands.adapter=WirelessBandAdapter(this@DeviceWirelessBandsFragment,toMutableList(),Radio.G24)
                                }
                            }
                            Radio.G5->{
                                is5GOn=radio.valueBand?.data?.radio_enable==1
                                num5G= radio.valueBand?.data?.ssid_num_enable!!
                                binding.itemWireless5gBands.adapter?.apply {
                                    get5GAdapter().updateDataSet(toMutableList(),size)

                                }?: kotlin.run {
                                    binding.itemWireless5gBands.adapter=WirelessBandAdapter(this@DeviceWirelessBandsFragment,toMutableList(),Radio.G5)
                                }
                            }
                            Radio.G6->{
                                if(support6G){
                                    is6GOn=radio.valueBand?.data?.radio_enable==1
                                    num6G= radio.valueBand?.data?.ssid_num_enable!!
                                    binding.itemWireless6gBands.adapter?.apply {

                                        get6GAdapter().updateDataSet(toMutableList(),size)

                                    }?: kotlin.run {
                                        binding.itemWireless6gBands.adapter=WirelessBandAdapter(this@DeviceWirelessBandsFragment,toMutableList(),Radio.G6)
                                    }
                                }

                            }
                        }
                    }
                }

                updateWirelessBandsPageMode()
                getMainActivity()?.onHideProgress()
            }

        }

    }


    fun checkingPasswordRight(name:GetWirelessBands.WirelessName):Boolean{
        ConstantClass.printForDebug("checkingPasswordRight ${name.pass}")
        var main=getMainActivity()!!
       return  if(name.auth.needPassword()&&name.pass?.length?:0<8)
         {

                lifecycleScope.launch(contextMain){
                    SophosMessageDialog(requireContext(),null,
                        getString(R.string.preshared_key_illege),{dialog ->

                        }).apply {
                        setOnDismissListener {
                            main.blurMainFrame(false)
                        }
                        show()
                        main.blurMainFrame(true)
                    }
                }
                false


        }else if(name.auth.needPassword()&&StringUtils.hasTwoBytes(name.pass!!)){
          ConstantClass.printForDebug("password invalid_character ${name.pass}")
           lanuchMain {
               SophosMessageDialog(requireContext(),null,
                   getString(R.string.invalid_character),{dialog ->

                   }).apply {
                   setOnDismissListener {
                       main.blurMainFrame(false)
                   }
                   show()
                   main.blurMainFrame(true)
               }
           }
           false
       }else true

    }

    fun checkingSSIDRight(name:GetWirelessBands.WirelessName):Boolean{
     return   if(name.ssidname.isNullOrEmpty()|| name.ssidname!!.isBlank())
        {

            lifecycleScope.launch(contextMain){
                SophosMessageDialog(requireContext(),null,
                    getString(R.string.enter_ssid),{dialog ->

                    }).apply {
                    setOnDismissListener {
                        getMainActivity()?.blurMainFrame(false)
                    }
                    show()
                    getMainActivity()?.blurMainFrame(true)
                }
            }
            false


        }else if(StringUtils.hasTwoBytes(name.ssidname!!)){
         lifecycleScope.launch(contextMain){
             ConstantClass.printForDebug("ssidname invalid_character ${name.ssidname}")
             SophosMessageDialog(requireContext(),null,
                 getString(R.string.invalid_character),{dialog ->

                 }).apply {
                 setOnDismissListener {
                     getMainActivity()?.blurMainFrame(false)
                 }
                 show()
                 getMainActivity()?.blurMainFrame(true)
             }
         }
         false
        }else true

    }

    suspend fun updateValue():Int{
        kotlin.runCatching {

            /*2.4G*/
            var g24 = Radio.G24.valueBand!!.data!!

            var new24GSets = get24GAdapter().getNewDataSet()
            var new5GSets = get5GAdapter().getNewDataSet()
            if(support6G) {
                var new6GSets = get6GAdapter().getNewDataSet()
                new6GSets.forEach {
                    if(!checkingPasswordRight(it)||!checkingSSIDRight(it)){
                        ConstantClass.printForDebug("6G password ${it} lengh to low")
                        return@updateValue updateValueResultReturn
                    }
                }
            }

            new24GSets.forEach {
                if(!checkingPasswordRight(it)||!checkingSSIDRight(it)){
                    ConstantClass.printForDebug("24G password ${it} lengh to low")
                    return@updateValue updateValueResultReturn
                }
            }
            new5GSets.forEach {
                if(!checkingPasswordRight(it)||!checkingSSIDRight(it)){
                    ConstantClass.printForDebug("5G password ${it} lengh to low")
                    return@updateValue updateValueResultReturn
                }
            }

            var old24GSets =g24?.clientList
            var change=false
            var same24G = true

            if(new24GSets.size!=old24GSets?.size?:0){
                same24G = false
            }else{
                for ((i, value) in new24GSets?.withIndex()!!) {

                    if (!old24GSets?.get(i)!!.equalName(value)) {
                        same24G = false
                        break
                    }
                }
            }

            if (g24.ssid_num_enable != num24G || g24.radio_enable != (if (is24GOn) 1 else 0) || same24G == false) {
                change=true
                var newG24 = g24.copy()
                newG24.clientList = new24GSets.toTypedArray()
                newG24.ssid_num_enable=num24G
                newG24.radio_enable=(if (is24GOn) 1 else 0)
                ConstantClass.printForDebug("newG24=${newG24}")
                var result24G = SophosNetwork.retryAsynBoolean {
                    runBlocking {
                        SophosNetwork.setBand(getCurrentEditDevice()!!, Radio.G24, newG24)
                    }
                }
                ConstantClass.printForDebug("setBand result24G=${result24G.await()}")
               if(!result24G.await()) return updateValueResultFullFail
            }

            for ((i, value) in new24GSets?.withIndex()!!) {
                if (i>=old24GSets?.size?:0||!old24GSets?.get(i)!!.equalAuth(value)) {
                    change=true
                   var data=value.auth.getSecurityData(i,value.auth).apply {

                       auth_detail?.psk_value=value.pass
                   }
                    var json=value.auth.creatEncrypyJson(data)
                    ConstantClass.printForDebug("new24GSets security i=${i} type=${value.auth} " +
                            //"\nold=${if(i>=old24GSets?.size?:0)"null" else old24GSets?.get(i)}" +
                            //"\n new=${value} " +
                            "json=${json}")
                    SophosNetwork.retryAsynBoolean {
                        runBlocking {
                            SophosNetwork.setSecurity( getCurrentEditDevice()!!,Radio.G24,i,json)
                        }
                    }

                }
            }


            /*5G*/
            var g5 = Radio.G5.valueBand!!.data!!

            //var old5GSets = get5GAdapter()?.getDataSet()
            var old5GSets =g5?.clientList
            var same5G = true

            if(new5GSets.size!=old5GSets?.size?:0){
                same5G = false
            }else{
                for ((i, value) in new5GSets?.withIndex()!!) {
                    if (!old5GSets?.get(i)!!.equalName(value)) {
                        same5G = false
                        break
                    }
                }
            }


            if (g5.ssid_num_enable != num5G || g5.radio_enable != (if (is5GOn) 1 else 0) || same5G == false) {
                change=true
                var newG5 = g5.copy()
                newG5.clientList = new5GSets.toTypedArray()
                newG5.ssid_num_enable=num5G
                newG5.radio_enable=(if (is5GOn) 1 else 0)
                var result5G= SophosNetwork.retryAsynBoolean {
                    runBlocking {
                        SophosNetwork.setBand(getCurrentEditDevice()!!, Radio.G5, newG5)
                    }
                }
                ConstantClass.printForDebug("result5G=${result5G.await()}")
                 if(!result5G.await())return updateValueResulPartialtFail
            }

            for ((i, value) in new5GSets?.withIndex()!!) {
                if (i>=old5GSets?.size?:0||!old5GSets?.get(i)!!.equalAuth(value)) {
                    change=true
                    var data=value.auth.getSecurityData(i,value.auth).apply {
                        auth_detail?.psk_value=value.pass
                    }
                    var json=value.auth.creatEncrypyJson(data)
                    ConstantClass.printForDebug("new5GSets security i=${i} json=${json}")
                    SophosNetwork.retryAsynBoolean {
                        runBlocking {
                            SophosNetwork.setSecurity( getCurrentEditDevice()!!,Radio.G5,i,json)
                        }
                    }
                }
            }

            /*6G*/
            if(support6G){
                var g6 = Radio.G6.valueBand!!.data!!
                var new6GSets = get6GAdapter().getNewDataSet()
               // var old6GSets = get6GAdapter()?.getDataSet()
                var old6GSets =g6?.clientList
                var same6G = true

                if(new6GSets.size!=old6GSets?.size?:0){
                    same6G = false
                }else{
                    for ((i, value) in new6GSets?.withIndex()!!) {
                        if (!old6GSets?.get(i)!!.equalName
                                (value)) {
                            same6G = false
                            break
                        }
                    }
                }


                if (g6.ssid_num_enable != num5G || g6.radio_enable != (if (is5GOn) 1 else 0) || same6G == false) {
                    change=true
                    var newG6 = g6.copy()
                    newG6.clientList = new6GSets.toTypedArray()
                    var result6G= SophosNetwork.retryAsynBoolean {
                        runBlocking {
                            SophosNetwork.setBand(getCurrentEditDevice()!!, Radio.G6, newG6)
                        }
                    }
                    ConstantClass.printForDebug("setBand result6G=${result6G.await()}")
                    if(!result6G.await())return updateValueResulPartialtFail
                }


                for ((i, value) in new6GSets?.withIndex()!!) {
                    ConstantClass.printForDebug("new6GSets old auth =${old6GSets?.get(i)!!.auth} new auth=${value.auth} old size=${old6GSets?.size?:0}")
                    if (i>=old6GSets?.size?:0||!old6GSets?.get(i)!!.equalAuth(value)) {
                        change=true
                        var data=value.auth.getSecurityData(i,value.auth).apply {
                            auth_detail?.psk_value=value.pass
                        }
                        var json=value.auth.creatEncrypyJson(data)
                        ConstantClass.printForDebug("new6GSets security i=${i} json=${json}")
                        SophosNetwork.retryAsynBoolean {
                            runBlocking {
                                SophosNetwork.setSecurity( getCurrentEditDevice()!!,Radio.G6,i,json)
                            }
                        }
                    }
                }
            }

            if(change)
            {
                var reload= SophosNetwork.setReload(getCurrentEditDevice()!!)
                ConstantClass.printForDebug("setSystemInfo Reload=${reload} success xxxxxxxxxxxxxxxxxxxx")

                if(reload!=null&&reload>=0)   {
                    viewModel()!!.countReload(reload)
                    delay((reload*1000).toLong())
                    viewModel()!!.removeCountReload()
                }else{
                    ConstantClass.printForDebug("setBand Reload fail xxxxxxxxxxxxxxxxxxxx")
                    return@updateValue updateValueResulPartialtFail
                }
            }else {
                ConstantClass.printForDebug("setBand update value no change")
                return@updateValue updateValueResultNoChange
            }


        }.onFailure { it.printStackTrace() }

        return@updateValue updateValueResultSuccess
    }


}