package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.text.InputFilter
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.UiThread
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.acelink.library.easy_input_filter.CharacterType
import com.acelink.library.easy_input_filter.EasyInputFilter
import com.acelink.library.pagetool.BaseBindingHolder
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.MainActivity


import com.sophos.network.wireless.gui.widget.dialog.SophosChooseDialog
import com.sophos.network.R
import com.sophos.network.databinding.ItemWirelessBandsBinding
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.DeviceWirelessBandsFragment
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.WirelessBandsPageMode
import com.sophos.network.wireless.gui.widget.dialog.SophosSmallChooseDialog
import com.sophos.network.wireless.psclass.SophosEditableStype
import com.sophos.network.wireless.psclass.enum.Authentication_Type
import com.sophos.network.wireless.psclass.enum.Radio
import com.sophos.network.wireless.psclass.gson.data.GetWirelessBands

class WirelessBandAdapter (private val fragment: DeviceWirelessBandsFragment, private var dataSet: MutableList<GetWirelessBands.WirelessName>, val radio: Radio) :
    RecyclerView.Adapter<WirelessBandAdapter.ViewHolder>() {

    var viewSize=0

    init {
        viewSize=dataSet.size
    }

    lateinit var parent: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        parent=recyclerView
        super.onAttachedToRecyclerView(recyclerView)
    }

    inner class ViewHolder(var vb: ItemWirelessBandsBinding) : BaseBindingHolder<ItemWirelessBandsBinding>(vb) {
         var authType= Authentication_Type.none
        var authDialog: Dialog ?=null
        private set

            fun bindItem(value:GetWirelessBands.WirelessName,position:Int){
                bindingHolder.apEditSsidTitle.setText("${fragment.getText(R.string.ssid)}${position+1}")
                bindingHolder.apEditSsid.setText(value.ssidname)
                bindingHolder.apEditSsid.isSelected=true
                authType=value.auth
                bindingHolder.apEditAuthenticationType.setText(value.auth.getStringRes())
                bindingHolder.apEditVlan.setText(value.vlan_id.toString())
                bindingHolder.apEditVlan.filters = arrayOf<InputFilter>(
                    EasyInputFilter.Builder()//.setMaxLength(CustomConstants.NEW_VERSION_LENGTH)
                        .setAcceptorType(CharacterType.DIGIT).setMaxLength(8).build()
                )
                if(position%2!=0){
                    bindingHolder.apEditBandParent.setBackgroundResource(R.drawable.background_top_gray_dd)
                }else{
                    bindingHolder.apEditBandParent.setBackgroundResource(R.drawable.card_edge_secondary)
                }
                updateWirelessBandsPageMode(value)
                /*if(position==itemCount-1){
                    (binding.root.getLayoutParams() as (ViewGroup.MarginLayoutParams)).bottomMargin=0
                }*/
            }

        fun updateWirelessBandsPageMode(value:GetWirelessBands.WirelessName){

            fragment.apply{
                when(wirelessBandsPageMode){
                    WirelessBandsPageMode.Normal->{
                        bindingHolder.apEditSsid.isEnabled=false
                        bindingHolder.apEditVlan.isEnabled=false
                        bindingHolder.apEditAuthenticationType.isEnabled=false
                        bindingHolder.apEditAuthenticationType.setOnClickListener(null)
                        bindingHolder.apEditAuthenticationTypeImg.visibility= View.INVISIBLE

                        SophosEditableStype.disableEditText(bindingHolder.apEditSsid)
                        SophosEditableStype.disableEditText(bindingHolder.apEditVlan)
                        ContextCompat.getColor(requireContext(),android.R.color.black).apply {
                            bindingHolder.apEditSsid.setTextColor(this)
                            bindingHolder.apEditVlan.setTextColor(this)
                            bindingHolder.apEditAuthenticationType.setTextColor(this)
                        }
                        updateEditPresharedKey(value)
                    }
                    WirelessBandsPageMode.Edit->{
                        bindingHolder.apEditSsid.isEnabled=true
                        bindingHolder.apEditVlan.isEnabled=true
                        bindingHolder.apEditAuthenticationTypeImg.visibility= View.VISIBLE
                        bindingHolder.apEditAuthenticationTypeImg.setImageResource( R.mipmap.drop_down)

                        bindingHolder.apEditAuthenticationType.isEnabled=true
                        var authtypeLisener=object :View.OnClickListener{
                            override fun onClick(p0: View?) {
                                /*   val authList = arrayOf(getString(R.string.wpa_wpa2),getString(R.string.wpa2_aes),getString(R.string.wpa2_wpa3),
                               getString(R.string.wpa3_aes),getString(R.string.wpa_wpa2_eap),getString(R.string.wpa2_eap_aes),
                               getString(R.string.wpa3_eap_aes),getString(R.string.owe)
                           )*/

                                var auths=Authentication_Type.values()
                                var list=auths.toMutableList()
                                list.remove(Authentication_Type.wep)
                                if(radio==Radio.G6){
                                    list.remove(Authentication_Type.none)
                                    list.remove(Authentication_Type.wpa2_aes)
                                    list.remove(Authentication_Type.wpa2_wpa3)
                                    list.remove(Authentication_Type.wpa2_eap_aes)
                                }
                                var pos=0
                                for ((i,data) in auths.withIndex()){
                                    if(authType==data){
                                        pos=i
                                        break
                                    }
                                }

                                authDialog=SophosChooseDialog<Authentication_Type>(requireActivity(),getString(R.string.choose_authentication),list,pos,true,true,{dialog,auth->
                                    ConstantClass.printForDebug("SophosChooseDialog choose ${auth}")
                                    var enc=list.get(auth)
                                    // binding.itemWireless24gBands.apEditAuthenticationType.setText(getString(auth))
                                    authType=enc
                                    bindingHolder.apEditAuthenticationType.setText(enc.getStringRes())
                                    updateEditPresharedKey(value)
                                    dialog.dismiss()
                                    (requireActivity() as MainActivity).blurMainFrame(false)
                                }).apply {
                                    setOnDismissListener {
                                        bindingHolder.apEditAuthenticationTypeImg.setImageResource(R.mipmap.drop_down)
                                        (requireActivity() as MainActivity).blurMainFrame(false)
                                    }
                                       show()
                                }
                                bindingHolder.apEditAuthenticationTypeImg.setImageResource(R.mipmap.drop_up)

                                (requireActivity() as MainActivity).blurMainFrame(true)
                            }
                        }
                        bindingHolder.apEditAuthenticationType.setOnClickListener (authtypeLisener)
                        bindingHolder.apEditAuthenticationTypeImg.setOnClickListener (authtypeLisener)


                        /*ContextCompat.getColor(requireContext(),R.color.gray_edit).apply {
                            bindingHolder.apEditSsid.setTextColor(this)
                            bindingHolder.apEditVlan.setTextColor(this)
                            bindingHolder.apEditAuthenticationType.setTextColor(this)
                        }*/
                        SophosEditableStype.enableEditText(bindingHolder.apEditSsid)
                        SophosEditableStype.enableEditText(bindingHolder.apEditVlan)


                        ContextCompat.getColor(requireContext(),R.color.table_color).apply {
                            bindingHolder.apEditSsid.setTextColor(this)
                            bindingHolder.apEditVlan.setTextColor(this)
                            bindingHolder.apEditAuthenticationType.setTextColor(this)
                        }

                        updateEditPresharedKey(value)
                    }
                }
            }

        }

        fun updateEditPresharedKey(value:GetWirelessBands.WirelessName){
            fragment.apply {
                bindingHolder.apEditPresharedKey.apply {
                    if(authType==Authentication_Type.none||authType==Authentication_Type.owe||authType==Authentication_Type.wep||wirelessBandsPageMode== WirelessBandsPageMode.Normal)
                    {
                        setText("")
                        setTextColor(  ContextCompat.getColor(requireContext(),R.color.table_color))
                        SophosEditableStype.disableEditText(  bindingHolder.apEditPresharedKey)
                       // setOnFocusChangeListener (null)
                        //isEnabled=false
                       // bindingHolder.apEditPresharedKeyFrame.setBackgroundResource(android.R.color.transparent)
                        if(authType==Authentication_Type.none||authType==Authentication_Type.owe||authType==Authentication_Type.wep){
                            setText("")
                        }else  setText(value.pass)
                    }
                    else
                    {

                        setTextColor(  ContextCompat.getColor(requireContext(),R.color.table_color))
                        SophosEditableStype.enableEditText(  this)
                      /*  setOnFocusChangeListener { v, hasFocus ->
                            if(hasFocus){
                                requireContext().getDrawable(R.drawable.background_focus_textbox_5dp).apply {
                                    bindingHolder.apEditPresharedKeyFrame.background=this
                                }
                            }else{
                                requireContext().getDrawable(R.drawable.background_default_textbox_5dp).apply {
                                    bindingHolder.apEditPresharedKeyFrame.background=this
                                }
                            }
                        }

                        if(isFocused){
                            requireContext().getDrawable(R.drawable.background_focus_textbox_5dp).apply {
                                bindingHolder.apEditPresharedKeyFrame.background=this
                            }
                        }else{
                            requireContext().getDrawable(R.drawable.background_default_textbox_5dp).apply {
                                bindingHolder.apEditPresharedKeyFrame.background=this
                            }
                        }*/
                        setText(value.pass)
                        //isEnabled=true
                    }
                }
            }
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(viewGroup.viewBinding(ItemWirelessBandsBinding::inflate).value)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        viewHolder.itemView.setOnClickListener {

        }
        // Get element from your dataset at this position and replace the
        // contents of the view with that element

        if(position<dataSet.size)viewHolder.bindItem(dataSet[position],position)
        else {
            ConstantClass.printForDebug("${javaClass.simpleName} IndexOutOfBoundsException position=${position} size=${dataSet.size}")
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    //override fun getItemCount() = dataSet.size
    override fun getItemCount() = viewSize
    @Synchronized
    @UiThread
    fun updateDataSet(sets: MutableList<GetWirelessBands.WirelessName>,size: Int){
        viewSize=size

        dataSet.clear()
        dataSet.addAll(sets)
        notifyDataSetChanged()
    }

    fun getNewDataSet(): MutableList<GetWirelessBands.WirelessName>{
        var data=MutableList<GetWirelessBands.WirelessName>(viewSize,{i ->
            GetWirelessBands.WirelessName().apply {
              var holder=  (parent.findViewHolderForLayoutPosition(i) as WirelessBandAdapter.ViewHolder)
                ssidname=holder.vb.apEditSsid.text.toString()
                index=i
                vlan_id=holder.vb.apEditVlan.text.toString().toInt()
                auth=holder.authType
                pass=holder.vb.apEditPresharedKey.text.toString()
            }
        })
        return data
    }

    @Synchronized
    @UiThread
    fun updateSize(size: Int){
        viewSize=size

        notifyDataSetChanged()
    }

    fun getDataSet()=dataSet

}
