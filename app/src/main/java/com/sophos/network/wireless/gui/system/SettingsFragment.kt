package com.sophos.network.wireless.gui.system

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.biometric.BiometricManager
import androidx.core.content.ContextCompat
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.android.viewer.utils.TimerUtil
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.gui.BaseFragment
import com.acelink.library.utils.data.ConstantClass
import com.acelink.library.sharepreference.SPUtils
import com.sophos.network.wireless.gui.widget.dialog.SophosMessageDialog
import com.sophos.network.wireless.pagetool.ViewPage2Pages.Companion.mainPages
import com.sophos.network.R
import com.sophos.network.databinding.FragSettingsBinding

//System prioity Fingerprint 2.Iris 3.Face。
class SettingsFragment : BaseFragment() {

    companion object{
        fun getInstance(): SettingsFragment {
            return  SettingsFragment()
        }
       val mTag ="SettingsFragment"

    }

    val binding by viewBinding(FragSettingsBinding::inflate)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")
        var b= SPUtils.getBiometric(requireContext())
        binding.apSettingFaceSwitch.setImageResource(gettoggleResouce(b))
        var p= SPUtils.getPincode(requireContext())
        binding.apSettingPasswordSwitch.setImageResource(gettoggleResouce(p))
        binding.apSettingFaceSwitch.setOnClickListener {
            var i= SPUtils.getBiometric(requireContext())
            var r=if(i==0) 1 else 0
            if(r==1){
              var strong=getMainActivity()!!.bioBiometricsStrongCheck()
                ConstantClass.printForDebug("bioBiometricsStrongCheck res=${it} ")

                if (strong == BiometricManager.BIOMETRIC_SUCCESS) {
                    SPUtils.setBiometric(requireContext(),r)
                    binding.apSettingFaceSwitch.setImageResource(gettoggleResouce(r))
                } else {
                    biometricFail(strong)
                }
            }else{
                SPUtils.setBiometric(requireContext(),r)
                binding.apSettingFaceSwitch.setImageResource(gettoggleResouce(r))
             }

        }

        binding.apSettingPasswordSwitch.setOnClickListener {
            var p= SPUtils.getPincode(requireContext())
            var r=if(p==0) 1 else 0
            if(r==1){
                var device=getMainActivity()!!.bioBiometricsDeviceCheck ()
                ConstantClass.printForDebug("apSettingPasswordSwitch res=${it} ")

                if (device == BiometricManager.BIOMETRIC_SUCCESS) {

                    SPUtils.setPincode(requireContext(),r)
                    binding.apSettingPasswordSwitch.setImageResource(gettoggleResouce(r))
                } else {
                    pincodeFail(device)
                }
            }else{
                SPUtils.setPincode(requireContext(),r)
                binding.apSettingPasswordSwitch.setImageResource(gettoggleResouce(r))
            }
        }


        return binding.root
    }

    private fun gettoggleResouce(b:Int)=if(b==0) R.mipmap.off_enable else R.mipmap.on_enable

    fun biometricFail(strong:Int){
        getMainActivity()?.apply {
            // binding.useBiometrics.visibility = View.INVISIBLE
            when (strong) {
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->{
                    Log.e("123", "No biometric features available on this device.")
                    showNotUnavaliable()
                }

                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ,
                BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED->{
                    Log.e("123", "Biometric features are currently unavailable.")

                    showNotUnavaliable()
                }
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    Log.e("123", "The user hasn't associated any biometric credentials with their account.")
                    this.lanuchMain {
                        SophosMessageDialog(requireActivity(),"",
                            getString(R.string.suthentication_set_msg),{ dialog ->
                                blurMainFrame(false)
                                // Prompts the user to create credentials that your app accepts.
                                /*val enrollIntent = Intent(Settings.ACTION_BIOMETRIC_ENROLL).apply {
                                    putExtra(
                                        Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,
                                        BiometricManager.Authenticators.BIOMETRIC_STRONG
                                    )
                                }
                                if (enrollIntent.resolveActivity(getMainActivity()!!.getPackageManager()) != null){
                                    startActivityForResult(enrollIntent, resultBiometric)
                                }*/
                            }).apply {
                            setOnDismissListener {
                                blurMainFrame(false)
                            }
                            show()
                            blurMainFrame(true)
                        }
                    }


                }
            }
        }

    }

    fun pincodeFail(device:Int){
        // binding.useBiometrics.visibility = View.INVISIBLE
        when (device) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->{
                Log.e("123", "No biometric features available on this device.")
                showNotUnavaliable()
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE,
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED
            ->{
                Log.e("123", "Biometric features are currently unavailable.")
                if(device== BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED){
                    if(getMainActivity()!!.oldKeyguardSecure()){
                        SPUtils.setPincode(requireContext(),1)
                        binding.apSettingPasswordSwitch.setImageResource(gettoggleResouce(1))
                        return
                    }
                }
                showNotUnavaliable()
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                Log.e("123", "The user hasn't associated any biometric credentials with their account.")
               this.lanuchMain {
                   msgDialog=SophosMessageDialog(requireActivity(),"",
                       getString(R.string.suthentication_set_msg),{ dialog ->
                           getMainActivity()?.blurMainFrame(false)
                           // Prompts the user to create credentials that your app accepts.
                           /* val enrollIntent = Intent(Settings.ACTION_BIOMETRIC_ENROLL).apply {
                                putExtra(
                                    Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED, BiometricManager.Authenticators.DEVICE_CREDENTIAL
                                )
                            }
                            if (enrollIntent.resolveActivity(getMainActivity()!!.getPackageManager()) != null){
                                val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
                                    Log.e("123", "Biometrics resultCode=${result.resultCode} ")

                                    if (result.resultCode == Activity.RESULT_OK) {

                                    }
                                }
                                startForResult.launch(enrollIntent)

                            }*/
                       }).apply {
                       setOnDismissListener {
                           getMainActivity()?.blurMainFrame(false)
                       }
                       show()
                       getMainActivity()?.blurMainFrame(true)
                   }
               }


            }
        }
    }

    var msgDialog:SophosMessageDialog?=null
    fun showNotUnavaliable(){
        getMainActivity()?.apply {
            msgDialog=SophosMessageDialog(this,"",
                getString(R.string.suthentication_unavaliable),{ dialog ->
                    blurMainFrame(false)

                }).apply {
                setOnDismissListener {
                    blurMainFrame(false)
                }
                show()
                blurMainFrame(true)
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onBackPress() {
        super.onBackPress()
        /*var currentIndex= mainPages.indexOf(frags_page.settings)
        getMainActivity()?.changeMainPage(mainPages.get(currentIndex-1))*/
        getMainActivity()?.openDrawer()
    }


    override fun onSelect() {
        ConstantClass.printForDebug("$mTag onSelect")
        super.onSelect()
    }

    override fun onDeSelect() {
        super.onDeSelect()
    }


}