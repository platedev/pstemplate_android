package com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.acelink.library.pagetool.BaseBindingTool.viewBinding

import com.android.viewer.utils.TimerUtil
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment

import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.SophosEditableStype
import com.sophos.network.R
import com.sophos.network.databinding.FragDeviceAllListNewBinding
import com.sophos.network.wireless.MainViewModel
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.fragments.adapter.DeviceNewAllListAdapter
import com.sophos.network.wireless.gui.widget.dialog.SophosYesNoDialog
import com.sophos.network.wireless.psclass.gson.data.SophosDeviceItem
import com.sophos.network.wireless.psclass.sharepreference.device.SophosAccount

enum class NewPageMode{
    New,Scan
}
class DeviceNewAllListFragment : Fragment() {
    companion object{
        fun getInstance()=  DeviceNewAllListFragment()

        val mTag = "DeviceNewAllListFragment"
    }

    private var newPageMode= NewPageMode.New

    val binding by viewBinding(FragDeviceAllListNewBinding::inflate,getAllListFragment()?.layoutInflater!!)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")


        val layoutManager =  object: LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        };
        binding.deviceNewListRecycleview.layoutManager=layoutManager

        binding.deviceAllNewAdd.setOnClickListener {
            SophosYesNoDialog(requireActivity(),"",
                getString(R.string.device_all),{ dialog,yes ->
                    getAllListFragment()?.getMainActivity()?.blurMainFrame(false)
                    if(yes){
                        (requireActivity() as MainActivity).apply {
                            var device= getSophosDevice()
                            device?.forEach {
                                it.new=false
                                SophosDeviceItem().apply {
                                    name=it.deviceName?:""
                                    operatoer=it.operatorMode?:0
                                    SophosAccount.setMyDevice(it.mac!!,this )
                                }

                            }

                            //    ConstantClass.printForDebug("update mac=${mac} size=${device?.size?:0} to My list")
                            if(device?.size?:0>0){

                                (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
                                    notificationAllSlaveUpdate()
                                    toMyListTab()

                                }
                            }

                        }
                    }



                }).apply {
                setOnDismissListener {
                    getAllListFragment()?.getMainActivity()?.blurMainFrame(false)
                }
                show()
                getAllListFragment()?.getMainActivity()?.blurMainFrame(true)
            }

        }
        if(!getMainActivity()!!.isDisconvery()){
            calculSophosDevices()
        }

        newPageMode=NewPageMode.New
        updateUIMode()

        return binding.root
    }

    fun calculSophosDevices(){

        var news=getMainActivity()?.getSophosDevice()!!.filter { it.new==true }
        getAllListFragment()?.updateTabNum(1,news.size)
        val sorted =news?.toTypedArray()?.sortedBy {it.deviceName  }
        DeviceNewAllListAdapter(this@DeviceNewAllListFragment,sorted?.toMutableList()).apply {
            binding.deviceNewListRecycleview.adapter =this

            SophosEditableStype.addSearchStyle(binding.deviceAllListNewEditFilter,{s->
                updateTextFilter(s.toString())
            })
        }
    }

    private fun updateUIMode(){
        when(newPageMode){
            NewPageMode.New ->{
                binding.deviceNewListRecycleview.visibility=View.VISIBLE
                var news=(requireActivity() as MainActivity).getSophosDevice()!!.filter { it.new==true }
                binding.deviceAllNewAdd.visibility= if(news.size==0)View.GONE else  View.VISIBLE

            }
            NewPageMode.Scan->{
                binding.deviceNewListRecycleview.visibility=View.GONE
                binding.deviceAllNewAdd.visibility=View.GONE

            }
        }
    }

    fun getMainActivity(): MainActivity=getAllListFragment()!!.getMainActivity()!!


    private fun getAllListFragment():DeviceAllListFragment?{
        return  (frags_page.allList.pageFragment as? DeviceAllListFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

}