package com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.fragments.adapter

import android.annotation.SuppressLint
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment

import com.sophos.network.wireless.pagetool.frags_page


import com.sophos.network.R
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import com.sophos.network.wireless.psclass.gson.data.SophosDeviceItem
import com.sophos.network.wireless.psclass.sharepreference.device.SophosAccount

open class DeviceNewAllListAdapter (private val frag: Fragment, private var devices: MutableList<BaseSophosDevice>?=null) :
    DeviceAllListAdapter(frag,devices)
{


    override fun onBindViewHolder(viewHolder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        var value=filterResult!![position]
        value.apply {
            viewHolder.apply {
                bindingHolder.deviceListItemName.isSelected=true

                bindingHolder.deviceListItemName.setText(deviceName)

                bindingHolder.deviceListItemMac.setText(mac)
                bindingHolder.deviceListNextImg.setImageResource(R.mipmap.add)
                bindingHolder.deviceListCardParent.setBackgroundResource(if (operatorMode==1)R.drawable.card_edge_central else R.drawable.card_edge)
                if(WiFiRadio==0){
                    bindingHolder.deviceListItemIconImg.setImageResource(R.mipmap.ap_disable_small_alert)
                }else{
                    bindingHolder.deviceListItemIconImg.setImageResource(R.mipmap.wifi)
                }
                updateHealth(bindingHolder)

                updateOeratorMode(bindingHolder)
                bindingHolder.deviceListItemClintTxt.setText(ConnectedUsers.toString())
                bindingHolder.deviceListItemMac.setText(mac)
                bindingHolder.deviceListNext.setOnClickListener {
                    (frag.requireActivity() as MainActivity).apply {
                        var device= getSophosDevice()?.filter { it.mac== mac}
                        ConstantClass.printForDebug("update  mac=${mac} name=${deviceName} size=${device?.size?:0} to My list")

                        if(device!=null&&device.size>0){
                            device!![0].apply {
                                new=false
                                SophosDeviceItem().apply {
                                    name=deviceName?:""
                                    operatoer=operatorMode
                                    SophosAccount.setMyDevice(mac!!,this )
                                }
                            }


                            (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
                                notificationAllSlaveUpdate()
                                //toMyListTab()//1 set  no change

                            }
                        }
                    }


                }

            }

        }
    }

    override fun getItemViewType(position: Int): Int {
        //return super.getItemViewType(position)
        return devices?.get(position)?.mac?.hashCode()?:super.getItemViewType(position)
    }
}
