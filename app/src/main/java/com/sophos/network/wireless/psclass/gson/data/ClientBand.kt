package com.sophos.network.wireless.psclass.gson.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.sophos.network.wireless.psclass.enum.Radio

class ClientBand {
    @Expose
    @SerializedName("result")
    val cmdResult = -1

    @Expose
    @SerializedName("vendor")
    var vendor:String ?= null


    var radio: Radio?= null

    @Expose
    @SerializedName("ip_address")
    var ip:String ?= null

    @Expose
    @SerializedName("mac_address")
    var mac:String ?= null

    @Expose
    @SerializedName("tx")
    var tx:String ?= null

    @Expose
    @SerializedName("rx")
    var rx:String ?= null

    @Expose
    @SerializedName("signale_ratio")
    var signal:Int ?= null

    @Expose
    @SerializedName("rssi")
    var rssi:Int ?= null

    @SerializedName("connected_time")
    var connectedTime:String ?= null

    @SerializedName("idle_time")
    var idle:Int ?= null

    @SerializedName("ssid_index")
    var ssid_index:Int ?= null

}