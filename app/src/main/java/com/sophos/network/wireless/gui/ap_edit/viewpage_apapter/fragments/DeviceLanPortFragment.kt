package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.acelink.library.utils.data.ConstantClass
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.acelink.library.biometric.BiometricPromptUtils
import com.acelink.library.gson.JsonHelper
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.utils.JudgeIpAddress
import com.acelink.library.utils.JudgeIpAddress.isIPv4Address
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.ap_edit.APEditFragment
import com.sophos.network.wireless.gui.ap_edit.APEditViewModel

import com.sophos.network.wireless.gui.widget.dialog.SophosSmallChooseDialog
import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.SophosEditableStype
import com.sophos.network.wireless.psclass.connection.SophosNetwork
import com.sophos.network.wireless.psclass.gson.data.GetLanPort
import com.sophos.network.wireless.psclass.gson.data.GetLanPort.Companion.ipv4IPTypeStatic
import com.sophos.network.R
import com.sophos.network.databinding.FragApEditLanPortBinding
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.ap_edit.APEditFragment.Companion.updateValueResultSuccess
import com.sophos.network.wireless.gui.ap_edit.APEditFragment.Companion.updateValueResultSuccess_andIPChange
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.abstractfrag.BaseSubEditFragment
import com.sophos.network.wireless.gui.widget.dialog.SophosMessageDialog
import com.sophos.network.wireless.psclass.gson.data.GetLanPort.Companion.ipv4IPTypeDHCP
import com.sophos.network.wireless.psclass.gson.data.GetLanPort.Companion.ipv4IPTypeUserDefine
import kotlinx.coroutines.runBlocking

enum class LanPortPageMode{
    Normal_Static,Normal_DHCP,Edit_Static,Edit_DHCP,Central_Static,Central_DHCP
}

class DeviceLanPortFragment : BaseSubEditFragment() ,View.OnClickListener{
    companion object{
        fun getInstance(): DeviceLanPortFragment {
            return  DeviceLanPortFragment()
        }
       val mTag = "DeviceLanPortFragment"
    }


    val binding by viewBinding(FragApEditLanPortBinding::inflate)

    var lanPortPageMode=LanPortPageMode.Normal_Static

    var dhcpServerEnable=true

    val lan_ip_type  by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        arrayOf(getString(R.string.static_ip), getString(R.string.dhcp))
    }
     /*0: Half Hour
     1: One Hour
     2: Two Hours
     3: Half Day
     4: One Day
     5: Two Days
     6: One Week
     7: Two Weeks
     8: One Month
     9: Forever*/
   // "One Hour","One Day","One Week","One Month","Forever" only support 1,4,6,8,9
    val leaseList  by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        arrayOf(getString(R.string.lease_time_half_hour),getString(R.string.lease_time_one_hour),
                getString(R.string.lease_time_two_hours),getString(R.string.lease_time_half_day),
            getString(R.string.lease_time_one_day),getString(R.string.lease_time_two_days),
            getString(R.string.lease_time_one_week),getString(R.string.lease_time_two_weeks),
            getString(R.string.lease_time_one_month),getString(R.string.lease_time_forever)
        )
    }

    val gatewayList by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        arrayOf(getString(R.string.user_defined),getString(R.string.from_dhcp))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")
        updateLanPortPageMode()
        return binding.root
    }

    fun updateLanPortPageMode(){
        ConstantClass.printForDebug("updateLanPortPageMode ${lanPortPageMode} ")
        if(lanPortPageMode==LanPortPageMode.Normal_Static||lanPortPageMode==LanPortPageMode.Central_Static||lanPortPageMode==LanPortPageMode.Edit_Static)
        {
            binding.apEditDnsSecondaryTxt.visibility=View.VISIBLE
            binding.apEditDnsPrimaryTxt.visibility=View.VISIBLE
            binding.apEditGatewayTxt.visibility=View.VISIBLE

            binding.apEditDhcpServerAllLay.visibility=View.VISIBLE
            binding.apEditGatewaySelectLay.visibility=View.GONE
            binding.apEditDnsPrimarySelectLay.visibility=View.GONE
            binding.apEditDnsSecondarySelectLay.visibility=View.GONE
            binding.apEditGatewaySelectTxt.setText("")
            binding.apEditDnsPrimarySelectTxt.setText("")
            binding.apEditDnsSecondarySelectTxt.setText("")
            binding.apEditGatewaySelectTxt.setOnClickListener(null)
            binding.apEditDnsSecondarySelectTxt.setOnClickListener(null)
            binding.apEditDnsPrimarySelectTxt.setOnClickListener(null)
            binding.apEditDhcpLeaseTimeImg.visibility=View.INVISIBLE


        }else
        {
            binding.apEditDhcpServerAllLay.visibility=View.GONE
            binding.apEditGatewaySelectLay.visibility=View.VISIBLE
            binding.apEditDnsPrimarySelectLay.visibility=View.VISIBLE
            binding.apEditDnsSecondarySelectLay.visibility=View.VISIBLE
            binding.apEditDhcpServerCheck.setOnClickListener(null)
            lanPortData?.ipv4?.apply {
                if(lanPortPageMode==LanPortPageMode.Normal_DHCP||lanPortPageMode==LanPortPageMode.Central_DHCP)
                {
                    binding.apEditGatewaySelectImg.visibility=View.GONE
                    binding.apEditDnsPrimarySelectImg.visibility=View.GONE
                    binding.apEditDnsSecondarySelectImg.visibility=View.GONE
                    if(ipv4_default_gateway_type==1){
                        binding.apEditGatewaySelectLay.visibility=View.VISIBLE
                        binding.apEditGatewayTxt.visibility=View.GONE
                    }else{
                        binding.apEditGatewaySelectLay.visibility=View.GONE
                        binding.apEditGatewayTxt.visibility=View.VISIBLE
                    }
                    if(ipv4_dns_primary_type==1){
                        binding.apEditDnsPrimarySelectLay.visibility=View.VISIBLE
                        binding.apEditDnsPrimaryTxt.visibility=View.GONE
                    }else{
                        binding.apEditDnsPrimarySelectLay.visibility=View.GONE
                        binding.apEditDnsPrimaryTxt.visibility=View.VISIBLE
                    }
                    if(ipv4_dns_secondary_type==1){
                        binding.apEditDnsSecondarySelectLay.visibility=View.VISIBLE
                        binding.apEditDnsSecondaryTxt.visibility=View.GONE
                    }else{
                        binding.apEditDnsSecondarySelectLay.visibility=View.GONE
                       binding.apEditDnsSecondaryTxt.visibility=View.VISIBLE
                    }
                }else//edit dhcp
                {

                    binding.apEditGatewaySelectLay.visibility=View.VISIBLE
                    binding.apEditDnsPrimarySelectLay.visibility=View.VISIBLE
                    binding.apEditDnsSecondarySelectLay.visibility=View.VISIBLE
                    binding.apEditGatewaySelectImg.visibility=View.VISIBLE
                    binding.apEditDnsPrimarySelectImg.visibility=View.VISIBLE
                    binding.apEditDnsSecondarySelectImg.visibility=View.VISIBLE

                    binding.apEditGatewaySelectImg.setImageResource(R.mipmap.drop_down)
                    binding.apEditDnsPrimarySelectImg.setImageResource(R.mipmap.drop_down)
                    binding.apEditDnsSecondarySelectImg.setImageResource(R.mipmap.drop_down)

                    binding.apEditDnsSecondaryTxt.visibility=View.VISIBLE
                    binding.apEditDnsPrimaryTxt.visibility=View.VISIBLE
                    binding.apEditGatewayTxt.visibility=View.VISIBLE

                }

            }

        }

        when(lanPortPageMode){
            LanPortPageMode.Normal_Static, LanPortPageMode.Normal_DHCP,
                    LanPortPageMode.Central_Static, LanPortPageMode.Central_DHCP->{

                ColorDrawable(ContextCompat.getColor(requireContext(),android.R.color.transparent)).apply {
                  //  binding.apEditLanIpFrame.background=this
                   // binding.apEditSubnetMaskFrame.background=this
                    binding.apEditDhcpPrimaryDnsFrame.background=this
                    binding.apEditDhcpSecondaryDnsFrame.background=this

                    binding.apEditDhcpStartingIpFrame.background=this
                    binding.apEditDhcpEndingIpFrame.background=this
                    binding.apEditDhcpDomainNameFrame.background=this
                    binding.apEditDnsPrimaryTxt.background=this
                    binding.apEditDnsSecondaryTxt.background=this
                }
                binding.apEditLanTypeSelectImg.visibility=View.GONE

                SophosEditableStype.disableEditText( binding.apEditGatewayTxt)
                SophosEditableStype.disableEditText( binding.apEditLanIpTxt)
                SophosEditableStype.disableEditText( binding.apEditSubnetMaskTxt)

                SophosEditableStype.disableEditText( binding.apEditDnsPrimaryTxt)
                SophosEditableStype.disableEditText( binding.apEditDnsSecondaryTxt)

                SophosEditableStype.disableEditText( binding.apEditDhcpStartingIpTxt)
                SophosEditableStype.disableEditText( binding.apEditDhcpEndingIpTxt)
                SophosEditableStype.disableEditText( binding.apEditDhcpDomainNameTxt)
                SophosEditableStype.disableEditText( binding.apEditDhcpPrimaryDnsTxt)
                SophosEditableStype.disableEditText( binding.apEditDhcpSecondaryDnsTxt)
                binding.apEditLanTypeSelect.setOnClickListener(null)
                binding.apEditLanTypeSelectImg.setOnClickListener(null)
                binding.apEditDhcpLeaseTimeTxt.setOnClickListener(null)

                binding.apEditLanTypeSelectImg.setOnClickListener(null)



               /* ContextCompat.getColor(requireContext(),R.color.gray_edit).apply {
                    binding.apEditIpAddressAssignTitle.setTextColor(this)
                    binding.apEditLandSideTitle.setTextColor(this)
                    binding.apEditDhcpServerTitle.setTextColor(this)
                }*/
                ResourcesCompat.getFont(requireContext(), R.font.sophossans_regular).apply {

                }
                binding.apEditDhcpServerCheck.setImageResource(if(dhcpServerEnable)R.mipmap.on_disable else R.mipmap.off_disable)
                binding.apEditDhcpServerCheck.setOnClickListener(null)

                binding.apEditGatewaySelectTxt.setOnClickListener(null)
                binding.apEditDnsPrimarySelectTxt.setOnClickListener(null)
                 binding.apEditDnsSecondarySelectTxt.setOnClickListener(null)

            }
            LanPortPageMode.Edit_Static->{
                ConstantClass.printForDebug("Edit_Static---------------------------------------------------> ")
                requireContext().getDrawable(R.drawable.background_default_textbox_5dp).apply {
                    // binding.apEditLanIpFrame.background=this
                 //   binding.apEditSubnetMaskFrame.background=this
                    binding.apEditDnsPrimaryTxt.background=this
                    binding.apEditDnsSecondaryTxt.background=this
                    binding.apEditDhcpPrimaryDnsFrame.background=this
                    binding.apEditDhcpSecondaryDnsFrame.background=this

                }
                SophosEditableStype.enableEditText( binding.apEditLanIpTxt)
                SophosEditableStype.enableEditText( binding.apEditSubnetMaskTxt)
                SophosEditableStype.enableEditText( binding.apEditGatewayTxt)
                SophosEditableStype.enableEditText( binding.apEditDnsPrimaryTxt)
                SophosEditableStype.enableEditText( binding.apEditDnsSecondaryTxt)


                binding.apEditLanTypeSelect.setOnClickListener(this)
                binding.apEditLanTypeSelectImg.setOnClickListener(this)
                binding.apEditLanTypeSelectImg.apply {
                    visibility=View.VISIBLE
                    setImageResource( R.mipmap.drop_down)
                }

                //binding.apEditGatewaySelectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                ContextCompat.getColor(requireContext(),android.R.color.black).apply {
                    binding.apEditIpAddressAssignTitle.setTextColor(this)
                    binding.apEditLandSideTitle.setTextColor(this)
                    binding.apEditDhcpServerTitle.setTextColor(this)
                }
                binding.apEditDhcpServerCheck.setImageResource(if(dhcpServerEnable)R.mipmap.on_enable else R.mipmap.off_enable)
                binding.apEditDhcpServerCheck.setOnClickListener {
                    dhcpServerEnable=!dhcpServerEnable
                    binding.apEditDhcpServerCheck.setImageResource(if(dhcpServerEnable)R.mipmap.on_enable else R.mipmap.off_enable)
                    updateDhcpServerEnableUI()

                }
                updateDhcpServerEnableUI()
                ConstantClass.printForDebug("Edit_Static end---------------------------------------------------> ")
            }
            LanPortPageMode.Edit_DHCP->{

                ContextCompat.getColor(requireContext(),android.R.color.black).apply {
                    binding.apEditIpAddressAssignTitle.setTextColor(this)
                    binding.apEditLandSideTitle.setTextColor(this)
                    binding.apEditDhcpServerTitle.setTextColor(this)
                }

                SophosEditableStype.disableEditText( binding.apEditLanIpTxt)
                SophosEditableStype.disableEditText( binding.apEditSubnetMaskTxt)
               /* ContextCompat.getColor(requireContext(),R.color.sophos_edit_disable_gray).apply {
                    binding.apEditLanIpTxt.setTextColor(this)
                    binding.apEditSubnetMaskTxt.setTextColor(this)
                }*/

                SophosEditableStype.enableEditText( binding.apEditGatewayTxt)
                SophosEditableStype.enableEditText( binding.apEditDnsPrimaryTxt)
                SophosEditableStype.enableEditText( binding.apEditDnsSecondaryTxt)


                binding.apEditLanTypeSelect.setOnClickListener(this)
                binding.apEditLanTypeSelectImg.setOnClickListener(this)
                binding.apEditLanTypeSelectImg.apply {
                    visibility=View.VISIBLE
                    setImageResource( R.mipmap.drop_down)
                }


                binding.apEditGatewaySelectTxt.setOnClickListener(this)
                binding.apEditDnsPrimarySelectTxt.setOnClickListener(this)
                binding.apEditDnsSecondarySelectTxt.setOnClickListener(this)

                lanPortData?.ipv4?.apply {
                    updateGatewaySelect(ipv4_default_gateway_type!!)
                    updateDnsPrimarySelectTxt(ipv4_dns_primary_type!!)
                    updateDnsSecondarySelect(ipv4_dns_secondary_type!!)
                }


            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    private fun updateDhcpServerEnableUI(){
        if(dhcpServerEnable){
            requireContext().getDrawable(R.drawable.background_default_textbox_5dp).apply {
                binding.apEditDhcpStartingIpFrame.background=this
                binding.apEditDhcpEndingIpFrame.background=this
                binding.apEditDhcpDomainNameFrame.background=this
                binding.apEditDhcpPrimaryDnsFrame.background=this
                binding.apEditDhcpSecondaryDnsFrame.background=this
            }
            SophosEditableStype.enableEditText( binding.apEditDhcpStartingIpTxt)
            SophosEditableStype.enableEditText( binding.apEditDhcpEndingIpTxt)
            SophosEditableStype.enableEditText( binding.apEditDhcpDomainNameTxt)
            SophosEditableStype.enableEditText( binding.apEditDhcpPrimaryDnsTxt)
            SophosEditableStype.enableEditText( binding.apEditDhcpSecondaryDnsTxt)
            binding.apEditDhcpLeaseTimeTxt.setOnClickListener(this)

            binding.apEditDhcpLeaseTimeImg.visibility=View.VISIBLE
        }else
        {
            ColorDrawable(ContextCompat.getColor(requireContext(),android.R.color.transparent)).apply {
                binding.apEditDhcpPrimaryDnsFrame.background=this
                binding.apEditDhcpSecondaryDnsFrame.background=this

                binding.apEditDhcpStartingIpFrame.background=this
                binding.apEditDhcpEndingIpFrame.background=this
                binding.apEditDhcpDomainNameFrame.background=this
            }
            SophosEditableStype.disableEditText( binding.apEditDhcpStartingIpTxt)
            SophosEditableStype.disableEditText( binding.apEditDhcpEndingIpTxt)
            SophosEditableStype.disableEditText( binding.apEditDhcpDomainNameTxt)
            SophosEditableStype.disableEditText( binding.apEditDhcpPrimaryDnsTxt)
            SophosEditableStype.disableEditText( binding.apEditDhcpSecondaryDnsTxt)
            binding.apEditDhcpLeaseTimeTxt.setOnClickListener(null)
            binding.apEditDhcpLeaseTimeImg.visibility=View.INVISIBLE
        }
    }

    private fun updateGatewaySelect(pos:Int){
        binding.apEditGatewaySelectTxt.text=gatewayList.get(pos)
        binding.apEditGatewayTxt.setText(lanPortData?.ipv4?.defaultGateway)
        when(lanPortPageMode){
            LanPortPageMode.Edit_Static ->  SophosEditableStype.enableEditText(binding.apEditGatewayTxt)
            LanPortPageMode.Edit_DHCP->{
                if(pos==0){
                    SophosEditableStype.enableEditText(binding.apEditGatewayTxt)
                }else{
                    SophosEditableStype.disableEditText(binding.apEditGatewayTxt)
                }
            }
            else ->SophosEditableStype.disableEditText(binding.apEditGatewayTxt)
        }
    }

    private fun updateDnsPrimarySelectTxt(pos:Int){
        binding.apEditDnsPrimarySelectTxt.setText(gatewayList.get(pos))
        binding.apEditDnsPrimaryTxt.setText(lanPortData?.ipv4?.ipv4DnsPrimary)
        when(lanPortPageMode){
            LanPortPageMode.Edit_Static ->  SophosEditableStype.enableEditText(binding.apEditDnsPrimaryTxt)
            LanPortPageMode.Edit_DHCP->{
                if(pos==0){
                    SophosEditableStype.enableEditText(binding.apEditDnsPrimaryTxt)
                }else{
                    SophosEditableStype.disableEditText(binding.apEditDnsPrimaryTxt)
                }
            }
            else ->SophosEditableStype.disableEditText(binding.apEditDnsPrimaryTxt)

        }

    }

    private fun updateDnsSecondarySelect(pos:Int){
        binding.apEditDnsSecondarySelectTxt.setText(gatewayList.get(pos))
        binding.apEditDnsSecondaryTxt.setText(lanPortData?.ipv4?.ipv4DnsSecondary)

        when(lanPortPageMode){

            LanPortPageMode.Edit_Static ->  SophosEditableStype.enableEditText(binding.apEditDnsSecondaryTxt)
            LanPortPageMode.Edit_DHCP->{
                if(pos==0){
                    SophosEditableStype.enableEditText(binding.apEditDnsSecondaryTxt)
                }else{
                    SophosEditableStype.disableEditText(binding.apEditDnsSecondaryTxt)
                }
            }
            else ->SophosEditableStype.disableEditText(binding.apEditDnsSecondaryTxt)

        }
    }

    var lanSelectDialog:SophosSmallChooseDialog<String>?=null
    var dhcpChooseDialog:SophosSmallChooseDialog<String>?=null
    var leaseTimeDialog:SophosSmallChooseDialog<String>?=null

    override fun onClick(v: View?) {
        lanPortData!!.ipv4!!.apply {
            when(v?.id){
                R.id.ap_edit_lan_type_select,R.id.ap_edit_lan_type_select_img ->{
                    ConstantClass.printForDebug("ap_edit_lan_type_select click ")
                    lanSelectDialog=SophosSmallChooseDialog<String>(requireActivity(),"",lan_ip_type.toMutableList(),0,false,{dialog,pos->
                        var type=lan_ip_type.get(pos)
                        ipv4Type=pos
                        ConstantClass.printForDebug("SophosChooseDialog iptype ${type}")
                        binding.apEditLanTypeSelect.text=type.toString()
                        dialog.dismiss()
                        (requireActivity() as MainActivity).blurMainFrame(false)
                        when(pos){
                            0->{
                                if(   lanPortPageMode!= LanPortPageMode.Edit_Static){
                                    lanPortPageMode=LanPortPageMode.Edit_Static
                                    updateLanPortPageMode()
                                }
                            }
                            1->{
                                if(   lanPortPageMode!= LanPortPageMode.Edit_DHCP){
                                    lanPortPageMode=LanPortPageMode.Edit_DHCP
                                    updateLanPortPageMode()
                                }
                            }
                        }

                    }).apply {

                        setOnDismissListener {
                            binding.apEditLanTypeSelectImg.setImageResource(R.mipmap.drop_down)
                            (requireActivity() as MainActivity).blurMainFrame(false)
                        }
                    }
                    lanSelectDialog!!.show()
                    binding.apEditLanTypeSelectImg.setImageResource(R.mipmap.drop_up)
                    (requireActivity() as MainActivity).blurMainFrame(true)
                }
                R.id.ap_edit_dhcp_lease_time_txt->{
                    var supportLease=leaseList.filterIndexed { index, s ->
                        when(index){
                            1,4,6,8,9->true
                            else ->false
                        }
                    }
                    leaseTimeDialog=SophosSmallChooseDialog<String>(requireActivity(),"",supportLease.toMutableList(),0,false,{dialog,pos->
                        var lease=when(pos){
                            0->1
                            1->4
                            2->6
                            3->8
                            else->9
                        }
                        dhcpLeaseTime=lease
                        dhcpLeaseTime=lease
                        ConstantClass.printForDebug("SophosChooseDialog lease ${lease}")
                        binding.apEditDhcpLeaseTimeTxt.text=leaseList.get(lease)
                        dialog.dismiss()
                        (requireActivity() as MainActivity).blurMainFrame(false)

                    }).apply {

                        setOnDismissListener {
                            binding.apEditDhcpLeaseTimeImg.setImageResource(R.mipmap.drop_down)
                            (requireActivity() as MainActivity).blurMainFrame(false)
                        }
                        show()
                    }
                    binding.apEditDhcpLeaseTimeImg.setImageResource(R.mipmap.drop_up)
                    (requireActivity() as MainActivity).blurMainFrame(true)
                }
                R.id.ap_edit_gateway_select_txt,
                R.id.ap_edit_dns_primary_select_txt,
                R.id.ap_edit_dns_secondary_select_txt->{
                    if(ipv4Type==ipv4IPTypeDHCP){

                        dhcpChooseDialog=SophosSmallChooseDialog<String>(requireActivity(),"",gatewayList.toMutableList(),0,false,{dialog,pos->
                            var gateway=gatewayList.get(pos)

                            ConstantClass.printForDebug("SophosChooseDialog lease ${gateway}")
                            dialog.dismiss()
                            (requireActivity() as MainActivity).blurMainFrame(false)
                            when(v!!.id){
                                R.id.ap_edit_gateway_select_txt->{
                                    ipv4_default_gateway_type=pos
                                    updateGatewaySelect(pos)

                                }
                                R.id.ap_edit_dns_primary_select_txt->{

                                    ipv4_dns_primary_type=pos
                                    updateDnsPrimarySelectTxt(pos)

                                }
                                R.id.ap_edit_dns_secondary_select_txt->{
                                    ipv4_dns_secondary_type=pos
                                    updateDnsSecondarySelect(pos)

                                }
                            }


                        }).apply {
                            setOnDismissListener {
                                when(v.id){
                                    R.id.ap_edit_gateway_select_txt->  binding.apEditGatewaySelectImg.setImageResource( R.mipmap.drop_down)
                                    R.id.ap_edit_dns_primary_select_txt->binding.apEditDnsPrimarySelectImg.setImageResource( R.mipmap.drop_down);
                                    R.id.ap_edit_dns_secondary_select_txt->binding.apEditDnsSecondarySelectImg.setImageResource( R.mipmap.drop_down);
                                }
                                (requireActivity() as MainActivity).blurMainFrame(false)
                            }
                            show()
                        }
                        when(v.id){
                            R.id.ap_edit_gateway_select_txt->  binding.apEditGatewaySelectImg.setImageResource( R.mipmap.drop_up)
                            R.id.ap_edit_dns_primary_select_txt->binding.apEditDnsPrimarySelectImg.setImageResource( R.mipmap.drop_up)
                            R.id.ap_edit_dns_secondary_select_txt->binding.apEditDnsSecondarySelectImg.setImageResource( R.mipmap.drop_up);
                        }

                        (requireActivity() as MainActivity).blurMainFrame(true)
                    }

                }
            }
        }
    }

    var lanPortData: GetLanPort.LanPorData?=null
    private fun updateLanPortUI(lanPort: GetLanPort){
        lanPort.data?.ipv4?.apply {
            ConstantClass.printForDebug("updateLanPortUI ipv4Type=${ipv4Type} ipv4_default_gateway_type=${ipv4_default_gateway_type}")
            lanPortData=lanPort.data.copy()
            if(ipv4Type==ipv4IPTypeStatic){
                binding.apEditLanTypeSelect.text=lan_ip_type.get(0).toString()
                when(lanPortPageMode){
                    LanPortPageMode.Normal_DHCP->lanPortPageMode=LanPortPageMode.Normal_Static
                    LanPortPageMode.Central_DHCP->lanPortPageMode=LanPortPageMode.Central_Static
                    LanPortPageMode.Edit_DHCP->lanPortPageMode=LanPortPageMode.Edit_Static
                }
            }else{
                binding.apEditLanTypeSelect.text=lan_ip_type.get(1).toString()
                when(lanPortPageMode){
                    LanPortPageMode.Normal_Static->lanPortPageMode=LanPortPageMode.Normal_DHCP
                    LanPortPageMode.Central_Static->lanPortPageMode=LanPortPageMode.Central_DHCP
                    LanPortPageMode.Edit_Static->lanPortPageMode=LanPortPageMode.Edit_DHCP
                }
            }
            binding.apEditLanIpTxt.setText(ipv4Address)
            binding.apEditSubnetMaskTxt.setText(subnetMask)
            binding.apEditGatewayTxt.setText(defaultGateway)

            binding.apEditDnsPrimaryTxt.setText(ipv4DnsPrimary)
            binding.apEditDnsSecondaryTxt.setText(ipv4DnsSecondary)

            binding.apEditDhcpStartingIpTxt.setText(dhcpStartAddr)
            binding.apEditDhcpEndingIpTxt.setText(dhcpEndAddr)
            binding.apEditDhcpDomainNameTxt.setText(dhcpDomainName)
            binding.apEditDhcpLeaseTimeTxt.setText(dhcpLeaseTime?.let { leaseList.get(it) })
            binding.apEditDhcpPrimaryDnsTxt.setText(dhcpDnsPrimary)
            binding.apEditDhcpSecondaryDnsTxt.setText(dhcpDnsSecondary)
            if(dhcpEnabled==1){
                dhcpServerEnable=true
            }else{
                dhcpServerEnable=false
            }
            updateLanPortPageMode()

            updateGatewaySelect(ipv4_default_gateway_type!!)
            updateDnsPrimarySelectTxt(ipv4_dns_primary_type!!)
            updateDnsSecondarySelect(ipv4_dns_secondary_type!!)
        }
    }

    var messageDialog:SophosMessageDialog?=null
    fun showMessageDialog(mesage:String)
    {
        lanuchMain {
            getMainActivity()?.apply {
                messageDialog=SophosMessageDialog(this,"",
                    mesage,{ dialog ->
                        blurMainFrame(false)
                    }).apply {
                    setOnDismissListener {
                        blurMainFrame(false)
                    }
                    show()
                    blurMainFrame(true)
                }
            }
        }
    }


    suspend fun updateLanPort():Int
    {
        lanPortData?.ipv4?.apply {
            if(ipv4Type==ipv4IPTypeDHCP)
            {
                if(ipv4_default_gateway_type==ipv4IPTypeUserDefine){
                    var gateway=binding.apEditGatewayTxt.text.toString()
                    if(!isIPv4Address(gateway)){
                        showMessageDialog("${getString(R.string.default_gateway)} ${getString(R.string.ip_wrong)}")
                        return APEditFragment.updateValueResultReturn
                    }
                    defaultGateway  =gateway
                    ConstantClass.printForDebug("set defaultGateway=${defaultGateway}")
                }
                if(ipv4_dns_primary_type==ipv4IPTypeUserDefine){
                    var ip=binding.apEditDnsPrimaryTxt.text.toString()
                    if(!isIPv4Address(ip)){
                        showMessageDialog("${getString(R.string.primary_address)} ${getString(R.string.ip_wrong)}")
                        return APEditFragment.updateValueResultReturn
                    }
                    ipv4DnsPrimary  =ip
                }
                if(ipv4_dns_secondary_type==ipv4IPTypeUserDefine){
                    var ip=binding.apEditDnsSecondaryTxt.text.toString()
                    if(!isIPv4Address(ip)){
                        showMessageDialog("${getString(R.string.secondary_address)} ${getString(R.string.ip_wrong)}")
                        return APEditFragment.updateValueResultReturn
                    }
                    ipv4DnsSecondary  =ip
                }
            }else if(ipv4Type==ipv4IPTypeStatic)
            {
                var lanip=binding.apEditLanIpTxt.text.toString()
                if(!isIPv4Address(lanip)){
                    showMessageDialog("${getString(R.string.ip_address)} ${getString(R.string.ip_wrong)}")
                    return APEditFragment.updateValueResultReturn
                }
                var gateway=binding.apEditGatewayTxt.text.toString()
                if(!isIPv4Address(gateway)){
                    showMessageDialog("${getString(R.string.default_gateway)} ${getString(R.string.ip_wrong)}")
                    return APEditFragment.updateValueResultReturn
                }
                var primary=binding.apEditDnsPrimaryTxt.text.toString()
                if(!isIPv4Address(primary)){
                    showMessageDialog("${getString(R.string.primary_address)} ${getString(R.string.ip_wrong)}")
                    return APEditFragment.updateValueResultReturn
                }
                var secondary=binding.apEditDnsSecondaryTxt.text.toString()
                if(!isIPv4Address(secondary)){
                    showMessageDialog("${getString(R.string.secondary_address)} ${getString(R.string.ip_wrong)}")
                    return APEditFragment.updateValueResultReturn
                }
                var mask=binding.apEditSubnetMaskTxt.text.toString()
                if(!isIPv4Address(mask)){
                    showMessageDialog("${getString(R.string.invalid_submask)}")
                    return APEditFragment.updateValueResultReturn
                }

                ipv4Address=lanip
                subnetMask=mask
                defaultGateway=gateway
                ipv4DnsPrimary=primary
                ipv4DnsSecondary= secondary
                dhcpEnabled=if(dhcpServerEnable) 1 else 0

                if(dhcpEnabled==1){
                    var start=binding.apEditDhcpStartingIpTxt.text.toString()
                    if(!isIPv4Address(start)){
                        showMessageDialog("${getString(R.string.starting_ip)} ${getString(R.string.ip_wrong)}")
                        return APEditFragment.updateValueResultReturn
                    }
                    var end=binding.apEditDhcpEndingIpTxt.text.toString()
                    if(!isIPv4Address(end)){
                        showMessageDialog("${getString(R.string.ending_ip)} ${getString(R.string.ip_wrong)}")
                        return APEditFragment.updateValueResultReturn
                    }

                    var primary=binding.apEditDhcpPrimaryDnsTxt.text.toString()
                    if(!isIPv4Address(primary)){
                        showMessageDialog("${getString(R.string.primary_dns)} ${getString(R.string.ip_wrong)}")
                        return APEditFragment.updateValueResultReturn
                    }
                    var secondary=binding.apEditDhcpSecondaryDnsTxt.text.toString()
                    if(!isIPv4Address(secondary)){
                        showMessageDialog("${getString(R.string.secondary_dns)} ${getString(R.string.ip_wrong)}")
                        return APEditFragment.updateValueResultReturn
                    }
                    dhcpStartAddr= start
                    dhcpEndAddr= end
                    dhcpDomainName=binding.apEditDhcpDomainNameTxt.text.toString()
                   //dhcpLeaseTime already set when on click
                    dhcpDnsPrimary=binding.apEditDhcpPrimaryDnsTxt.text.toString()
                    dhcpDnsSecondary=binding.apEditDhcpSecondaryDnsTxt.text.toString()
                }

                if(!JudgeIpAddress.CheckMaskandIPAddress(this@DeviceLanPortFragment,mask,lanip,dhcpEnabled==1,dhcpStartAddr!!,dhcpEndAddr!!)){

                    return APEditFragment.updateValueResultReturn
                }
            }

             viewModel()?.lanPortData?.apply {
                 var finalData=data?.createLanPortJson(  lanPortData!!)
                 ConstantClass.printForDebug("set LANPORT 0 defaultGateway= ${finalData?.ipv4?.defaultGateway} "+ JsonHelper.createJson(finalData!!))
                 ConstantClass.printForDebug("set LANPORT 1 "+ JsonHelper.createJson(data!!))
                 ConstantClass.printForDebug("set LANPORT equal= "+ data!!.equalData( finalData!!))

                 var res=viewModel()?.setLanPorts(finalData,this@DeviceLanPortFragment)!!
                 if(res==updateValueResultSuccess){
                     if(lanPortData?.ipv4?.ipv4Type!!!=this.data.ipv4?.ipv4Type!!||(lanPortData?.ipv4?.ipv4Type==ipv4IPTypeStatic&&lanPortData?.ipv4?.ipv4Address!=this.data.ipv4?.ipv4Address!!)){
                         res=updateValueResultSuccess_andIPChange
                     }
                 }
                 return  res
             }


        }
        return  APEditFragment.updateValueResultNoChange
    }


    fun onSelect(){
        lanuchIO {
            viewModel()?.getLanPort()?.apply {
                if(this){
                    lanuchMain {
                        ConstantClass.printForDebug("onSelect ipv4Type=${viewModel()?.lanPortData!!.data?.ipv4?.ipv4Type}")
                        updateLanPortUI( viewModel()?.lanPortData!!)
                    }

                }
            }
            getMainActivity()?.onHideProgress()
        }
    }



}