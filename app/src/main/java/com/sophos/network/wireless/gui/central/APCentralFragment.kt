package com.sophos.network.wireless.gui.central

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.sophos.network.wireless.pagetool.frags_page


import com.acelink.library.utils.data.ConstantClass


import com.sophos.network.R
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import com.acelink.library.pagetool.CoroutineIO
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.sophos.network.BuildConfig
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceEditFragments

import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.AdapterDeviceCentralFragments
import com.sophos.network.databinding.FragApCentralBinding
import com.sophos.network.wireless.MainViewModel
import com.sophos.network.wireless.gui.APConfigFragment
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment
import com.sophos.network.wireless.psclass.sharepreference.device.SophosAccount
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class APCentralFragment : APConfigFragment() {
    companion object{
        fun getInstance(): APCentralFragment {
            return  APCentralFragment()
        }
       val mTag = "APCentralFragment"
    }

    val binding by viewBinding(FragApCentralBinding::inflate)

    private var timer =TimerUtil()

    private lateinit var tabLayoutMediator:TabLayoutMediator


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} ")
        viewModel.centralFragment=this

        getMainActivity()!!.updateTitleRightVisable(View.GONE)
        binding.apEditViewpager.adapter= AdapterDeviceCentralFragments(this)
        tabLayoutMediator=TabLayoutMediator(binding.apEditTabs, binding.apEditViewpager) { tab, position ->
            when(position){
                AdapterDeviceEditFragments.edit_system_page ->  {
                    tab.text = getString(R.string.system)
                    changeTabFont(0)
                }
                AdapterDeviceEditFragments.edit_lan_port_page ->{
                    tab.text = getString(R.string.lan_port)
                }
                AdapterDeviceEditFragments.edit_wireless_band_page ->{
                    tab.text = getString(R.string.wireless_bands)
                }
                AdapterDeviceEditFragments.edit_clients_page ->{
                    tab.text = getString(R.string.wireless_clients)
                }
                AdapterDeviceEditFragments.edit_day_time_page ->{
                    tab.text = getString(R.string.day_and_time)
                }
                AdapterDeviceEditFragments.edit_ping_test_page ->{
                    tab.text = getString(R.string.ping_test)
                }
                AdapterDeviceEditFragments.edit_traceroute_test_page ->{
                    tab.text = getString(R.string.traceroute_test)
                }
            }

            ConstantClass.printForDebug("TabLayoutMediator pos=${ position}")

        }.apply {attach()  }
        allotEachTabWidth()
        binding.apEditTabs.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                ConstantClass.printForDebug("Celtral onTabSelected pos=${ tab?.position?:-1}")
                changeTabFont(tab.position)

                lanuchIO{
                    delay(100)
                    //lanuchMain {
                        when(tab.position){
                            AdapterDeviceEditFragments.edit_system_page ->getViewPager2Adapter().deviceSystemFragment.onSelect()
                            AdapterDeviceEditFragments.edit_lan_port_page ->getViewPager2Adapter().deviceLanPortFragment.onSelect()
                            AdapterDeviceEditFragments.edit_wireless_band_page ->getViewPager2Adapter().deviceWirelessBandsFragment.onSelect()
                            AdapterDeviceEditFragments.edit_clients_page ->getViewPager2Adapter().deviceClientsFragment.onSelect()

                            AdapterDeviceEditFragments.edit_day_time_page ->getViewPager2Adapter().dayTimeFragment.onSelect()
                            AdapterDeviceEditFragments.edit_traceroute_test_page ->getViewPager2Adapter().deviceTracerouteTestFragment.onSelect()
                        }
                   // }

                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {
               // ConstantClass.printForDebug("onTabReselected pos=${ tab?.position?:-1}")
               // changeTabFont(tab.position)
            }
        })
       /* binding.apEditViewpager.registerOnPageChangeCallback(object :ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if(binding.apEditTabs.selectedTabPosition!=position){
                    val tab: TabLayout.Tab = binding.apEditTabs.getTabAt(position)!!
                    tab.select()
                }
            }
        })*/

        changeTabFont(0)
        updateTitleUI()

        return binding.root
    }

    private fun updateTitleUI(){
        getCurrentEditDevice()!!.apply {
            binding.apEditTitleName.setText(deviceName)
            binding.apEditTitleName.isSelected=true
            binding.apEditTitleMac.setText(mac)

            binding.apEditTitleDelete.setOnClickListener {
                ConstantClass.printForDebug("apEditTitleDelete")
                mac!!.let { it1 -> SophosAccount.deleteDevice(it1) }
                getSophosDevice()!!.find { it.mac==mac }!!.new=true

                (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
                    notificationAllSlaveUpdate()
                    getViewPager2Adapter().deviceMyAllListFragment.startScan(false)
                }
                onBackPress()

            }

            binding.apEditTitleFrequency.setText(getWiFiRadioFrequency())
             binding.apEditTitleClintCount.setText("${ConnectedUsers} ${getString(R.string.connected_clients)}")

        }
    }


    private fun allotEachTabWidth() {
        val slidingTabStrip = binding.apEditTabs.getChildAt(0) as ViewGroup
        for (i in 0 until binding.apEditTabs.getTabCount()) {
            val tab = slidingTabStrip.getChildAt(i)
            val layoutParams = tab.layoutParams as LinearLayout.LayoutParams
            layoutParams.weight = if(i==2) 2f else if(i==1) 1.3f else if(i==0) 1.1f else 1f
            tab.layoutParams = layoutParams
        }
    }

    fun changeTabFont( posSelect:Int){
        ConstantClass.printForDebug("changeTabFont pos=${posSelect}")
        val vg =  binding.apEditTabs.getChildAt(0) as ViewGroup
        val tabsCount = vg.childCount
        for (j in 0 until tabsCount) {
            val vgTab = vg.getChildAt(j) as ViewGroup
            val tabChildsCount = vgTab.childCount
            for (i in 0 until tabChildsCount) {
                val tabViewChild = vgTab.getChildAt(i)
                if (tabViewChild is TextView)
                {
                    tabViewChild.setTextSize(TypedValue.COMPLEX_UNIT_SP,14f )
                    if(posSelect==j)
                        tabViewChild.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.sophossans_bold))
                    else
                        tabViewChild.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.sophossans_regular))
                }
            }
        }
    }


    fun getViewPager2Adapter(): AdapterDeviceCentralFragments {
        return  binding.apEditViewpager.adapter as AdapterDeviceCentralFragments!!
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }


    override fun onBackPress() {
        super.onBackPress()
        getMainActivity()!!.changeMainPage(frags_page.allList)
    }


    override fun onSelect() {
        ConstantClass.printForDebug("$mTag onSelect DeviceListFragment")

        super.onSelect()
    }

    override fun onDeSelect() {
        timer.removeMyTimer()
        super.onDeSelect()
    }

    override fun updateDiscoveryEnd(){
        ConstantClass.printForDebug("ssdpScan AP Central updateDiscoveryEnd")
        var current=getCurrentEditDevice()
        getSophosDevice()!!.forEachIndexed { index, baseSophosDevice ->
            if(baseSophosDevice.mac==current!!.mac){
                lanuchMain {
                    binding.apEditTitleClintCount.setText("${baseSophosDevice.ConnectedUsers} ${getString(R.string.connected_clients)}")
                    if(baseSophosDevice.deviceName!= binding.apEditTitleName.text.toString()||BuildConfig.DEBUG){
                        binding.apEditTitleName.setText(baseSophosDevice.deviceName)
                    }

                   /* if(baseSophosDevice.timeNotFound==0){
                        var time=System.currentTimeMillis()
                        if(time-discoveryUpdateModeDelay>10000){
                            discoveryUpdateModeDelay=time
                            if(baseSophosDevice.WiFiRadio==0&&apEditMode!= APEditMode.radio_disable){
                                apEditMode= APEditMode.radio_disable
                                updateApEditModeUI()
                            }else if(baseSophosDevice.WiFiRadio!=0&&apEditMode!= APEditMode.enable){
                                apEditMode= APEditMode.enable
                                updateApEditModeUI()
                            }
                        }

                    }*/
                    binding.apEditTitleFrequency.setText(baseSophosDevice.getWiFiRadioFrequency())
                }

            }
        }
    }


}