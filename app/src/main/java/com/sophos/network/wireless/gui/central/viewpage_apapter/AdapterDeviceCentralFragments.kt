package com.sophos.network.wireless.gui.ap_edit.viewpage_apapter

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.sophos.network.wireless.gui.ap_edit.viewpage_apapter.fragments.*

import java.lang.NullPointerException

class AdapterDeviceCentralFragments(val fragment: Fragment) : FragmentStateAdapter(fragment)
{

    val  deviceSystemFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceSystemFragment.getInstance().apply {
            systemPageMode=SystemPageMode.Central
        }
    }
    val  deviceLanPortFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceLanPortFragment.getInstance().apply {
            lanPortPageMode=LanPortPageMode.Central_Static
        }
    }
    val  deviceWirelessBandsFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceWirelessBandsFragment.getInstance()
    }

    val  deviceClientsFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceClientsFragment.getInstance().apply {
            dragable=false
        }
    }

    val  dayTimeFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DayTimeFragment.getInstance()
    }

    val  devicePingTestFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DevicePingTestFragment.getInstance()
    }

    val  deviceTracerouteTestFragment by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        DeviceTracerouteTestFragment.getInstance()
    }

    override fun createFragment(position: Int): Fragment
    {
        var f =when(position){
            AdapterDeviceEditFragments.edit_system_page ->deviceSystemFragment
            AdapterDeviceEditFragments.edit_lan_port_page ->deviceLanPortFragment
            AdapterDeviceEditFragments.edit_wireless_band_page ->deviceWirelessBandsFragment
            AdapterDeviceEditFragments.edit_clients_page ->deviceClientsFragment
            AdapterDeviceEditFragments.edit_day_time_page ->dayTimeFragment
            AdapterDeviceEditFragments.edit_ping_test_page ->devicePingTestFragment
            AdapterDeviceEditFragments.edit_traceroute_test_page ->deviceTracerouteTestFragment
            else->throw NullPointerException()
        }

        return f

    }

    override fun onBindViewHolder(holder: FragmentViewHolder, position: Int, payloads: MutableList<Any>) {
      //  ConstantClass.printForDebug("onBindViewHolder ${position} ")
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getItemCount(): Int
    {
        return 7
    }


}