package com.sophos.network.wireless.psclass.devcies

import android.view.View
import androidx.core.content.ContextCompat
import com.acelink.library.search.UPnPSophosDiscovery
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.R
import com.sophos.network.databinding.ItemDeviceCardBinding
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.fragments.adapter.DeviceAllListAdapter

open class BaseSophosDevice {
    companion object{
        //const val DEFAULT_DDNS_PORT = 8090
        const val DEFAULT_DDNS_PORT = 10443
        const val HTTPS_PREFIX = "https://"
        const val SLASH_SYMBOL = "/"
        const val COLON_SYMBOL = ":"
        private const val LOGIN_PORT = DEFAULT_DDNS_PORT
    }
    var timeNotFound=0

    var mac:String?=null
    var model:String?=null
    var Identity:Int?=null
    var firmwareVersion:String?=null
    //0=none(stay alone) 1=managed by Sophos Central 2=managed by Mesh 3= managed by AP controller
    var operatorMode:Int?=null
    var location:String?=null

    //0=Disable  1=2.4G Only 2=5G Only  4=6G Only 3=2.4G+5G  5=2.4G+6G  6=5G+6G  7=2.4G+5G+6G
    var WiFiRadio:Int?=null

    fun getWiFiRadioFrequency():String{
        return when(WiFiRadio){
            0->""
            1->"2.4Ghz"
            2->"5 Ghz"
            4->"6 Ghz"
            3->"2.4Ghz | 5Ghz"
            5->"2.4Ghz | 6Ghz"
            6->"5Ghz | 6Ghz"
            else->"2.4 Ghz | 5Ghz | 6Ghz"
        }
    }

    // var password:String?=null
    var deviceName:String?=null
    var new=true
    var currentUser:String?=null
    var currentPassword:String?=null
    //0=excellen 1=good 2=poor
    var heath: Int?=0
    //Excellent 為 <= 55%
    //Good 為 >= 56% && <= 85%
    //Poor 為 >= 86%
    // 以 CPU Utilization 為例因為範圍是 0-100 所以到達 86 就是 poor
    var CPUUtilization:Int=0
    // 以Connected Users為例如果範圍是 0-256 那麽到達 220 就是poor
    var ConnectedUsers=0
    var SNR:Int?=null
    var MemoryUtilization:Int=0

    fun isSupport6G()=model?.run { contains("840E")||contains("420E") }?:false

    fun bandList()= model.run {
        if(this==null||!("840" in this) ){
            (1..8).toList().toTypedArray()
        }else
            (1..16).toList().toTypedArray()
    }

    fun isUnknown()=timeNotFound>= UPnPSophosDiscovery.ssdpDiscoveryNotFountToUnknown||location==null||model==null

    fun calculateHealth(){
        //420 ->256 840->512 users
       // val maxUsers=model?.run {if("840" in this) 512 else 256  }?:256
        val poorUsers=model?.run {if("840" in this) 440 else 280  }?:280
        if(CPUUtilization>=86||MemoryUtilization>=86||ConnectedUsers>=poorUsers)
        {
            heath=2
        }else if(CPUUtilization>=56||MemoryUtilization>=56||ConnectedUsers>=poorUsers*0.5)
        {
            heath=1
        }else {
            heath=0
        }
    }

    fun updateHealth(bindingHolder: ItemDeviceCardBinding){
        when(heath){
            0->{

                bindingHolder.deviceListItemHealthTxt.setText(R.string.signal_excellent)
                bindingHolder.deviceListItemSignal.setBackgroundColor(
                    ContextCompat.getColor(bindingHolder.root.context,
                        R.color.sophos_signal_excellent))
            }
            1->{
                bindingHolder.deviceListItemHealthTxt.setText(R.string.signal_good)
                bindingHolder.deviceListItemSignal.setBackgroundColor(
                    ContextCompat.getColor(bindingHolder.root.context,
                        R.color.sophos_signal_good))
            }
            2->{
                bindingHolder.deviceListItemHealthTxt.setText(R.string.signal_poor)
                bindingHolder.deviceListItemSignal.setBackgroundColor(
                    ContextCompat.getColor(bindingHolder.root.context,
                        R.color.sophos_signal_poor))
            }
        }
    }

    fun updateOeratorMode(bindingHolder: ItemDeviceCardBinding){
        if(operatorMode==1){
            bindingHolder.deviceListItemClintImg.setImageResource(R.mipmap.client_black)
            bindingHolder.deviceListItemClintTxt.setTextColor(ContextCompat.getColor(bindingHolder.root.context,android.R.color.black))
            bindingHolder.deviceListItemCentralImg.visibility= View.VISIBLE
            bindingHolder.deviceListItemCentralTxt.visibility= View.VISIBLE
        }else{
            bindingHolder.deviceListItemClintImg.setImageResource(R.mipmap.client)
            bindingHolder.deviceListItemClintTxt.setTextColor(ContextCompat.getColor(bindingHolder.root.context,R.color.sophos_blue))
            bindingHolder.deviceListItemCentralImg.visibility= View.GONE
            bindingHolder.deviceListItemCentralTxt.visibility= View.GONE
        }
    }

    override fun toString(): String {
        return "mac=${mac}, model=${model} , name=${deviceName} , CPUUtilization=${CPUUtilization},SNR=${SNR}, location=${location},MemoryUtilization=${MemoryUtilization}"
    }

    open fun isComplete():Boolean{
        ConstantClass.printForDebug("isComplete mac=${mac},location=${location}, model=${model}")
        return mac!=null&&location!=null&&model!=null
    }


    fun getHttpLocationURL():String?{
        // return "${HTTPS_PREFIX}${IP}:${DEFAULT_DDNS_PORT}${SLASH_SYMBOL}login"
        return "${HTTPS_PREFIX}${location}"
    }

    fun copy(base:BaseSophosDevice){
        mac=base.mac
        model=base.model

        firmwareVersion=base.firmwareVersion
        location=base.location

        deviceName=base.deviceName
        operatorMode=base.operatorMode
        Identity=base.Identity
        CPUUtilization=base.CPUUtilization
        ConnectedUsers=base.ConnectedUsers
        SNR=base.SNR
        MemoryUtilization=base.MemoryUtilization
        WiFiRadio=base.WiFiRadio
        heath=base.heath
    }


}