package com.sophos.network.wireless.gui.widget.dialog.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.acelink.library.pagetool.BaseBindingHolder
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.R
import com.sophos.network.databinding.ItemChooseBinding
import com.sophos.network.wireless.psclass.enum.Authentication_Type

class ChooseAdapter <T>(
    private val dataSet: List<T>,
    val pos:Int,
    val needIcon: Boolean=true,
    val center: Boolean,
    val blueTouch:Boolean=false,
    val result:(choose:Int)->Unit) :
    RecyclerView.Adapter<ChooseAdapter<T>.ViewHolder>() {


    private var itemSelected:Int=pos
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    inner class ViewHolder(vb: ItemChooseBinding) : BaseBindingHolder<ItemChooseBinding>(vb) {
            fun bindItem(value:String,position:Int){
                bindingHolder.itemChooseText.setText(value)
                bindingHolder.itemChooseText.isSelected=true

                if(needIcon){
                    bindingHolder.chooseItemBackground.apply {
                        if(position==itemSelected){
                            // setImageResource( R.mipmap.btn_basic_blue)
                            // binding.itemChooseText.setTextColor(  ContextCompat.getColor(itemView.context,android.R.color.white))
                            bindingHolder.itemChooseCheckImg.setImageResource(R.mipmap.radio_btn_on)
                        }else{
                            //binding.itemChooseText.setTextColor(  ContextCompat.getColor(itemView.context,R.color.dialog_text))
                            //setImageResource( R.color.top_bar_bg)
                            bindingHolder.itemChooseCheckImg.setImageResource(R.mipmap.radio_btn_no)
                        }

                    }
                }else{
                    bindingHolder.itemChooseCheckImg.visibility=View.GONE
                }
                if(blueTouch){
                    bindingHolder.itemChooseContentParent.setOnTouchListener { view, motionEvent ->
                        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            bindingHolder.itemChooseText.setTextColor(ContextCompat.getColor(view.context, R.color.sophos_url_blue))
                        }
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP||motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                            bindingHolder.itemChooseText.setTextColor(ContextCompat.getColor(view.context, android.R.color.black))
                        }
                        ConstantClass.printForDebug("ChooseAdapter motionEvent="+motionEvent.getAction())
                        false
                    }
                }

                if(center){
                   // bindingHolder.itemChooseText.gravity=Gravity.CENTER//no use
                }else{
                    if(dataSet.get(0) is Int){
                        bindingHolder.itemChooseText.gravity=Gravity.CENTER
                    }
                }


            }

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(viewGroup.viewBinding(ItemChooseBinding::inflate).value)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        viewHolder.itemView.setOnClickListener {
           // ConstantClass.printForDebug("itemView  ${position} itemSelected=${itemSelected}")
           // if(itemSelected!=position){
                var pos=itemSelected
                itemSelected=position
                notifyItemChanged(pos)
                notifyItemChanged(itemSelected)
                ConstantClass.printForDebug("itemSelected ${itemSelected}")
                result.invoke(itemSelected)
           // }
        }
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        dataSet[position].apply {
            when(this){
                is Array<*> ->  viewHolder.bindItem( this[0].toString(),position)
                is Authentication_Type ->  viewHolder.bindItem(  viewHolder.itemView.context.getString(getStringRes()),position)
                else->  viewHolder.bindItem( this.toString(),position)
            }
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

   // fun getSelected()=itemSelected



}
