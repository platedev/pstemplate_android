package com.sophos.network.wireless.psclass.enum


import android.util.SparseArray
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.psclass.gson.data.ClientBand
import com.sophos.network.wireless.psclass.gson.data.GetClients
import com.sophos.network.wireless.psclass.gson.data.GetSecurity
import com.sophos.network.wireless.psclass.gson.data.GetWirelessBands
import kotlinx.coroutines.Deferred

enum class Radio {
    G24,G5,G6;

    override fun toString(): String {
        return  when(this){
            G24->"2g"
            G5->"5g"
            G6->"6g"
        }
    }

    fun bands():String{
        return  when(this){
            G24->"2.4GHZ"
            G5->"5GHZ"
            G6->"6GHZ"
        }
    }

    var deferred: Deferred<GetClients?>?=null

    var clientsValue: GetClients?=null

    var deferredBand: Deferred<GetWirelessBands?>?=null

    var valueBand: GetWirelessBands?=null

    var deferredSecuritys=SparseArray<Deferred<GetSecurity?>?>()

    var valueSecuritys=SparseArray<GetSecurity?>()


    companion object{
        fun getAllClients(): ArrayList<ClientBand>{
            val list: ArrayList<ClientBand> = ArrayList()
            Radio.values().forEach {
                it.clientsValue?.data?.clientList?.apply {
                    var count=size

                    var subList=MutableList<ClientBand>(count,{ pos->
                        this[pos].apply {
                            radio=it
                        }
                    })
                    list.addAll(subList)
                }

               // ConstantClass.printForDebug("${it.toString()} count=${count} total=${it.clientsValue?.data?.totalSize?:"error code=${it.clientsValue?.errorCode}"?:"null"} list size=${list.size}" )
            }
            return list
        }

        val G6DefaultAuth=Authentication_Type.owe
    }
}