package com.sophos.network.wireless.gui.general

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.viewer.utils.ViewUtils
import com.bumptech.glide.Glide
import com.sophos.network.wireless.gui.BaseFragment
import com.acelink.library.utils.data.ConstantClass
import com.acelink.library.utils.data.ConstantClass.printForDebug

import android.graphics.Bitmap
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.sophos.network.R
import com.sophos.network.databinding.FragWelcomeBinding


class WelcomeFragment : BaseFragment() {
    companion object {
        fun getInstance() = WelcomeFragment()
        val mTag ="WelcomeFragment"
    }

    private val binding by viewBinding(FragWelcomeBinding::inflate)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug(mTag,"${mTag} onCreateView")

        ViewUtils.hideStatusBar(requireActivity())

        showWebLogo()

        val root: View = binding.root
        return root
    }



    private fun showWebLogo() {


        printForDebug("showWebLogo ")


    }



    override fun onDestroy() {
        ConstantClass.printForDebug("${javaClass.simpleName} onDestroy")

        super.onDestroy()
    }
}