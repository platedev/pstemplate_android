package com.sophos.network.wireless.gui.widget.dialog.adapter

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.sophos.network.wireless.psclass.SophosEditableStype
import com.sophos.network.R
import com.sophos.network.databinding.SophosDialogLoginBinding


class SophosLoginDialog
    (
   val con: Context,val passwordError:Boolean,
   val result:(dialog:Dialog,user:String,pass:String,keep:Boolean,yes:Boolean)->Unit
) : Dialog(con, R.style.MyDialog), OnShowListener, View.OnClickListener {

    private val binding by viewBinding(SophosDialogLoginBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // ConstantClass.printForDebug("OwnYesNoDialog " )
        window?.apply {
            attributes.windowAnimations = R.style.DialogAnimationScaletIn
            setBackgroundDrawableResource(android.R.color.transparent);
        }
        setCancelable(true)
        setCanceledOnTouchOutside(true)
        setContentView(binding.root)
        setOnShowListener(this)
    }

    override fun onShow(arg0: DialogInterface) {
        binding.cancel.setOnClickListener(this)
        binding.ok.setOnClickListener(this)
        SophosEditableStype.enableEditText(binding.loginAdminName)
        SophosEditableStype.enableEditText(binding.loginAdminPassword)
        binding.keepLoginCheckTxt.setOnClickListener(this)
        if(passwordError)showPasswordError()
        binding.loginAdminPassword.transformationMethod = PasswordTransformationMethod.getInstance()
    }

    override fun onClick(v: View) {
       when(v.id){
           R.id.ok->{
               var user=binding.loginAdminName.text.toString()
               var pass=binding.loginAdminPassword.text.toString()
               if(user.isEmpty()||pass.isEmpty()){
                   return
               }
               result.invoke(this,user,pass,binding.keepLoginCheck.isChecked,true)
               dismiss()
           }
           R.id.cancel->{
              // result.invoke(this,null,null,false,false)
               dismiss()
           }
           R.id.keep_login_check_txt->{
               binding.keepLoginCheck.isChecked=!binding.keepLoginCheck.isChecked
           }
       }
    }

    fun showPasswordError(){
        binding.dialogAccountInputError.visibility=View.VISIBLE
    }
}