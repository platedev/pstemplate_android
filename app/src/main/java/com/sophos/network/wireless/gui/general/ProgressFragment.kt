package com.sophos.network.wireless.gui.general

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acelink.library.pagetool.BaseBindingTool.viewBinding

import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.databinding.FragProgressBinding


class ProgressFragment : DialogFragment() {

    companion object {
        fun newInstance() = ProgressFragment()
        val mTag = "ProgressFragment"
        var progress:ProgressFragment?=null
    }

    private val binding by viewBinding(FragProgressBinding::inflate)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        ConstantClass.printForDebug("onCreateView ProgressFragment")
        val root: View = binding.root
        this.dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return root
    }



    override fun onDestroyView() {
        ConstantClass.printForDebug("onDestroyView ProgressFragment")
        super.onDestroyView()

    }

    fun setMsg(msg: String) {
        binding.txtMsg.text = msg
        binding.txtMsg.visibility = View.VISIBLE
    }

}
