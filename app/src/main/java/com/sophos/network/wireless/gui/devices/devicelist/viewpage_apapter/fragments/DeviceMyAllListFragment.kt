package com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.acelink.library.pagetool.BaseBindingTool.viewBinding
import com.android.viewer.utils.TimerUtil
import com.acelink.library.utils.data.ConstantClass

import androidx.core.content.ContextCompat
import com.acelink.library.pagetool.CoroutineIO.lanuchIO
import com.acelink.library.pagetool.CoroutineIO.lanuchMain
import com.acelink.library.search.UPnPSophosDiscovery
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.gui.devices.devicelist.DeviceAllListFragment

import com.sophos.network.wireless.pagetool.frags_page
import com.sophos.network.wireless.psclass.SophosEditableStype
import com.sophos.network.R
import com.sophos.network.databinding.FragDeviceAllListMyBinding
import com.sophos.network.wireless.MainViewModel
import com.sophos.network.wireless.gui.devices.devicelist.viewpage_apapter.fragments.adapter.DeviceMyAllListAdapter
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import kotlinx.coroutines.delay

enum class MyPageMode{
    MyDevices,NoDevice
}

class DeviceMyAllListFragment : Fragment() {
    companion object{
        fun getInstance(): DeviceMyAllListFragment {
            return  DeviceMyAllListFragment()
        }
       val mTag = "DeviceMyAllListFragment"
    }

    val binding by viewBinding(FragDeviceAllListMyBinding::inflate)

    private val timer=TimerUtil()

    var myPageMode:MyPageMode=MyPageMode.NoDevice

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        ConstantClass.printForDebug("onCreateView ${javaClass.simpleName} isDisconvery=${getMainActivity()?.isDisconvery()}")

        val layoutManager =  object: LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        };
        binding.devicelistAllListRecycleview.layoutManager=layoutManager

        initMyPage()

        return binding.root
    }

    fun initMyPage(){
        /*if(getMainActivity()?.getSophosDevice()?.run { size>0&&any { it.location!=null&&it.model!=null } }?:false){
            (frags_page.allList.pageFragment as DeviceAllListFragment).apply {

                notificationAllSlaveUpdate()
                var my= getMainActivity()?.getSophosDevice()?.filter { it.new==false } as ArrayList<BaseSophosDevice>
                //var news= getMainActivity()?.getSophosDevice()?.filter { it.new==true } as ArrayList<BaseSophosDevice>
                myPageMode=MyPageMode.MyDevices
                updateUIMode()
                if(my.size>0&&my.all { it.location!=null }){
                    toMyListTab()
                }else{
                    toNewTab()
                }
            }

        }*/
        if(getMainActivity()?.getSophosDevice()?.run { filter { it.new==false }.size>0  }?:false){
            (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
                toMyListTab()
            }
            updateUIMyDevicesMode()
            calculSophosDevices()
            startScan(false)
        }else{
            updateUINoDeviceMode()
            startScan(true)
        }

    }

    fun calculSophosDevices():ArrayList<BaseSophosDevice>?{
        var devices=getMainActivity()!!.getSophosDevice()
        ConstantClass.printForDebug("My calculSophosDevices size=${devices?.size?.toString()?:"null"} ")
        var my=devices?.filter {
            it.new==false
        }
        my?.forEach {
            ConstantClass.printForDebug("My calculSophosDevices mac=${it.mac} timeNotFound=${it.timeNotFound} ")
        }
        var sorted=my?.toTypedArray()?.sortedBy {it.deviceName  }
        getDeviceMyAllListAdapter()?.updateLists(sorted?.toMutableList())?: kotlin.run {
            DeviceMyAllListAdapter(this@DeviceMyAllListFragment,sorted?.toMutableList())
                .apply {
                    binding.devicelistAllListRecycleview.adapter =this

                    SophosEditableStype.addSearchStyle(binding.deviceAllListMyEditFilter!!,{s->
                        updateTextFilter(s.toString())
                    })

                }
        }

        getAllListFragment()?.updateTabNum(0,my?.size?:0)

        //   SophosEditableStype.enableEditText(binding.deviceAllListMyEditFilter)

        return my as ArrayList<BaseSophosDevice>
    }

    fun notificationCurrentEditUpdate(){
        var device=getMainActivity()!!.getCurrentEditDevice()
        getDeviceMyAllListAdapter()?.getDataSets()?.forEachIndexed { index, baseSophosDevice ->
            if(baseSophosDevice.mac==device?.mac){
                getDeviceMyAllListAdapter()?.notifyItemChanged(index)
            }
        }

    }

    private fun updateUIMode(){
        ConstantClass.printForDebug("updateUIMode myPageMode=${myPageMode}")
        when(myPageMode){
            MyPageMode.NoDevice ->{
                binding.devicelistAllListRecycleview.visibility=View.GONE

                binding.devicelistAllListWelcome.root.visibility=View.VISIBLE
                binding.devicelistAllListWelcome.deviceAllListProgressLay.visibility=View.VISIBLE

                binding.devicelistAllListWelcome.deviceAllListScanProgress.setProgressTintList(
                    ColorStateList.valueOf(
                        ContextCompat.getColor(requireContext(), R.color.sophos_url_blue)));

            }

            MyPageMode.MyDevices->{
                binding.devicelistAllListRecycleview.visibility=View.VISIBLE
                binding.devicelistAllListWelcome.root.visibility=View.GONE

            }
        }
    }

    fun updateUIMyDevicesMode(){
        myPageMode=MyPageMode.MyDevices
        updateUIMode()
    }

    fun updateUINoDeviceMode(){
        myPageMode=MyPageMode.NoDevice
        updateUIMode()
    }

    fun startScan(new:Boolean){
        ConstantClass.printForDebug("updateUIMode startScan")
        if(!new)getMainActivity()?.onShowProgress(null)
        UPnPSophosDiscovery.timeoutDiscovery = UPnPSophosDiscovery.totalDiscoveryTime
        getMainViewModel()!!.discoveryDevices(requireContext(),{
            getMainViewModel()?.updateXendDevice(it)
            lanuchMain{
              //  myPageMode=MyPageMode.MyDevices
              //  updateUIMode()

                (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
                    var mys= getMainActivity()?.getSophosDevice()?.filter { it.new==false } as ArrayList<BaseSophosDevice>

                    if(!new){
                        lanuchIO {

                            ConstantClass.printForDebug("deviceNewAllListFragment isAdded=${isAdded}")
                            lanuchMain{
                                if(mys.size>0){
                                    toMyListTab()
                                }else{
                                    toNewTab()
                                }

                                notificationAllSlaveUpdate()
                            }
                        }
                    }else{
                        lanuchMain {
                            if(UPnPSophosDiscovery.timeoutDiscovery<=0) {
                                if(new)toNewTab()//wait discovery end
                            }
                            notificationAllSlaveUpdate()
                        }
                    }

                }
                if(!new){
                    timer.removeMyTimer()
                    getMainActivity()?.onHideProgress()
                }
            }
        })
        timer.startTimer({
           
            lanuchMain{
                var progress=(100-UPnPSophosDiscovery.timeoutDiscovery/50)
                ConstantClass.printForDebug("setProgress= ${progress}")
                binding.devicelistAllListWelcome.deviceAllListScanProgress.setProgress(progress)
                var time="${Math.ceil( (UPnPSophosDiscovery.timeoutDiscovery/1000f).toDouble()).toInt()} ${getString(R.string.seconds_left)}"
                binding.devicelistAllListWelcome.deviceListScanTimerTxt.setText(time)
            }
            ConstantClass.printForDebug("updateUIMode startScan timeoutDiscovery ${UPnPSophosDiscovery.timeoutDiscovery}")
            if(UPnPSophosDiscovery.timeoutDiscovery<=0){
                timer.removeMyTimer()
                ConstantClass.printForDebug("updateUIMode startScan end toNewTab")
                lanuchMain {
                    updateUIMyDevicesMode()
                    if(new)(frags_page.allList.pageFragment as DeviceAllListFragment).toNewTab()

                    lanuchIO {
                        delay(500)
                        lanuchMain {
                            (frags_page.allList.pageFragment as DeviceAllListFragment).apply {
                                notificationAllSlaveUpdate()
                            }
                        }
                    }
                }
            }
        },0,50)
    }

    fun getMainActivity(): MainActivity?=getAllListFragment()?.getMainActivity()

    fun getMainViewModel(): MainViewModel?=getAllListFragment()?.getMainViewModel()

    private fun getAllListFragment():DeviceAllListFragment?{
      return  (frags_page.allList.pageFragment as? DeviceAllListFragment)
    }

    fun getDeviceMyAllListAdapter():DeviceMyAllListAdapter?{
        return   binding.devicelistAllListRecycleview.adapter as? DeviceMyAllListAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer.removeMyTimer()
    }


}