package com.sophos.network.wireless.psclass.connection

import com.sophos.network.wireless.psclass.gson.data.*
import com.acelink.library.utils.data.ConstantClass
import kotlinx.coroutines.async
import com.acelink.library.psnet.PSNetworkText
import com.acelink.library.gson.JsonHelper
import com.acelink.library.common.annotations.Definitions
import com.acelink.library.psnet.HttpResult
import com.acelink.library.utils.EasyUtils.tryCatchBlock
import com.acelink.library.utils.JudgeIpAddress
import com.google.gson.JsonSyntaxException
import com.sophos.network.BuildConfig
import com.sophos.network.wireless.psclass.devcies.BaseSophosDevice
import com.sophos.network.wireless.psclass.enum.Radio
import com.sophos.network.wireless.psclass.sharepreference.device.SophosAccount
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.runBlocking

import org.json.JSONObject
import java.lang.Exception

object SophosNetwork {
//upper limit api 10 counts at the same time
   /* private fun getApplication(): Application? {
        return try {
            val activityThreadClass = Class.forName("android.app.ActivityThread")
            val method = activityThreadClass.getMethod("currentApplication")
            method.isAccessible=true
            if (BuildConfig.DEBUG) Log.i("111", "getApplication method=$method")
            return method.invoke(null) as Application
        } catch (e: Exception) {
            null
        }
    }
    fun useMainThread(context: Context, run: Runnable) {
        ContextCompat.getMainExecutor(context).execute(run)
    }
    */


    suspend fun <T> retryAsyn(block:()->T): Deferred<T?>? {
        return runBlocking {
            var deferre:Deferred<T?>?=null

            for (i in 0  .. 2){
                deferre= async{ block.invoke()}
                if(deferre.await()!=null){
                    break
                }
            }
             deferre
        }
    }

    suspend fun retryAsynBoolean(block:()->Boolean): Deferred<Boolean> {
        return runBlocking {
            var deferre:Deferred<Boolean>?=null

            for (i in 0  .. 2){
                deferre= async{ block.invoke()}
                if(deferre.await()){
                    break
                }
            }
            deferre!!
        }
    }

    fun getResult(result: HttpResult.Success<String>):Int{
      return tryCatchBlock<Int> ( {
          var json=  JSONObject(result.data)
          if(json.has(Definitions.errorCode)){
              return@tryCatchBlock json.getInt(Definitions.errorCode)
          }
          Definitions.Result.FORMAT_ERROR
      },   Definitions.Result.FORMAT_ERROR)!!

    }

    fun getReloadTime(result: HttpResult.Success<String>):Int?{
        return tryCatchBlock<Int> ( {
            var json=  JSONObject(result.data)
            if(json.has("data")){
                return@tryCatchBlock json.getJSONObject("data").getInt("reload_time")
            }
            return@tryCatchBlock null
        }, null)

    }


    suspend fun setReload(device:BaseSophosDevice):Int?{
        var result= retryAsyn{
            runBlocking {
                var url="${device.getHttpLocationURL()}${Definitions.CGI.RELOAD}"
                var result=PSNetworkText.connectDigestPUT(url,device.currentUser!!,device.currentPassword!!)
                return@runBlocking simplesParseHttpResult(result,{
                    when(result){
                        is HttpResult.Success->{
                            return@simplesParseHttpResult tryCatchBlock ({
                                return@tryCatchBlock getReloadTime(result)
                            } ,null)
                        }

                    }
                    return@simplesParseHttpResult  null
                })

            }

        }
        return result?.await()

    }

    suspend fun setReboot(device:BaseSophosDevice):Boolean{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.REBOOT}"
        ConstantClass.printForDebug("123","setReboot DEBUG=${BuildConfig.DEBUG}")
        if(BuildConfig.DEBUG){
            var res=PSNetworkText.testPUT(device.getHttpLocationURL()!!)

            getResult(res as  HttpResult.Success)
        }//for ui test
        var result=PSNetworkText.connectDigestPUT(url,device.currentUser!!,device.currentPassword!!)

        return simpleBoolParseHttpResult(result,{
            getResult(it)==0
        })
    }

    //private val startURL="https://192.168.0.22:10443"
   // private val startURL="https://192.168.1.7:10443"

    suspend fun getSystemInfo(device:BaseSophosDevice):GetSystem?{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.SYSTEM_INFO}"

        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)
        ConstantClass.printForDebug("getSystemInfo=${when(result){
           is HttpResult.Success->  result.data
            is HttpResult.Error ->"exception->"+result.exception
        }}")
        when(result){
            is HttpResult.Success->{
                var data:GetSystem?=null
                try {
                    data= JsonHelper.parseJson<GetSystem>(result.data, GetSystem::class.java)
                }catch (je: JsonSyntaxException){
                    printStackTrace(je)
                    if(result.code==401){
                        return GetSystem().apply { errorCode=401 }
                    }
                }
                catch (e:Exception){
                    printStackTrace(e)
                }
                return data
            }
            else->{}
        }
        return null
    }

    suspend fun setSystemInfo(device:BaseSophosDevice,name:String):Boolean{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.SYSTEM_ADMINISTRATION}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)
        return simpleBoolParseHttpResult(result,{
            return@simpleBoolParseHttpResult tryCatchBlock<Boolean> ({
                var getJson=JSONObject(it.data).getJSONObject("data")
                if(getJson.has("product_name")){
                    getJson.put("product_name",name)
                    var result2= runBlocking {
                        PSNetworkText.connectDigestPost(url,getJson.toString(),device.currentUser!!,device.currentPassword!!)
                    }
                    if(result2 is HttpResult.Success && getResult(result2)==0){
                        return@tryCatchBlock true
                    }
                }
                return@tryCatchBlock false
            },false)!!
        })

    }

    suspend fun getBands(device:BaseSophosDevice,radio: Radio):GetWirelessBands?{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.WIRELESS_BAND}${radio.toString()}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)
        return simplesParseHttpResult(result,{
            when(result){
                is HttpResult.Success->{
                    var clients:GetWirelessBands?=null
                    tryCatchBlock {
                        clients= JsonHelper.parseJson<GetWirelessBands>(result.data, GetWirelessBands::class.java)
                        ConstantClass.printForDebug("getBands radio=${radio} clients=$clients")
                    }
                    return@simplesParseHttpResult clients
                }
                else->{}
            }
            return@simplesParseHttpResult null
        })

    }

    suspend fun getBandsString(device:BaseSophosDevice,radio: Radio):String?{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.WIRELESS_BAND}${radio.toString()}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)
        return simplesParseHttpResult(result,{
            when(result){
                is HttpResult.Success->{

                    return@simplesParseHttpResult result.data
                }
                else->{}
            }
            return@simplesParseHttpResult null
        })
    }

    suspend fun setBand(device:BaseSophosDevice,radio: Radio,band: GetWirelessBands.WirelessBandData):Boolean{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.WIRELESS_BAND}${radio.toString()}"
        var json=JsonHelper.createJson(band)
        var result=PSNetworkText.connectDigestPost20s(url,json,device.currentUser!!,device.currentPassword!!)

        //ConstantClass.printForDebug("setBand result=${json} ")
        return simpleBoolParseHttpResult(result,{
            getResult(it)==0
        })
    }

    suspend fun setBandString(device:BaseSophosDevice,radio: Radio,band: String):Boolean{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.WIRELESS_BAND}${radio.toString()}"
        var result=PSNetworkText.connectDigestPost20s(url,band,device.currentUser!!,device.currentPassword!!)

        return simpleBoolParseHttpResult(result,{
            getResult(it)==0
        })
    }

    suspend fun getRadioBands(base:BaseSophosDevice){
        runBlocking {
            Radio.values().forEach {
                it.deferredBand=null
                it.valueBand=null
                it.deferredBand=async {
                    getBands(base,it)
                }
            }

            Radio.values().forEach{
                it.valueBand=it.deferredBand?.await()
            }
            //retry
            var radios=Radio.values()
            for( i in 0 .. 2){
                if(radios.all {  it.valueBand!=null})break
                Radio.values().forEach {
                    if(it.valueBand==null){
                        it.deferredBand=async {
                            getBands(base,it)
                        }
                    }
                }

                Radio.values().forEach{
                    if(it.valueBand==null){
                        it.valueBand=it.deferredBand?.await()
                    }

                }
            }
        }
    }

    suspend fun recoveryRadios(base:BaseSophosDevice,item:SophosDeviceItem?,support6G:Boolean):Boolean{
        return runBlocking {

            var g24Band= getBandsString(base,Radio.G24)

            try {
                var getJson=JSONObject(g24Band).getJSONObject("data")
                if(getJson.getInt("radio_enable")!=item?.enable24g?:1){
                    getJson.put("radio_enable",item?.enable24g?:1)
                    if(!setBandString(base,Radio.G24,getJson.toString())){
                        return@runBlocking false
                    }
                }
            }catch (e:Exception){
                printStackTrace(e)
                return@runBlocking false
            }
            var g5Band= getBandsString(base,Radio.G5)
            try {
                var getJson=JSONObject(g5Band).getJSONObject("data")
                if(getJson.get("radio_enable")!=item?.enable5g?:1){
                    getJson.put("radio_enable",item?.enable5g?:1)
                    if(!setBandString(base,Radio.G5,getJson.toString())){
                        return@runBlocking false
                    }
                }
            }catch (e:Exception){
                printStackTrace(e)
                return@runBlocking false
            }
            if(support6G){
                var g6Band= getBandsString(base,Radio.G6)
                try {
                    var getJson=JSONObject(g6Band).getJSONObject("data")
                    if(getJson.get("radio_enable")!=item?.enable6g?:1){
                        getJson.put("radio_enable",item?.enable6g?:1)
                        if(!setBandString(base,Radio.G6,getJson.toString())){
                            return@runBlocking false
                        }
                    }
                }catch (e:Exception){
                    printStackTrace(e)
                    return@runBlocking false
                }
            }
            true
        }

    }

    suspend fun disableRadios(base:BaseSophosDevice,item:SophosDeviceItem?,support6G:Boolean):Boolean{
        return runBlocking {
            getRadioBands(base)
            if(Radio.G24.valueBand?.data?.radio_enable==null||Radio.G5.valueBand?.data?.radio_enable==null
                ||(support6G&&Radio.G6.valueBand?.data?.radio_enable==null)){
                return@runBlocking false
            }
            //save radio status
            item?.apply {
                enable24g=Radio.G24.valueBand?.data?.radio_enable
                enable5g=Radio.G5.valueBand?.data?.radio_enable
                if(support6G)
                    enable6g=Radio.G6.valueBand?.data?.radio_enable
                SophosAccount.setMyDevice(base.mac!!,this)
            }

            if(Radio.G24.valueBand!!.data!!.radio_enable!=0)
            {
                var result24G=retryAsynBoolean{
                    runBlocking {
                        Radio.G24.valueBand!!.data!!.radio_enable=0
                        setBand(base,Radio.G24,Radio.G24.valueBand!!.data!!)
                    }

                }
                if(!result24G.await()){
                    ConstantClass.printForDebug("disableRadios 24G Fail")
                    return@runBlocking false
                }
            }

            if(Radio.G5.valueBand!!.data!!.radio_enable!=0)
            {
                var result5G=retryAsynBoolean{
                    runBlocking {
                        Radio.G5.valueBand!!.data!!.radio_enable=0
                        setBand(base,Radio.G5,Radio.G5.valueBand!!.data!!)
                    }

                }
                if(!result5G.await()){
                    ConstantClass.printForDebug("disableRadios 5G Fail")
                    return@runBlocking false
                }
            }

            if(support6G)
            {
                if(Radio.G6.valueBand!!.data!!.radio_enable!=0){
                    var result6G=retryAsynBoolean{
                        runBlocking {
                            Radio.G6.valueBand!!.data!!.radio_enable=0
                            setBand(base,Radio.G6,Radio.G6.valueBand!!.data!!)
                        }

                    }
                    if(!result6G.await()){
                        ConstantClass.printForDebug("disableRadios 6G Fail")
                        return@runBlocking false
                    }
                }
            }


            true
        }

    }

    suspend fun getSecurity(device:BaseSophosDevice,radio: Radio,index:Int):GetSecurity?{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.WIRELESS_SECURITY}${radio.toString()}${Definitions.CGI.WIRELESS_SECURITY_INDEX}${index}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)

        return simplesParseHttpResult(result,{
            when(result){
                is HttpResult.Success->{
                    var clients:GetSecurity?=null
                    tryCatchBlock({
                        clients= JsonHelper.parseJson<GetSecurity>(result.data, GetSecurity::class.java)
                        ConstantClass.printForDebug("getSecurity ${radio} ${index} clients=${clients}")
                    })
                    return@simplesParseHttpResult clients
                }
                else->{}
            }
            return@simplesParseHttpResult null
        })

    }

    suspend fun setSecurity(device:BaseSophosDevice,radio: Radio,index:Int,file: String):Boolean{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.WIRELESS_SECURITY}${radio.toString()}${Definitions.CGI.WIRELESS_SECURITY_INDEX}${index}"
        var result=PSNetworkText.connectDigestPost(url,file,device.currentUser!!,device.currentPassword!!)

        return simpleBoolParseHttpResult(result,{
            getResult(it)==0
        })
    }

    suspend fun getLanPorts(device:BaseSophosDevice):GetLanPort?{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.LAN_PORT}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)
        return simplesParseHttpResult(result,{
            when(result){
                is HttpResult.Success->{
                    var data:GetLanPort?=null
                    tryCatchBlock {
                        data= JsonHelper.parseJson<GetLanPort>(result.data, GetLanPort::class.java)
                    }
                    return@simplesParseHttpResult data
                }
            }
            return@simplesParseHttpResult null
        })

    }

  /*  suspend fun getLanPortsString(device:BaseSophosDevice):String?{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.LAN_PORT}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)

        when(result){
            is HttpResult.Success->{
                var json=JSONObject(result.data)
                runCatching {
                    var data=json.getJSONObject("data")
                    var ipv4=data.getJSONObject("ipv4")
                    if(ipv4.getInt("ipv4_type")!=null){
                        return result.data
                    }

                   ConstantClass.printForDebug("ipv4_type null")

                }.onFailure { it.printStackTrace() }

            }
        }
        return null
    }*/

    suspend fun setLanPorts(device:BaseSophosDevice,lanPort:String):Boolean{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.LAN_PORT}"
        var result=PSNetworkText.connectDigestPost(url,lanPort,device.currentUser!!,device.currentPassword!!)

        return simpleBoolParseHttpResult(result,{
            getResult(it)==0
        })
    }

    suspend fun getClients(device:BaseSophosDevice,radio: Radio):GetClients?{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.CLIENTS}${radio.toString()}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)

        return simplesParseHttpResult(result,{result->
            var clients:GetClients?=null
            tryCatchBlock{
                clients= JsonHelper.parseJson<GetClients>(result.data, GetClients::class.java)
            }

            return@simplesParseHttpResult clients
        })


    }

    suspend fun kicks(device:BaseSophosDevice,radio: Radio,index: Int,mac: String):Boolean{

        //var encodemac= URLEncoder.encode(mac, StandardCharsets.UTF_8.name() )
        var url="${device.getHttpLocationURL()}${Definitions.CGI.KICK_CLIENT}${radio.toString()}${Definitions.CGI.WIRELESS_SECURITY_INDEX}${index}${Definitions.CGI.CLIENT_MAC}${mac}"
        var result=PSNetworkText.connectDigestPUT(url,device.currentUser!!,device.currentPassword!!)
        return simpleBoolParseHttpResult(result,{
            getResult(it)==0
        })
    }

    suspend fun setDayTime(device:BaseSophosDevice,file: GetDayTime.DayTimeData):Boolean{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.DAY_TIME}"

        var result=PSNetworkText.connectDigestPost(url,JsonHelper.createJson(file),device.currentUser!!,device.currentPassword!!)
        return simpleBoolParseHttpResult(result,{
            getResult(it)==0
        })
       /* when(result){
            is HttpResult.Success->{

                return getResult(result)==0
            }
        }
        return false*/
    }

    suspend fun getDayTime(device: BaseSophosDevice):GetDayTime?{
        var url="${device.getHttpLocationURL()}${Definitions.CGI.DAY_TIME}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)

        return simplesParseHttpResult(result,{result->
            var data:GetDayTime?=null

            tryCatchBlock{
                data= JsonHelper.parseJson<GetDayTime>(result.data, GetDayTime::class.java)
                ConstantClass.printForDebug("getDayTime= ${data}")
            }
            return@simplesParseHttpResult data
        })

    }

    suspend fun traceRoute(device: BaseSophosDevice,address:String):String?{
        //var url="${startURL}${Definitions.CGI.TRACEROUTE}&address=${address}"
        var url="${device.getHttpLocationURL()}${Definitions.CGI.TRACEROUTE}&address=${address}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)
        return simplesParseHttpResult(result,{result->
            var res:String?=null
            tryCatchBlock{
                var data= JsonHelper.parseJson<SimpleResult>(result.data, SimpleResult::class.java)
                res=data?.data?.result?:null
            }
            return@simplesParseHttpResult res
        })
    }

    suspend fun sophosPing(device: BaseSophosDevice,address:String):String?{
      //  var url="${startURL}${Definitions.CGI.PING}&address=${address}"
        var ipv4=JudgeIpAddress.isIPv4Address(address)
        ConstantClass.printForDebug("sophosPing ipv4=${ipv4}")
        var url="${device.getHttpLocationURL()}${Definitions.CGI.PING}${if(ipv4)"" else "6"}&address=${address}"
        var result=PSNetworkText.connectDigest(url,device.currentUser!!,device.currentPassword!!)

        return simplesParseHttpResult(result,{result->
            var res:String?=null
            var data:SimpleResult?=null
            tryCatchBlock{
                data= JsonHelper.parseJson<SimpleResult>(result.data, SimpleResult::class.java)
                ConstantClass.printForDebug("ping data=$data")
                res=data?.data?.result?:null
            }

            return@simplesParseHttpResult res
        })

    }

    fun simpleBoolParseHttpResult(result:HttpResult<String>,blockSuccess: (result:HttpResult.Success<String>) -> Boolean):Boolean{
        return  when(result){
            is HttpResult.Success->blockSuccess.invoke(result)
            else ->false
        }
    }

    fun <T> simplesParseHttpResult(result:HttpResult<String>,blockSuccess: (result:HttpResult.Success<String>) -> T?):T? {//For UI test
      return  when(result){
            is HttpResult.Success->blockSuccess.invoke(result)
            else ->null
        }
    }

    fun printStackTrace(e:Exception){
        e.printStackTrace()
    }



}


