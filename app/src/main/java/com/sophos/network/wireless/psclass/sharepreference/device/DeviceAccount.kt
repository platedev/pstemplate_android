package com.sophos.network.wireless.psclass.sharepreference.device

import com.acelink.library.gson.JsonHelper
import com.acelink.library.sharepreference.PSGCMEncryptSPSecurity
import com.acelink.library.utils.EasyUtils.tryCatchBlock
import com.sophos.network.wireless.psclass.gson.data.SophosDeviceItem
import com.acelink.library.utils.data.ConstantClass
import com.sophos.network.wireless.psclass.gson.data.SophosKeyItem
import java.lang.StringBuilder
import kotlin.collections.HashMap

object SophosAccount {
    private val deviceMy="deviceMy"
    private val deviceAccount="deviceAccount"
   /* private val deviceAccountUser="deviceAccountUser"
    private val deviceAccountPassword="deviceAccountPassword"
    private val deviceName="deviceName"
    private val stationName="stationName"
*/
    fun getAllDevice():HashMap<String,SophosDeviceItem?>{
        var all=PSGCMEncryptSPSecurity.getAllGCMEncrypt()
        var mac=HashMap<String,SophosDeviceItem?>()
        for ((key,value) in all){
            ConstantClass.printForDebug("getAllDevice key =${key} value=${value} ")
            if(key.indexOf(deviceMy)>1&&value!=null){
                var keyHead=key.substring(0,key.lastIndexOf(":"))

                keyHead=keyHead.run {
                    var sb=StringBuilder()
                    var end=(keyHead.length/2)-1
                    for (i in 0 ..  end){
                         sb.append(keyHead.substring(i*2,(i+1)*2))
                        if(i!=end)sb.append(":")
                    }
                    sb.toString()
                }
                var data:SophosDeviceItem?=null
                tryCatchBlock{
                    data=  JsonHelper.parseJson<SophosDeviceItem>(value!!.toString(), SophosDeviceItem::class.java)
                }
                mac.put(keyHead, data)
            }
        }
        return mac
    }

    fun isMyDevice(mac:String):String{
        var m=mac.replace(":","").uppercase()
        return PSGCMEncryptSPSecurity.getStringGCM("${m}:${deviceMy}","")
    }

    fun getMyDevice(mac:String):SophosDeviceItem?{
        var m=mac.replace(":","").uppercase()
        var s=PSGCMEncryptSPSecurity.getStringGCM("${m}:${deviceMy}","")
        return tryCatchBlock<SophosDeviceItem>({
            return@tryCatchBlock  JsonHelper.parseJson<SophosDeviceItem>(s, SophosDeviceItem::class.java)
        },null)

    }

    fun setMyDevice(mac:String,device: SophosDeviceItem){
        var m=mac.replace(":","").uppercase()
        return PSGCMEncryptSPSecurity.setStringGCM("${m}:${deviceMy}",device.json())
    }

    fun getAccount(mac:String): SophosKeyItem?{
        var m=mac.replace(":","").uppercase()
        var json=PSGCMEncryptSPSecurity.getStringGCM("${m}:${deviceAccount}","")!!
        ConstantClass.printForDebug("getAccount json=${json}")
        return JsonHelper.parseJson<SophosKeyItem>(json, SophosKeyItem::class.java)
    }

    fun saveAccount(mac:String,user:String,pass:String){
        var m=mac.replace(":","").uppercase()
        var item= SophosKeyItem(user,pass).json()
        PSGCMEncryptSPSecurity.setStringGCM("${m}:${deviceAccount}",item)
    }

    fun deleteDevice(mac:String){
        var m=mac.replace(":","").uppercase()
        PSGCMEncryptSPSecurity.clearGCMEncryptKey("${m}:${deviceMy}")
        PSGCMEncryptSPSecurity.clearGCMEncryptKey("${m}:${deviceAccount}")
    }
/*
   fun getAccountUser(mac:String):String{
       var m=mac.replace(":","").uppercase()
       return PSGCMEncryptSPSecurity.getStringGCM("${m}:${deviceAccountUser}","")!!
   }

    fun saveAccountUser(mac:String,value:String){
        var m=mac.replace(":","").uppercase()
        PSGCMEncryptSPSecurity.setStringGCM("${m}:${deviceAccountUser}",value)
    }

    fun getPassword(mac:String):String{
        var m=mac.replace(":","").uppercase()
        return PSGCMEncryptSPSecurity.getStringGCM("${m}:${deviceAccountPassword}","")!!
    }

    fun savePassword(mac:String,value:String){
        var m=mac.replace(":","").uppercase()
        PSGCMEncryptSPSecurity.setStringGCM("${m}:${deviceAccountPassword}",value)
    }
*/
}