package com.sophos.network.wireless.gui

import android.Manifest
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.sophos.network.wireless.MainActivity
import com.sophos.network.wireless.MainViewModel
import com.sophos.network.wireless.pagetool.frags_page
import com.acelink.library.utils.data.ConstantClass

import kotlin.math.pow

abstract class BaseFragment: Fragment() {

    companion object{
       // infix fun Int.pow(exponent: Int): Int = toDouble().pow(exponent).toInt()
    }

    //var previousPage:frags_page?=null

    open fun onSelect() {
        ConstantClass.printForDebug("onSelect ${javaClass.simpleName}")
    }

    open fun setStatusBar(){
        getMainActivity()?.setStatusDeep()
    }



    open fun onDeSelect() {
        ConstantClass.printForDebug("onDeSelect ${javaClass.simpleName}")
    }


    open fun onBackPress() {

    }

    fun getMainViewModel(): MainViewModel?=  getMainActivity()?.mainViewModel


    fun getMainActivity(): MainActivity?=(activity as? MainActivity)

    fun getCurrentEditDevice()=getMainActivity()?.getCurrentEditDevice()


    fun getSophosDevice()=(activity as? MainActivity)?.getSophosDevice()


    //fun onShowProgress(msg: String?)= getMainActivity()?.onShowProgress(msg)

    //fun onShowProgress()= getMainActivity()?.onShowProgress(null)

   // fun onHideProgress()=getMainActivity()?.onHideProgress()



    override fun onDestroyView() {
        ConstantClass.printForDebug("onDestroyView ${javaClass.simpleName}")
        super.onDestroyView()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        ConstantClass.printForDebug("${this.javaClass.simpleName} onHiddenChanged ${hidden}")
    }


   /* internal val multiplePermissions = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { it ->
        it.entries.forEach {
            when (it.key) {
                Manifest.permission.CAMERA -> {
                    Log.d("GG", "CAMERA ${it.value}")
                }
                Manifest.permission.READ_EXTERNAL_STORAGE -> {
                    Log.d("GG", "READ_EXTERNAL_STORAGE ${it.value}")
                }
                Manifest.permission.READ_CONTACTS -> {
                    Log.d("GG", "READ_CONTACTS ${it.value}")
                }
                Manifest.permission.ACCESS_FINE_LOCATION -> {
                    Log.d("GG", "READ_CONTACTS ${it.value}")
                }
            }
        }
    }*/
}

