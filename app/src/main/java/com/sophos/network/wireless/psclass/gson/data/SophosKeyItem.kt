package com.sophos.network.wireless.psclass.gson.data


import com.acelink.library.gson.JsonHelper
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SophosKeyItem(
    @Expose @SerializedName("user") var user:String,
    @Expose @SerializedName("password") var password:String){


    fun json(): String {
        return JsonHelper.createJson(this)
    }
}