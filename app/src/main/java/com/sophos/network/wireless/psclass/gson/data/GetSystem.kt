package com.sophos.network.wireless.psclass.gson.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class GetSystem {
    @Expose
    @SerializedName("error_code")
    var errorCode = -1

  //  @Expose
  //  @SerializedName("error_msg")
   // val errorMsg:String?=null

    @Expose
    @SerializedName("data")
    val data:SystemData ?= null


    class SystemData{


        @Expose
        @SerializedName("model_name")
        val model_name:String?= null

        @Expose
        @SerializedName("product_name")
        var productName:String?= null

        @Expose
        @SerializedName("uptime")
        val uptime:String?= null

        @Expose
        @SerializedName("system_time")
        val system_time:String?= null

        @Expose
        @SerializedName("firmware_version")
        val firmware_version:String?= null

        @Expose
        @SerializedName("mac_address")
        val mac_address:String?= null

        @Expose
        @SerializedName("management_vlan_id")
        val management_vlan_id:Int ?= null

        @Expose
        @SerializedName("ip_address")
        val ip_address:String?= null

        @Expose
        @SerializedName("dns_server")
        val dns_server:String?= null

        @Expose
        @SerializedName("default_gateway")
        val default_gateway:String?= null

        @Expose
        @SerializedName("dhcp_server")
        val dhcp_server:String?= null

        @Expose
        @SerializedName("ipv6_address")
        val ipv6_address:String?= null

        @Expose
        @SerializedName("ipv6_link_local_address")
        val ipv6_link_local_address:String?= null

    }
}